import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import '../react-tabs.css';
class Doktorlar extends Component {
  render() {
    return (
       <div>
          <div className="resim">
            <img className="resim" src={this.props.deneme.icerikveri.icerikImage} alt=""/>
          </div>
          <div className="resimaltlik"></div>
          <div className="content-total">
            <div className="content-header">
              <span>{this.props.deneme.birimAdi}</span>
              {this.props.deneme.birimAdi?<div className="content-header-cubuk"></div>:false}
              <span>{this.props.deneme.icerikveri.icerikHeader}</span>
            </div>
            <hr/>
            <div className="content-context">
              
              <Tabs>
                <TabList>
                  {console.log(this.props)}
                {this.props.deneme.icerikveri.tab.tablist.map((data)=><Tab key={data.id}><b>{data.title} {data.name}</b> / {data.bolum}</Tab>)}
                </TabList>
                {this.props.deneme.icerikveri.tab.tablist.map((data)=>{
                  return(<TabPanel key={data.id}>
                    <div className="react-tabs__tab-panel__baslik"><b>{data.title} {data.name}</b></div>
                    <div className="react-tabs__tab-panel__kisisel">
                      <div className="react-tabs__tab-panel__kisisel__sol">
                        <b>{data.compoLang.kisisel_bilgiler}</b>
                        <span>Adı Soyadı: {data.name}</span>
                        {data.tarih?<span>Doğum Tarihi: {data.tarih}</span>:false}
                        {data.birthlocation?<span>Doğum Yeri: {data.birthlocation}</span>:false}
                        {data.mail?<span>E-Mail: {data.mail}</span>:false}
                      </div>
                      <img src={data.resim} alt=""/>
                      {data.education?<div>
                        <span><br/></span>
                        <b>Eğitim Bilgiler:</b>
                        <b>Mezun Olduğu Tıp Fakültesi ve yılı:</b>
                        {data.education.map((edu)=><span key={edu.id}>{edu.adi}</span>)}
                        </div>:false}
                      {data.lang?<div>
                        <span><br/></span>
                        <b>Yabancı Dil:</b>
                        {data.lang.map((dil)=><span key={dil.id}>{dil.adi}</span>)}
                        </div>:false}
                      {data.deneyim?<div>
                        <span><br/></span>
                        <b>Mesleki Deneyim:</b>
                        {data.deneyim.map((tecrube)=><span key={tecrube.id}>{tecrube.adi}</span>)}
                        </div>:false}
                    </div>
                    </TabPanel>)
                  })}
              </Tabs>
            </div>
          </div>
        </div>
    );
  }
}

export default Doktorlar;