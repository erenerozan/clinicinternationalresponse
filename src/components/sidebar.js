import React, { Component } from 'react';
import DropdownMenu, { NestedDropdownMenu } from 'react-dd-menu';
import Collapsible from 'react-collapsible';

//import logo from './logo.svg';
import {
  Link,
} from 'react-router-dom';
import Menu, {SubMenu, MenuItem} from 'rc-menu';
import 'rc-menu/assets/index.css';
import '../scss/react-dd-menu.css';
import Arrow from './arrow.png';

class Sidebar extends Component {
  constructor(props){
    super(props);
    this.state={
      selectedKeys: [],
      openKeys: [],
      sayi:5,
      isMenuOpen: true
    }
    this.toggle=this.toggle.bind(this);
    this.close=this.close.bind(this);
    this.click=this.click.bind(this);
  }
  toggle(){
    this.setState({ isMenuOpen: !this.state.isMenuOpen });
  }

  close(){
    this.setState({ isMenuOpen: false });
  }

  click(){
    console.log('You clicked an item');
  }
  render() {
    const { componentList } = this.props;
    /*function onOpenChange(value) {
      console.log('onOpenChange', value);
    }*/
    /*function handleSelect(info) {
      console.log(info);
      console.log(`selected ${info.key}`);
    }*/
    //var menuitem =[];
    //var sub =[];
   

  function createTree(data) {
    
    return (
      
        <ul>
          {data.map((node,i) => {
            return createNode(node,node.id);
          })}
        </ul>
      
    );
  }
  
  function createNode(data,i) {
    if(data.sidemenuVisible){
      if (data.subMenu) {
        //console.log(data.subMenu,"  ",i)
        return (
            <PrintNode sub={true} title={data.id} text={ data.adi } path={data.paths} i={data.id} sira={i} key={i}>{createTree(data.subMenu)}</PrintNode>
            //<SubMenu title={data.id} key={i}>{createTree(data.subMenu)}</SubMenu>
            
        );
      } else {
        //console.log(data,"  ",i)
        //<MenuItem key={i}><Link to={data.paths}>{data.id}</Link></MenuItem>
        return <PrintNode sub={false} title={data.id} text={ data.adi } path={data.paths} sira={i} index={i} key={i}/>
      }
    }else{
      return false;
    }
    
  }

  const PrintNode = (props) => {

    if(props.sub){

      return (<NestedDropdownMenu key={props.sira} toggle={<Link to={props.path}>{props.text}<img src={Arrow} alt=""/></Link>} animate={true}>
              {props.children}
            </NestedDropdownMenu>)
    }else{
 
      return <li key={props.sira}><Link to={props.path}>{props.text}</Link></li>
    }
    //return <li key={props.ad}>{ props.text }{ props.children }</li>
    
    //return <MenuItem title={props.text} key={props.ad}>İtem3 { props.children }</MenuItem>
  };
  function createTree2(data) {
    
    return (
      
        <ul>
          {data.map((node,i) => {
            return createNode2(node,node.id);
          })}
        </ul>
      
    );
  }
  
  function createNode2(data,i) {
    if(data.sidemenuVisible){
      if (data.subMenu) {
        //console.log(data.subMenu,"  ",i)
        return (
            <PrintNode sub={true} title={data.id} text={ data.adi } path={data.paths} i={data.id} sira={i} key={i}>{createTree2(data.subMenu)}</PrintNode>
            //<SubMenu title={data.id} key={i}>{createTree(data.subMenu)}</SubMenu>
            
        );
      } else {
        //console.log(data,"  ",i)
        //<MenuItem key={i}><Link to={data.paths}>{data.id}</Link></MenuItem>
        return <PrintNode sub={false} title={data.id} text={ data.adi } path={data.paths} sira={i} index={i} key={i}/>
      }
    }else{
      return false;
    }
    
  }

  const PrintNode2 = (props) => {

    if(props.sub){

      return (<Collapsible key={props.sira} trigger={<Link to={props.path}>{props.text}<img src={Arrow} alt=""/></Link>} animate={true}>
              {props.children}
            </Collapsible>)
    }else{
 
      return <li key={props.sira}><Link to={props.path}>{props.text}</Link></li>
    }
  };

    const menuOptions = {
      isOpen: this.state.isMenuOpen,
      close: this.close,
      toggle: false,
      align: 'left',
    };

    return (
      <div className="sidebar-wrapper">
          {/*<div id="inline">{inlineMenu}</div>*/}
          
          {  /*createTree(componentList)*/}
          <div id="inline">
            
              {/*<li><a href="#">Example 1</a></li>
              <li><button type="button" onClick={this.click}>Example 2</button></li>
              <li role="separator" className="separator" />
              <NestedDropdownMenu {...nestedProps}>
                <li><a href="#">I am in a Nested Menu!</a></li>
              </NestedDropdownMenu>*/}
              <Collapsible trigger={<div className="menu"><li>Menu</li></div>}>
                {createTree2(componentList)}
              </Collapsible>
            
          </div>
          <div id="vertical">
          <DropdownMenu {...menuOptions}>
            {/*<li><a href="#">Example 1</a></li>
            <li><button type="button" onClick={this.click}>Example 2</button></li>
            <li role="separator" className="separator" />
            <NestedDropdownMenu {...nestedProps}>
              <li><a href="#">I am in a Nested Menu!</a></li>
            </NestedDropdownMenu>*/}
            {createTree(componentList)}
          </DropdownMenu>
          </div>
      </div>
    );
  }
}

export default Sidebar;