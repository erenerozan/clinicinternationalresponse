import React, { Component } from 'react';
import * as FontAwesome from 'react-icons/lib/fa';
//import logo from './logo.svg';


class Footer extends Component {
  render() {
    return (
      <div className="footer-wrapper">
          <div className="footer-logo">
            <img src="http://clinicinternational.com.tr/2011/g/foot-logo.jpg" className="footer-logo" alt=""/>
          </div>
          <div className="footer-sgk-logo">
            <img src="http://clinicinternational.com.tr/2011/g/sgklogo.jpg" className="footer-sgk-logo" alt=""/>
          </div>
          <div className="footer-map">
            <img src="http://clinicinternational.com.tr/2011/g/foot-google-tr.jpg" className="footer-map" alt=""/>
          </div>
          <div className="footer-adres">
            <b>Adres:</b>Kıbrıs Şehitleri Cad. No 181 (Antik Tiyatro Çaprazı) 48400 Bodrum - Turkey
            <br/>
            <b><FontAwesome.FaPhone color="red"/>&nbsp;Tel:</b>+90 252 313 30 30 <b><FontAwesome.FaFax color="red"/>&nbsp;Fax:</b> +90 252 316 00 08
            <br/>
            <b><FontAwesome.FaEnvelope color="red"/>&nbsp;Email:</b>info@clinicinternational.com.tr
          </div>
      </div>
    );
  }
}

export default Footer;
