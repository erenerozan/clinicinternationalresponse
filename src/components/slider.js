import React, { Component } from 'react';
import Swiper from 'react-id-swiper';
import {
  Link,
} from 'react-router-dom';
import '../swiper.css';

class Slider extends Component {

  render() {
    const params = {
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: (index, className) => {
          return '<span class="' + className + '">' + (index + 1) + '</span>';
        },
      },
      autoplay: {
        delay: 4500,
        disableOnInteraction: false
      },
    };
    const params2 = {
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      direction: 'vertical',
      slidesPerView: 1,
      loop:true,
      autoplay: {
        delay: 4500,
        disableOnInteraction: false
      },
      spaceBetween: 30,
      mousewheelControl: true
    };
    return (
      <div>
          <div className="resimslider">
          <Swiper {...params}>
            {
              this.props.deneme.slider.map(sliderImage => <a href={sliderImage.link} target="_blank" className="resimslider"><img key={sliderImage.img} className="resimslider" src={sliderImage.img} alt=""/></a>)
            }
          </Swiper>
          </div>
          <div className="widget-wrap">
          {
            this.props.deneme.widget.map(widgetdata=>{
             return(
               <div className={widgetdata.classwrap} key={widgetdata.id}>
               <div key={widgetdata.id} className={widgetdata.classname}>
                <Link to={widgetdata.link}>
                  <img src={widgetdata.widgetpic} alt=""/>
                  <div className="widget-title">{widgetdata.title}</div>
                </Link>
              </div>
              </div>
            )})
          }
          </div>
          <div className="haberler-wrap">
            <div className="haberler-content">
              <div className="haberler-title">
                {this.props.deneme.haberlerbaslik}
              </div>
              <Swiper {...params2}>
                {
                  this.props.deneme.haberler.map(haberdata => {
                  return (
                      <div className="haberler" key={haberdata.id}>
                        {haberdata.mesaj}
                      </div>
                      )})
                }
              </Swiper>
            </div>
          </div>
      </div>
    );
  }
}

export default Slider;
