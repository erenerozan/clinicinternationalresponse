import React, { Component } from 'react';



class EmailRandevu extends Component {


  render() {
    var {compoShema} = this.props;
    return (
    <div>
        <div className="contact">
            <div className="filter">
            <form className="form" onSubmit={this.props._handleSubmitRandevu}>
            <span><span>{this.props.compoShema.adSoyad}</span><input type="text" value={this.props.contactName} onChange={this.props._handleChangeName} required/></span>
            <span><span>{this.props.compoShema.cep}</span><input type="text" value={this.props.contactPhone} onChange={this.props._handleChangePhone} required/></span>
            <span><span>E-mail : </span><input type="email" value={this.props.contactEmail} onChange={this.props._handleChange} required/></span>
            <span><span>{this.props.compoShema.randevu_birim}</span>
            <select value={this.props.optionsState} onChange={this.props._selectOptions}>
              {compoShema.birimlang.map((datalar,i)=><option key={i} value={datalar.adi}>{datalar.adi}</option>)}
            </select></span>
            <span><span>{this.props.compoShema.randevu_tarih}</span><input type="date" value={this.props.randevuTarih} onChange={this.props._randevuSelect} required/></span>
            <span><span>{this.props.compoShema.randevu_saat}</span>
            <select value={this.props.optionsTime} onChange={this.props._selectTime}>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
            </select></span>
            <span><span>{this.props.compoShema.aciklama}</span><textarea id="formMsg" name="formMsg" rows="8" cols="40" value={this.props.contactMessage} onChange={this.props._handleChangeMsg} required></textarea></span>
            <input type="submit" value={this.props.compoShema.gonder} className="btn--cta" id="btn-submit" />
            {this.props.Mailmesaj}
          </form>
            </div>
        </div>
    </div>
    );
  }
}

export default EmailRandevu;
