import React, { Component } from 'react';



class EmailIk extends Component {


  render() {
    return (
    <div>
        <div className="contact">
            <div className="filter">
                <form className="form" onSubmit={this.props._handleSubmitIk}>
                    <span>
                        <span className="content-p-small-red">{this.props.compoShema.kisisel}</span>
                    </span>
                    <span>
                        <span>{this.props.compoShema.pozisyon}</span>
                        <input
                            type="text"
                            value={this.props.pozisyonName}
                            onChange={this.props._handleChangePozisyon}
                            required
                        />
                    </span>
                    <span>
                        <span>{this.props.compoShema.basvuruTarih}</span>
                        {this.props.basvuruTarih}
                    </span>
                    <span>
                        <span>{this.props.compoShema.adSoyad}</span>
                        <input
                            type="text"
                            value={this.props.contactName}
                            onChange={this.props._handleChangeName}
                            required
                        />
                    </span>
                    <span>
                        <span>{this.props.compoShema.dogumYeri}</span>
                        <input
                            type="text"
                            value={this.props.contactDogum}
                            onChange={this.props._handleChangeDogum}
                            required
                        />
                    </span>
                    <span>
                        <span>{this.props.compoShema.dogumTarih}</span>
                        <input
                            type="date"
                            value={this.props.contactDogumTarih}
                            onChange={this.props._handleChangeDogumTarih}
                            required
                        />
                    </span>
                    <span>
                        <span>{this.props.compoShema.cinsiyet}</span>
                        <select required
                            //value={this.props.contactCinsiyet}
                            onChange={this.props._handleChangeCinsiyet}
                        >
                            <option value="">Seçiniz</option>
                            <option value="erkek">Erkek</option>
                            <option value="kadın">Kadın</option>
                        </select>
                    </span>
                    <span>
                        <span>{this.props.compoShema.uyruk}</span>
                        <input
                            type="text"
                            value={this.props.contactUyruk}
                            onChange={this.props._handleChangeUyruk}
                            required
                        />
                    </span>
                    <span>
                        <span>{this.props.compoShema.cep}</span>
                        <input
                            type="text"
                            value={this.props.contactPhone}
                            onChange={this.props._handleChangePhone}
                            required
                        />
                    </span>
                    <span>
                        <span>E-mail : </span>
                        <input type="email" value={this.props.contactEmail} onChange={this.props._handleChange} required/>
                    </span>
                    <span>
                        <span>{this.props.compoShema.mesaj}</span>
                        <textarea id="formMsg" name="formMsg" rows="8" cols="40" value={this.props.contactMessage} onChange={this.props._handleChangeMsg} required></textarea>
                    </span>
                    <span>
                        <span className="content-p-small-red">{this.props.compoShema.egitim}</span>
                    </span>
                    <span>
                        <span>{this.props.compoShema.sonmezunOlunan}</span>
                        <input
                            type="text"
                            value={this.props.contactMezun}
                            onChange={this.props._handleChangeMezun}
                            required
                        />
                    </span>
                    <span>
                        <span>{this.props.compoShema.yabanciDil}</span>
                        <input
                            type="text"
                            value={this.props.contactYabancidil}
                            onChange={this.props._handleChangeYabancidil}
                            required
                        />
                    </span>
                    <input type="submit" value={this.props.compoShema.gonder} className="btn--cta" id="btn-submit" />
                    {this.props.Mailmesaj}
                </form>
            </div>
        </div>
    </div>
    );
  }
}

export default EmailIk;
