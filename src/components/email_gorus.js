import React, { Component } from 'react';



class EmailPanel extends Component {

  
  render() {
    return (
    <div>
        <div className="contact">
            <div className="filter">
                <form className="form" onSubmit={this.props._handleSubmit}>
                    <span>
                        <span>{this.props.compoShema.adSoyad}</span>
                        <input type="text" value={this.props.contactName} onChange={this.props._handleChangeName} required/>
                    </span>
                    <span>
                        <span>E-mail : </span>
                        <input type="email" value={this.props.contactEmail} onChange={this.props._handleChange} required/>
                    </span>
                    <span>
                        <span>{this.props.compoShema.mesaj}</span>
                        <textarea id="formMsg" name="formMsg" rows="8" cols="40" value={this.props.contactMessage} onChange={this.props._handleChangeMsg} required></textarea>
                    </span>
                    <input type="submit" value={this.props.compoShema.gonder} className="btn--cta" id="btn-submit" />
                    {this.props.Mailmesaj}
                </form>
            </div>
        </div>
    </div>
    );
  }
}

export default EmailPanel;