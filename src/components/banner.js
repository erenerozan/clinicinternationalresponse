import React, { Component } from 'react';
import ReactSWF from 'react-swf';
import call from './call-center.png';
import swf from './Untitled-1.swf';
import {
  Link,
} from 'react-router-dom';

class Banner extends Component {
  render() {
    //const { componentList } = this.props;
    return (
      <div className="banner-wrapper">
        <div className="nav-wrapper">
          <div className="logo">
            <img className="logo" src="http://clinicinternational.com.tr/2011/g/logo.png" alt=""/>
          </div>
          <div className="banner-baslik">
            {this.props.baslik}
          </div>
          <div className="banner-lang">
            <Link className="banner-anasayfa" to="/">{this.props.anaSayfa}</Link>
            <div className="banner-lang-flag">
              <img src="http://clinicinternational.com.tr/2011/g/flags/tr-on.jpg" onClick={this.props.tr} alt=""/>
              <img src="http://clinicinternational.com.tr/2011/g/flags/en-on.jpg" onClick={this.props.en} alt=""/>
              <img src="http://clinicinternational.com.tr/2011/g/flags/fr-on.jpg" onClick={this.props.fr} alt=""/>
            </div>
          </div>
          <div className="call">
            <img src={call} alt=""/>
          </div>
          <div className="swf">
          <ReactSWF
            src={swf}
            id="guid_001"
            width="183"
            height="71"
            wmode="transparent"
            flashVars={{foo: 'A', bar: 1}}
          />
          </div>
        </div>
      </div>
    );
  }
}

export default Banner;