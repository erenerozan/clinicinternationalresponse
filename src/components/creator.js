import React, { Component } from 'react';
import Gallery from 'react-photo-gallery';
import Lightbox from 'react-images';
import YoutubeVideo from 'react-youtube-video';
import axios from 'axios';
import qs from 'qs';
import EmailPanel from './email_gorus.js';
import EmailRandevu from './email_randevu.js';
import EmailIk from './email_ik.js';

class Creator extends Component {
  constructor(props){
    super(props);
    this.state={
      lightboxIsOpen:false,
      currentImage:0,
      contactEmail: '',
      contactMessage: '',
      contactName:'',
      Mailmesaj:'',
      optionsState:"",
      optionsTime:"",
      randevuTarih:"",
      contactPhone:"",
      pozisyonName:"",
      contactDogum:"",
      contactDogumTarih:"",
      contactCinsiyet:"",
      contactUyruk:"",
      contactMezun:"",
      contactYabancidil:"",
    };
    this.openLightbox = this.openLightbox.bind(this);
    this.closeLightbox = this.closeLightbox.bind(this);
    this.gotoNext = this.gotoNext.bind(this);
    this.gotoPrevious = this.gotoPrevious.bind(this);



    this._handleSubmit = this._handleSubmit.bind(this);
    this._handleSubmitRandevu = this._handleSubmitRandevu.bind(this);
    this._handleSubmitIk = this._handleSubmitIk.bind(this);

    this._handleChange = this._handleChange.bind(this);
    this._handleChangePozisyon = this._handleChangePozisyon.bind(this);
    this._handleChangePhone = this._handleChangePhone.bind(this);
    this._handleChangeName = this._handleChangeName.bind(this);
    this._handleChangeMsg = this._handleChangeMsg.bind(this);
    this._handleChangeDogum = this._handleChangeDogum.bind(this);
    this._handleChangeDogumTarih = this._handleChangeDogumTarih.bind(this);
    this._handleChangeCinsiyet = this._handleChangeCinsiyet.bind(this);
    this._handleChangeUyruk = this._handleChangeUyruk.bind(this);
    this._handleChangeMezun = this._handleChangeMezun.bind(this);
    this._handleChangeYabancidil = this._handleChangeYabancidil.bind(this);

    this._selectOptions = this._selectOptions.bind(this);
    this._selectTime = this._selectTime.bind(this);
    this._randevuSelect = this._randevuSelect.bind(this);
  }
  componentDidMount(){
    this.tarihGetir();
  }
  openLightbox(event, obj) {
    this.setState({
      currentImage: obj.index,
      lightboxIsOpen: true,
    });
  }
  closeLightbox() {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  }
  gotoNext(){
    console.log("hi");
    var imga = this.state.currentImage;
    this.setState({currentImage:++imga});
  }
  gotoPrevious(){
    console.log("hi");
    var imga = this.state.currentImage;
    this.setState({currentImage:--imga});
  }
  _handleChange(e) {
    this.setState({
      contactEmail: e.target.value,
    });
  }
  _handleChangePozisyon(e) {
    this.setState({
      pozisyonName: e.target.value,
    });
  }
  _handleChangePhone(e) {
    this.setState({
      contactPhone: e.target.value,
    });
  }
  _handleChangeName(e) {
   this.setState({
    contactName: e.target.value,
   });
 }
  _handleChangeDogum(e) {
    this.setState({
    contactDogum: e.target.value,
    });
  }
  _handleChangeDogumTarih(e) {
    this.setState({
    contactDogumTarih: e.target.value,
    });
  }
  _handleChangeCinsiyet(e) {

    console.log(e.target.value);
    this.setState({
      contactCinsiyet: e.target.value,
    });
    console.log(this.state.contactCinsiyet);
  }
  _handleChangeUyruk(e) {
    this.setState({
      contactUyruk: e.target.value,
    });
  }
  _handleChangeMezun(e) {
    this.setState({
      contactMezun: e.target.value,
    });
  }
  _handleChangeYabancidil(e) {
    this.setState({
      contactYabancidil: e.target.value,
    });
  }
  // Change state of input field so text is updated while typing
  _handleChangeMsg(e) {
    this.setState({
      contactMessage: e.target.value
    });
  }
  _selectOptions(e){
    console.log(e.target.value);
    this.setState({optionsState:e.target.value});
  }
  _selectTime(e){
    console.log(e.target.value);
    this.setState({optionsTime:e.target.value});
  }
  _randevuSelect(e){
    console.log(e.target.value);
    this.setState({randevuTarih:e.target.value});
  }

  _handleSubmit(e) {
    e.preventDefault();
    var self =this;
    axios.post('http://clinicinternational.com.tr/2011/gorus_oneri_sendmail.php',
      qs.stringify({
        'Ad Soyad': this.state.contactName,
        'E-mail': this.state.contactEmail,
        'Mesaj': this.state.contactMessage,
       })
    ).then(function (res) {
     self.setState({
       Mailmesaj: 'Gönderildi...',
       contactEmail: '',
       contactMessage: '',
       contactName:'',
       optionsState:"",
       optionsTime:"",
       randevuTarih:"",
       contactPhone:"",
       pozisyonName:"",
       contactDogum:"",
       contactDogumTarih:"",
       contactCinsiyet:"",
       contactUyruk:"",
       contactMezun:"",
       contactYabancidil:"",
     });
    })
    .catch(function (err) {
        console.log(err);
    });
  }
  _handleSubmitIk(e) {
    console.log(this.state);
    e.preventDefault();
    var self =this;
    axios.post('http://clinicinternational.com.tr/2011/gorus_oneri_sendmail.php',
      qs.stringify({
        'Ad Soyad': this.state.contactName,
        'E-mail': this.state.contactEmail,
        'Mesaj': this.state.contactMessage,
        'Cep':this.state.contactPhone,
        'Pozisyon':this.state.pozisyonName,
        'Dogum Yeri':this.state.contactDogum,
        'Dogum Tarihi':this.state.contactDogumTarih,
        'Cinsiyet':this.state.contactCinsiyet,
        'Uyruk':this.state.contactUyruk,
        'Mezun':this.state.contactMezun,
        'Yabancı Dil':this.state.contactYabancidil,
       })
    ).then(function (res) {
     self.setState({
       Mailmesaj: 'Gönderildi...',
       contactEmail: '',
       contactMessage: '',
       contactName:'',
       optionsState:"",
       optionsTime:"",
       randevuTarih:"",
       contactPhone:"",
       pozisyonName:"",
       contactDogum:"",
       contactDogumTarih:"",
       contactCinsiyet:"",
       contactUyruk:"",
       contactMezun:"",
       contactYabancidil:"",
     });
    })
    .catch(function (err) {
        console.log(err);
    });
  }
  _handleSubmitRandevu(e) {
    e.preventDefault();
    var self =this;
    axios.post('http://clinicinternational.com.tr/2011/gorus_oneri_sendmail.php',
      qs.stringify({
        'Ad Soyad': this.state.contactName,
        'E-mail': this.state.contactEmail,
        'Cep No': this.state.contactPhone,
        'Mesaj': this.state.contactMessage,
        'Randevu Tarih':this.state.randevuTarih,
        'Randevu Birimi':this.state.optionsState,
        'Randevu Saati': this.state.optionsTime
       })
    ).then(function (res) {
     self.setState({
       Mailmesaj: 'Gönderildi...',
       contactEmail: '',
       contactMessage: '',
       contactName:'',
       optionsState:"",
       optionsTime:"",
       randevuTarih:"",
       contactPhone:"",
       pozisyonName:"",
       contactDogum:"",
       contactDogumTarih:"",
       contactCinsiyet:"",
       contactUyruk:"",
       contactMezun:"",
       contactYabancidil:"",
     });
    })
    .catch(function (err) {
        console.log(err);
    });
  }

  tarihGetir(){
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    this.setState({basvuruTarih:date});
  }

  render() {

    function contextcreator(tag,veri,styles,i){
      var icerikveri = veri;
      switch (tag) {
        case "p":
          return (<p key={i} className={styles}>{icerikveri}</p>);

        case "li":
          return (<li key={i} className={styles}>{icerikveri}</li>);

        case "br":
          return (<br key={i}/>);

        case "div":
          return (<div key={i} className={styles}>{icerikveri}</div>);

        case "map":
          return(<iframe key={i} width="720" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps/ms?ie=UTF8&amp;hl=tr&amp;t=h&amp;msa=0&amp;msid=210991494361326851769.00049cdc896d8fc7aa3e8&amp;ll=37.035481,27.426467&amp;spn=0.015416,0.034289&amp;z=15&amp;output=embed"></iframe>);

        case "video":
          return (<YoutubeVideo url={icerikveri}/>);

        default:
          break;
      }
    }
    const PHOTO_SET = [
      {
        src: 'http://clinicinternational.com.tr/2011/g/galeri-2016/1.jpg',
        alt: 'image 1',
        width:500,
        height:500,
      },
      {
        src: 'http://clinicinternational.com.tr/2011/g/galeri-2016/2.jpg',
        alt: 'image 2',
        width:500,
        height:500,
      },
      {
        src: 'http://clinicinternational.com.tr/2011/g/galeri-2016/3.jpg',
        alt: 'image 2',
        width:500,
        height:500,
      },
      {
        src: 'http://clinicinternational.com.tr/2011/g/galeri-2016/4.jpg',
        alt: 'image 1',
        width:500,
        height:500,
      },
      {
        src: 'http://clinicinternational.com.tr/2011/g/galeri-2016/5.jpg',
        alt: 'image 2',
        width:500,
        height:500,
      },
      {
        src: 'http://clinicinternational.com.tr/2011/g/galeri-2016/6.jpg',
        alt: 'image 2',
        width:500,
        height:500,
      },
      {
        src: 'http://clinicinternational.com.tr/2011/g/galeri-2016/7.jpg',
        alt: 'image 2',
        width:500,
        height:500,
      },
      {
        src: 'http://clinicinternational.com.tr/2011/g/galeri-2016/8.jpg',
        alt: 'image 2',
        width:500,
        height:500,
      },
      {
        src: 'http://clinicinternational.com.tr/2011/g/galeri-2016/9.jpg',
        alt: 'image 2',
        width:500,
        height:500,
      },
      {
        src: 'http://clinicinternational.com.tr/2011/g/galeri-2016/10.jpg',
        alt: 'image 2',
        width:500,
        height:500,
      },
      {
        src: 'http://clinicinternational.com.tr/2011/g/galeri-2016/11.jpg',
        alt: 'image 2',
        width:500,
        height:500,
      },
      {
        src: 'http://clinicinternational.com.tr/2011/g/galeri-2016/12.jpg',
        alt: 'image 2',
        width:500,
        height:500,
      },
    ];

    return (
      <div>
          <div className="resim">
            <img className="resim" src={this.props.deneme.icerikveri.icerikImage} alt=""/>
          </div>
          <div className="resimaltlik"></div>
          <div className="content-total">
            <div className="content-header">
              <span>{this.props.deneme.birimAdi}</span>
              {this.props.deneme.birimAdi?<div className="content-header-cubuk"></div>:false}
              <span>{this.props.deneme.icerikveri.icerikHeader}</span>
            </div>
            <hr/>
            <div className="content-context">
              {this.props.deneme.icerikveri.icerikContext.map((icerikdata,i)=>{
                  var { tag, icerik, classname} = icerikdata;
                  var fonsiyondan = contextcreator(tag,icerik,classname,i);
                  return fonsiyondan;
                })
              }
                {this.props.deneme.email_gorus?<div>
                  <EmailPanel
                    compoShema={this.props.deneme.emaillang}
                    Mailmesaj={this.state.Mailmesaj}
                    contactName={this.state.contactName}
                    contactPhone={this.state.contactPhone}
                    contactEmail={this.state.contactEmail}
                    contactMessage={this.state.contactMessage}
                    optionsState={this.state.optionsState}
                    optionsTime={this.state.optionsTime}
                    randevuTarih={this.state.randevuTarih}
                    _handleSubmit={this._handleSubmit}
                    _handleChangeName={this._handleChangeName}
                    _handleChange={this._handleChange}
                    _handleChangeMsg={this._handleChangeMsg}
                    _handleChangePhone={this._handleChangePhone}
                    _selectOptions={this._selectOptions}
                    _randevuSelect={this._randevuSelect}
                    _selectTime={this._selectTime}
                  />
                  </div>:false}
                {this.props.deneme.email_randevu?<div>
                    <EmailRandevu
                      compoShema={this.props.deneme.emaillang}
                      Mailmesaj={this.state.Mailmesaj}
                      contactName={this.state.contactName}
                      contactPhone={this.state.contactPhone}
                      contactEmail={this.state.contactEmail}
                      contactMessage={this.state.contactMessage}
                      optionsState={this.state.optionsState}
                      optionsTime={this.state.optionsTime}
                      randevuTarih={this.state.randevuTarih}
                      _handleSubmitRandevu={this._handleSubmitRandevu}
                      _handleChangeName={this._handleChangeName}
                      _handleChange={this._handleChange}
                      _handleChangeMsg={this._handleChangeMsg}
                      _handleChangePhone={this._handleChangePhone}
                      _selectOptions={this._selectOptions}
                      _randevuSelect={this._randevuSelect}
                      _selectTime={this._selectTime}
                    />
                  </div>:false}

                {this.props.deneme.insan_kaynaklari?<div>
                  <EmailIk
                    compoShema={this.props.deneme.emaillang}
                    _handleSubmitIk={this._handleSubmitIk}

                    pozisyonName={this.state.pozisyonName}
                    _handleChangePozisyon={this._handleChangePozisyon}

                    basvuruTarih={this.state.basvuruTarih}

                    contactName={this.state.contactName}
                    _handleChangeName={this._handleChangeName}

                    contactDogum={this.state.contactDogum}
                    _handleChangeDogum={this._handleChangeDogum}

                    contactDogumTarih={this.state.contactDogumTarih}
                    _handleChangeDogumTarih={this._handleChangeDogumTarih}

                    contactEmail={this.state.contactEmail}
                    _handleChange={this._handleChange}

                    contactMessage={this.state.contactMessage}
                    _handleChangeMsg={this._handleChangeMsg}

                    contactCinsiyet={this.state.contactCinsiyet}
                    _handleChangeCinsiyet={this._handleChangeCinsiyet}

                    contactUyruk={this.state.contactUyruk}
                    _handleChangeUyruk={this._handleChangeUyruk}

                    contactPhone={this.state.contactPhone}
                    _handleChangePhone={this._handleChangePhone}

                    contactMezun={this.state.contactMezun}
                    _handleChangeMezun={this._handleChangeMezun}

                    contactYabancidil={this.state.contactYabancidil}
                    _handleChangeYabancidil={this._handleChangeYabancidil}
                    Mailmesaj={this.state.Mailmesaj}
                  />
                  </div>:false}

                <div className="galer">
                {this.props.deneme.icerikveri.fotogallery?(<Gallery photos={PHOTO_SET} onClick={this.openLightbox}/>):false}
                <Lightbox
                    images={PHOTO_SET}
                    isOpen={this.state.lightboxIsOpen}
                    onClickPrev={this.gotoPrevious}
                    currentImage={this.state.currentImage}
                    onClickNext={this.gotoNext}
                    onClose={this.closeLightbox}
                  />
                </div>
            </div>
          </div>
      </div>
    );
  }
}

export default Creator;
