import React, { Component } from 'react';
//import logo from './logo.svg';
import {
  Switch,
  Route,
} from 'react-router-dom';
import Creator from './creator.js';
import Slider from './slider.js';
import Doktorlar from './doktorlar.js';

class Content extends Component {
  render() {
    const { componentData } = this.props;
    function compolar(veriler,propslar) {
      switch (veriler) {
        case 0:
          var layout = (<Creator deneme={propslar}/>);
          return layout;
        
        case 1:
          var layout1 = (<Slider deneme={propslar}/>);
          return layout1;
        
        case 2:
          var layout2 = (<Doktorlar deneme={propslar}/>);
          return layout2;  

        default:
          break;
      }
    }
    var obj =[];
    function createRoute(veri){
      var zet= veri.map((x)=>{
        if(x.subMenu){
         let layout=<Route 
              exact={x.exact}
              path={x.paths} 
              key={x.id} 
              component={()=>(compolar(x.componenti,x))}
          />;
          obj.push(layout);
          createRoute(x.subMenu);
        }else{
          let layout=<Route 
              exact={x.exact}
              path={x.paths} 
              key={x.id} 
              component={()=>(compolar(x.componenti,x))}
          />;
          obj.push(layout);

        }
        return "";
        }
      )
      //console.log(obj);
      return zet;
      
    }
    return (
      <div className="content-wrapper">
          <Switch>
            { createRoute(componentData) }
            { obj.map((veri)=>veri) }
          </Switch>
      </div>
    );
  }
}

export default Content;