import React, { Component } from 'react';
import LocalizedStrings from 'react-localization';
import {
  BrowserRouter,
  //Route,
  //Link,
  //HashRouter
} from 'react-router-dom';
//import logo from './logo.svg';
import './style.css';
import Banner from './components/banner.js';
import Content from './components/content.js';
import Footer from './components/footer.js';
//import Creator from './components/creator.js';
//import Doktorlar from './components/doktorlar.js';
import Sidebar from './components/sidebar.js';
class App extends Component {
  constructor(props){
    super(props);
    this.state={
      strings : new LocalizedStrings({
        tr:{
          baslik:"SAĞLIĞINIZ BİZE EMANET...",
          anasayfa:"ANA SAYFA",
          veri:[
            //110
            {
              id:110,
              paths:'/',
              adi:"Anasayfa",
              sidemenuVisible:false,
              exact:true,
              componenti:1,
              slider:[
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webgeçiştirmeyin.jpg",
                  link:"http://bit.ly/2EgnDBD"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webmiyom.jpg",
                  link:"http://bit.ly/2H1njUR"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webtansiyon.jpg",
                  link:"http://bit.ly/2nLiwiw"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webterleme.jpg",
                  link:"http://bit.ly/2G0IK7e"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webtipIIdiyabet.jpg",
                  link:"http://bit.ly/2nQdX6t"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webtüpmide.jpg",
                  link:"http://bit.ly/2C8TC0i"
                },
                {
                  img:"http://media.kenttv.net/20180204062659_kenttv_haber_606.jpg?v=1",
                  link:"https://www.kenttv.net/haber.php?id=40834"
                },
                {
                  img:"http://media.kenttv.net/20180221040704_kenttv_haber_606.jpg?v=1",
                  link:"http://www.kenttv.net/haber.php?id=41015"
                },
              ],
              widget:[
                {
                  id:"widget1",
                  link:"/hastahikayeleri",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"HASTA HİKAYELERİ",
                  widgetpic:"http://www.kadinhaberleri.com/images/haberler/kil_donmesi_kadinlarda_olur_mu_h600513.jpg"
                },
                {
                  id:"widget2",
                  link:"/iletisim",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"ACİL-İLETİŞİM-ULAŞIM",
                  widgetpic:"https://www.framestr.com/wp-content/uploads/2017/08/emergency-contact-form.jpg"
                },
                {
                  id:"widget3",
                  link:"/check_up",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"CHECK-UP",
                  widgetpic:"https://acibademmobil-wbeczwggocwcfpv3pwm.stackpathdns.com/wp-content/uploads/2017/01/bakicim-check-up-doktor-muayenesi.jpg"
                },
                {
                  id:"widget4",
                  link:"/video",
                  classname:"widgetsecond",
                  classwrap:"widget-box2",
                  title:"VİDEO GALERİ",
                  widgetpic:"http://ahps.org.sg/wp-content/uploads/photo-gallery/41144817-HEALTH-word-cloud-concept-Stock-Photo-health.jpg"
                },
                {
                  id:"widget5",
                  link:"#",
                  classname:"widgetsecond",
                  classwrap:"widget-box2",
                  title:"SAĞLIKLI BİLGİLER",
                  widgetpic:"https://i.pinimg.com/736x/c4/db/c0/c4dbc0f1f6c23496f433ac82bc4cef83--healthy-tips-healthy-eating.jpg"
                },
              ],
              haberlerbaslik:"HABERLER ve DUYURULAR",
              haberler:[
                {
                  id:"widget1",
                  link:"/hakkimizda",
                  mesaj:`1- Genel Cerrahi uzmanı Doçent Doktor Cengiz Kayahan, SGK anlaşmalı olarak hasta kabulüne başlamıştır, kendisine başarılar diliyoruz.
                  Hekimimiz özellikle obezite cerrahisi mide küçültme ameliyatları ) , tiroid cerrahisi, bağırsak ameliyatları, ve meme cerrahisi konusunda üstün deneyime sahiptir.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
                {
                  id:"widget3",
                  link:"/hakkimizda",
                  mesaj:`2- Genel Dahiliye Uzmanı Dr. Mehmet Demircioğlu, SGK anlaşmalı olarak merkezimizde çalışmaya başlamıştır. Özellikle diyabet üzerinde uzmanlaşan hekimimiz 3 sene boyunca Acıbadem Bodrum Hastanesi’nde çalıştıktan sonra mesleğine merkezimizde devam etmektedir.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
                {
                  id:"widget4",
                  link:"/hakkimizda",
                  mesaj:`3- 5 Yıldır Bodrum'da başka bir sağlık kuruluşunda çalışan Kadın Hastalıkları ve Doğum Uzmanı Op. Dr. Ömer Orçun Koçak, merkezimizde SGK anlaşmalı olarak çalışmaya başlamıştır, kendisine başarılar diliyoruz.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
                {
                  id:"widget5",
                  link:"/hakkimizda",
                  mesaj:`4- 6 yıldır Bodrum'da başka sağlık kuruluşlarında çalışan Kardiyoloji uzmanı ( Kalp Hastalıkları ) Uzm.Dr. Fikret Mert Acar, kurumumuzda SGK anlaşmalı olarak çalışmaya başlamıştır, kendisine başarılar diliyoruz.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
              ],
              icerikveri:{
                icerikImage:"https://paratic.com/dosya/2015/11/kadinlari-cezbeden-birbirinden-cekici-araba-modelleri-bugatti-veyron.jpg",
                icerikHeader:"Deneme Başlığıdğr",
                icerikContext:"Yukardakinin aynısı"
              },
            },
            //110bitis
            //120
            {
              id:120,
              paths:'/creator',
              adi:"Hakkımızda",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"HAKKIMIZDA",
                icerikContext:[
                  {
                    tag:"div",
                    icerik:"Turizm ve eğlence cenneti Bodrum, her yıl tatil hayallerini gerçekleştirmek için dünyanın dört bir yanından gelen binlerce misafir ağırlıyor. Ancak kimi zaman küçük bir rahatsızlık, ya da umulmadık bir kaza- bir hastalık, tüm planları ve tüm tatil hayallerini alt-üst edebiliyor.",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Kaza,hastalık ve yaralanmalar gibi beklenmedik durumlar ne zaman başınıza gelse, üzüntü, endişe ve sıkıntı kaynağı olurlar; üstelikte bilmediğiniz bir ülkede- şehirde- kültürde iken, hastalanmak bu korku ve endişeleri daha çok arttırır.",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Bizler bu korkularınızı çok iyi anlıyor, ve sizler için huzurlu, güvenli ve rahat bir ortam yaratmaya çabalıyoruz. Butik klinik konsepti ile Özel Clinic International BMC Tıp Merkezi, güleryüzlü ve modern hizmet anlayışıyla, sizlerin sorunlarına kısa sürede çözüm bulup, iyileşmenize yardımcı olurken, evinizdeki konfor ve huzuru da aratmamak için hizmetinizdedir.",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Özel Clinic International Tıp Merkezi, uzman ve seçkin kadrosu, güleryüzlü, modern hizmet anlayışıyla, hasta hakları ve etik kurallara saygılı, tıp, teknoloji ve sağlık sektöründeki gelişmeleri takip ederek, kendi hizmet kalitesini sürekli yenileyerek arttıran, hızıl ve kesin tedavi yaklaşımıyla koruyucu ve iyileştirici sağlık hizmetleri sunan butik bir kliniktir.",
                    classname:"content-warning",
                  },
                ]
              },
            },
            //120bitis
            //130
            {
              id:130,
              paths:'/tibbibirimler',
              adi:"Tıbbi Birimler",
              sidemenuVisible:true,
              exact:false,
              subMenu:[
                {
                  id:131,
                  paths:'/kadin',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Kadın Hastalıkları ve Doğum",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Kadın Hastalıkları ve Doğum",
                    icerikContext:[
                      {
                        tag:"li",
                        icerik:"Kadın hastalıkları muayenesi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Gebelik takibi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Doğum sonrası kontrol muayenesi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ultrasonografi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Menapoz takibi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Aile planlaması uygulamaları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"İnfertilite takibi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Menstruel bozuklukların takibi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Cervical kontrol ve biyopsiler",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Doğum",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Jinekolojik operasyonlar",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Pap-smear test (servikal yayma)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Meme muayenesi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Endometriyal biyopsi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Rahim içi araç (RİA) takılması",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Jinekoloji kliniğimizde uzman doktorumuz tarafından yapılmaktadır.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:132,
                  paths:'/ortopedi',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Ortopedi ve Travmaloji",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Ortopedi ve Travmaloji",
                    icerikContext:[
                      {
                        tag:"li",
                        icerik:"Spor yaralanmaları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kas iskelet sisteminin kırıklar ve yumuşak doku yaralanmaları dahil cerrahi ve cerrahi olmayan tedavileri",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Omurga kırık, yaralanma ve enfeksiyonları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Diz, kalça ve omuz ekleminin kireçlenmelerinin cerrahi müdahalesi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Her türlü artroskopik girişim",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Periferik sinir sıkışmaları (karpal tünel sendromu, ulnar tünel sendromu, posterior interosseöz sendromu)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kırıkların açık ve kapalı redüksiyonu",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kapalı kırık ameliyatları (minimal invaziv cerrahi)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ortopedik şekil bozukluklarını düzeltici ameliyatlar",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Her türlü eklem protezinin yerleştirilmesi (Kalça, dirsek, parmak, omuz... )",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Diz ve ayak bileği bağ ameliyatları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Pap-smear test (servikal yayma)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Bel kayması ve kırıklarının ameliyatları",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Uzman doktorumuz tarafından uygulanarak, teşhis tedavi ve operasyon sonrası kontrolleri kliniğimizde yapılmaktadır.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:133,
                  paths:'/cocuk',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Çoçuk Sağlığı ve Hastalıkları",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Çoçuk Sağlığı ve Hastalıkları",
                    icerikContext:[
                      {
                        tag:"li",
                        icerik:"0-15 yaşları arası tüm çocukluk dönemi hastalıkları ile ilgili muayene, takip ve tedaviler",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Büyüme gelişim takibi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Rutin ve özel aşılar",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Çocuk eğitimi danışmanlığı",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Beslenme ve emzirme danışmanlığı",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Yeni doğan tarama testleri",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Kliniğimizde uzman hekimimiz tarafından yapılmaktadır.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:134,
                  paths:'/acil',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Acil Servis",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Acil",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Acil Servis; kesiler, kırıklar, düşmeler, yüksek ateş, karın ağrıları, mide kanamaları, inme (felç), trafik kazaları, kalp krizi, kalp ritim bozuklukları, ani kalp durması, darplar, zehirlenmeler, intihar girişimleri vb birçok acil duruma ilk müdahale ve tedavi yapılan yerdir ",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Acil Servis’imizde her türlü hastaya ilk müdahale, ve acil hastalıkların tedavisi yapılabilmektedir. Ambulans servisi hem hasta nakli hem de hastane öncesi tıbbi bakım birimi olarak görev yapar. Ambulans ekibi, tecrübeli doktor, hemşire, acil teknisyeni ve şöförlerden oluşan personeli ile hizmet vermektedir. ",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Acil servisimizde, 3 gözlem odası, 1 müdahale odası, 2 müşaade odası mevcuttur.",
                        classname:"content-p",
                      }
                    ]
                  },
                },
                {
                  id:135,
                  paths:'/agiz',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Ağız ve Diş Sağlığı",
                  sidemenuVisible:true,
                  componenti:0,
                  subMenu:[
                    {
                      id:135256,
                      paths:'/imp',
                      birimAdi:"TIBBİ BİRİMLER ",
                      adi:"İmplantoloji ve Cerrahi Uygulamalar",
                      sidemenuVisible:true,
                      subMenu:[
                        {
                          id:262626131236,
                          paths:'/implant',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"İmplant",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"İmplant",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"İmplantoloji",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Diş kayıplarında tercih edilebilecek en iyi yöntem implanttır.
                                İmplantlar insan vücudu tarafından kolay kabuledilebilen ve çene kemiği içinde kök görevini gören titanyumdan yapılırlar.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`İmplantoloji denenmiş ve başarı sağlanmış bir tedavidir. Günümüz modern implantların %90’ı enaz 15 yıl kullanım sağlamaktadır. İmplantın başarısında kemiğin durumu çok önemlidir. Operasyon öncesinde hem klinik hem de radyolojik araştırma detaylı olarak yapılarak kemiğin durumu değerlendirilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Operasyon lokal anestezi altında yapılır ve bir implantın yerleştirilmesi yaklaşık 15-20 dakika alır.
                                Diş etinden flap kaldırılıp implant yerleştirildikten sonra diş etine tekrar dikiş atılır. İmpalantın yerleştirilmesinden sonra iyileşme 2-4 ay arası sürecektir. Kemik dokusu ve implantın kaynaştığı bu süre içinde beklemek gerekmektedir. Bu sürenin ardından implant kemik içine adapte olur. Daha sonra üst yapı yapılması için kapalı implantın üzeri açılır ve abutmentlar yerleştirilerek ölçü alınır. Daha sonra üst kuronlar hazırlanır, bu işlem 5-7 gün almaktadır.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:32424364,
                          paths:'/gom',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Gömük Diş Çekimi",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Gömük Diş Çekimi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Diş Çekimi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Basit Çekim",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Komplike Çekim ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Cerrahi Operasyon",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Diş çevresinde iyileşmeyen kronik iltihap",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dolgu ya da kuronla tamir edilemeyecek kadar diş dokusu kaybı olan dişler",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Çevre kemik dokusunda aşırı rezorbsiyon oluşmuş ve bu nedenle mobil hale gelmiş dişler",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Pozisyonu bozuk dişler",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Ortodontik tedavi için yer kazanılması gereken durumlarda diş çekimi yapılır.",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Diş çekimi öncesinde dijital röntgen çekilerek, olası komplikasyon riski minimize edilir.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kliniğimizde bütün komplike çekimler ve ağız içi cerrahi operasyonlar cerrahi uzmanı diş hekimleri tarafından yapılmaktadır.",
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                      ],
                      componenti:0,
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Deneme Başlığıdğr",
                        icerikContext:[
                          {
                            tag:"p",
                            icerik:"Teşhis ve tedavi plan",
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Bu nu bilmiyorum psidir.",
                            classname:"content-p",
                          }
                        ]
                      },
                    },
                    {
                      id:134546,
                      paths:'/kozmadis',
                      birimAdi:"TIBBİ BİRİMLER ",
                      adi:"Kozmetik Diş Hekimliği",
                      sidemenuVisible:true,
                      componenti:0,
                      subMenu:[
                        {
                          id:1515135,
                          paths:'/bleaching',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Bleaching",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Bleaching",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Bleaching",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Birçok araştırmaya göre dişler yüz estetiğini belirleyen ikinci en önemli kriterdir. Dişlerin renkleride kesinlikle biçim ve dizilişleri kadar önemlidir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Beyazlatma, parlak beyaz bir gülümsemeye sahip olmayı sağlayan en kolay ve ağrısız tedavidir. Basit ve güvenli olan bu tedavi son derece çarpıcı sonuçlar ortaya çıkarmaktadır. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kliniğimizde BDHF tarafından tavsiye edile, sonderece güvenli bir beyazlatma sistemi uygulanmaktadır. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Hastalarımız diş hekimi koltuğunda rahat bir şekilde uzanıp müzik dinlerken beyazlatma işlemi şu işlemlerde gerçekleştirilir.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"İlk adımda hastanın dudaklarını ve dişetlerini özel bariyerlerle izole edilerek beyazlatma ilacının dişetileri ve yumuşak dokularla teması engellenir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Ardından beyazlatma jeli dişlerin yüzeyine uygulanarak ışınla aktivasyonu sağlanır. Daha sonra jel diş yüzeyinden uzaklaştırılarak renk değişimi kontrol edilir. Bu işlem istenilen renge ulaşılıncaya kadar birkaç kez tekrar edilebilir.",
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:34621689,
                          paths:'/porselen',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Porselen Laminate",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Porselen Laminate",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Porselen Laminate",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Porselen laminate dişlerinizin şeklini, boyutunu, rengini ya da pozisyonunu değiştirerek gülüşünüzü güzelleştirmek amacıyla hazırlanana ince porselen bir tabakadır. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"En az miktarda diş materyali aşındırılarak, uzun süreli, mükemmel bir görünüşün sağlandığı koruyucu bir tekniktir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Özel rezin simanlarla dişe yapıştırılırlar ve düşmeleri çok zordur. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Porselen yüzeyi lekelenmelere, boyanmalara karşı oldukça dayanıklıdır, estetik ve doğal bir görünüm sağlarlar ve de diş etiyle çok uyumludurlar. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Laminatelerin hazırlanması 1 hafta kadar sürmektedir, bu sürede de 2-3 randevu gerekmektedir:
                                İlk randevuda diş yüzeylerinden bir miktar aşındırma yapılarak dişler laminate için hazırlanır. Ardından ölçü alınıp geçici restorasyon materyali ile açık dentin yüzeyi kapatılır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"İkinci randevuda geçiciler kaldırıldıktan sonra laboratuar tarafından hazırlanmış olan laminateler prova edilip, hasta ve hekim hemfikir ise özel resin simanlarla yapıştırılır. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Tedavi planları kişiden kişiye değişiklik göstermektedir. Sizin için en uygun tedavi seçeneği ve planlaması için dişhekimine muayene olmanız şarttır.",
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:5848237,
                          paths:'/bonding',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Bonding",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Bonding ve Estetik Dolgular",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Bonding",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Dental bonding estetik amaçla kullanılan bir tür dolgu materyalidirDişlerdeki küçük boşlukları kapatmak, dişleri yeniden şekillendirmek ve mine yüzeyinde oluşan doğal hasarı kapatmak için rutin olarak kullanılan estetik restorasyon materyalleridir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kompozit dolgu materyalinin çeşitli renk tonları vardır. Böylece dişinizin kendi rengine kolaylıkla uyum sağlar. Geleneksel amalgam dolgularınız kompozit dolgularla değiştirilebilir. Böylece dişleriniz sağlıklı bir ağız ortamıyla beraber estetik ve doğal bir görünüşe sahip olmuş olur. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kliniğimizde yüksek kalitede, dayanıklı ve kuvvetli kompozit materyali kullanmaktayız. Kullandığımız tüm malzemeler FDA, ADA ve Türk Sağlık Bakanlığı tarafından onaylıdır.",
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:9840059,
                          paths:'/smile',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Smile Design",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Smile Design",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Smile Design",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Gülüşünüzden memnun musunuz?",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dişlerinizin şekli ve rengi sizi rahatsız ediyor mu?",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Hiç başka birinin gülüşüne sahip olmak istediniz mi?",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dişetlerinizin görüntüsü sizi mutlu ediyor mu?",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Gülmek,yüzümüzdeki pek çok kas grubunun çalışmasıyla oluşan ve dişlerimizin göründüğü bir mimiktir. Gülüşümüzü etkileyen pek çok faktör vardır. Güzel bir gülüş için öncelikle yüz biçiminizle ,dudak yapınızla uyumlu dişlere sahip olmanız gerekir. İşte gülüş dizaynı, size kişisel özellikleriniz ve isteklerinizle doğallık ve fonksiyonu birleştirerek sizin için en ideal gülümsemeyi oluşturmak için vardır. Pırıl pırıl bir gülümseme hepimizin hoşuna gider. O kişi hakkında olumlu hisler hissetmemizi sağlar. Araştırmalar gösteriyor ki, dişler gözlerden sonra en önemli ikinci estetik unsurdur. Ahenkli bir gülümseme iletişimde her zaman avantaj sağlar. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"GÜLÜŞÜMÜZÜ ETKİLEYEN FAKTÖRLER",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"CİNSİYET",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"YÜZ ŞEKLİ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"YAŞ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"DUDAKLAR",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"DİŞ ETLERİ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"DİŞLER",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`1-CİNSİYET: Kadın ve erkek anatomisi birbirinden farklıdır. Erkeklerde yüz hatları daha keskin ve belirgindir.Alın burun, çene ucu orantısı kadın yüzüyle farklılıklar gösterir.Kadınlarda geçişler daha yumuşak burun ve kaş kemerleri daha siliktir. Dişlerde de aynı paralellik vardır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kadınlarda:",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Gülme hattı yukarı doğru kavislidir ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dişlerin köşeleri daha yumuşak döner. ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Komşu dişlerin köşeleri arasında minik aralıklar vardır.",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Ortadaki iki diş yandaki dişlerden biraz daha uzundur. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Erkeklerde:",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Gülme hattı daha düzdür. ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dişlerin hatları daha belirgindir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Komşu dişler daha düz bir hatta birleşirler ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`2-YÜZ ŞEKLİ: Yüz şeklinizle diş formlarınız arasında benzerlikler bulunur. Genellikle uzun yüzlü kişilerde diş formları da uzun; kare veya yuvarlak yüzlü kişilerde diş formları da kare veya yuvarlak olur. Klasik diş hekimliğinde bu benzerlikler korunmaya çalışılarak restorasyon yapılırdı. Ancak estetik yönden bir şeyleri değiştirmek istediğinizde bu benzerlikleri tersine çevirerek farklı ifadeler veren illüzyonlar elde edilebilir. Örneğin uzun yüzlü bir kişiye dikdörtgen formda uzun dişler yapılırsa yüzü olduğundan da uzun görünecektir. Oysa oval veya daha geniş formlar denenerek yüzdeki hoş olmayan uzunluk kamufle edilebilir. Veya yuvarlak yüzlü bir kişiye daha ince uzun formda dişler yapılarak yüzünün daha ince görünmesi sağlanabilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`3-YAŞ: Yaşlanmaya karşı yürütülen savaş her yıl milyonlarca insanı da içine alarak büyüyor. Kadın ve erkek fark etmeden hepimiz genç ve güzel görünmeyi sürdürmek istiyoruz.Kozmetik dişhekimliği bu konuda oldukça önemli yardımlar yapabilir.Gülüşümüz yüzümüzdeki en önemli gençlik kriterlerinden biridir. Yaşla birlikte dişlerimizin renginde bir koyulaşma,boylarında kısalma meydana gelir.

                                Dokuların sarkmasıyla beraber üst dudağımız da yerçekimi etkisiyle aşağıya doğru sarkar. Dikey boyutumuz kısaldığı için çene ucu-burun ucu birbirine olması gerekenden çok yaklaşır. Konuşurken ve gülerken üst dişlerimizin daha az, alt dişlerimizin daha çok, gözükmesi sonucunda daha yaşlı bir ifadeye sahip oluruz. Birçok kişi çok yanlış bir inanışla yaşlanınca sadece takma dişler yaptırabileceklerini sanıyorlar. Oysa dikey boyut yükseltilmesi, diş beyazlatma, kozmetik düzenleme, bonding, laminate veneer, implant gibi yöntemlerle 20 yıl önceki gülüşünüze kavuşmanız hiç de hayal değildir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`4-DUDAKLAR: Tıpkı bir çerçevenin tabloyu şekillendirdiği gibi dudaklar da dişlerimizi ve gülüşümüzü şekillendirirler.Gülüş dizaynı ;dudakların kalın,ince, uzun,kısa vs. olmalarına göre düzenlenerek var olan kusurlar kapatılabilir `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`5-DİŞETLERİ: Dişetleri dişlerimizi en yakından çerçeveleyen aksesuarlardır. Dişlerimiz güzel olsa bile sağlıksız(şiş, kırmızı ve parlak)dişetleri gülüşümüzün çirkin görünmesine neden olurlar.
                                bazen de dişetlerimiz sağlıklıdır ama gülünce gereğinden fazla gözükürler.Ya da dişeti çekilmesi oluşmuştur ve gözükmelerini istediğimiz kadar dişetimiz yoktur

                                Bu gibi durumlarda gülüşümüzde oluşan rahatsız edici görüntüler Varolan dişetlerine sağlık kazandırılarak ve ya dişetlerine küçük estetik müdahaleler yapılarak düzenlenebilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`6-DİŞLER: Günümüzde modern dişhekimliği konseptinde estetik sınırlar içinde dişlerimizi güzel göstermek için yapılabilecek çok fazla alternatifimiz var.Önemli olan sorunları doğru tespit edebilmek ve soruna yönelik, hedefi bulan,minimal müdahale ile maksimum estetiği sağlayabilen tedavileri seçmektir.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:591924,
                          paths:'/dispirlantasi',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Diş Pırlantası",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Diş Pırlantası",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Diş Pırlantası",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Çeşitli şekillerde ve renklerdeki küçük taşlardır. Bazısı kıymetli metal ve taşlardan yapılmış olabilir (altın-gümüş-pırlanta) ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Gülüşünüzün size özel ve süslü olmasını sağlarlar.
                                Diş yüzeyine özel diş bağlayıcılarıyla yapıştırılırlar.
                                Yapıştırılmaları sırasında dişten herhangi bir aşındırma yapılmaz ve eğer istenirse daha sonra diş yüzeyinden tamamen uzaklaştırılabilirler.`,
                                classname:"content-p",
                              }
                            ]
                          },
                        },
                      ],
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Deneme Başlığıdğr",
                        icerikContext:[
                          {
                            tag:"p",
                            icerik:"Deneme psidir.",
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Bu nu bilmiyorum psidir.",
                            classname:"content-p",
                          }
                        ]
                      },
                    },
                    {
                      id:13428286,
                      paths:'/rutin',
                      birimAdi:"TIBBİ BİRİMLER ",
                      adi:"Rutin Dental Tedaviler",
                      sidemenuVisible:true,
                      componenti:0,
                      subMenu:[
                        {
                          id:7848373,
                          paths:'/pedodonti',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Pedodonti",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Pedodonti - Çocuklarda diş tedavisi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Pedodonti - Çocuklarda diş tedavisi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Ağız ve diş sağlığı bebeklikten itibaren çok önemlidir. Her öğün beslenmenin ardından bebeğin yanak- dil- dişeti kretleri ve çevresi temiz gazlı bezle silinip temizlenmelidir. İlk süt dişlerinin sürmesiyle birlikte, bu temizlik işlemine dişler de eklenir. Aksi takdirde kalan yemek artıkları ağız içerisinde kötü kokuya, dişler üzerinde de çürüğe neden olur. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Çocuklarla tedavi her zaman yoğun ilgi ve sabır ister. İlk diş hekimi tecrübesi hayat boyu etkili olacağı için çocuk hasta ile karşılıklı uzlaşma, onu tedavi için doğru motive etme çok önemlidir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Süt dişleri, alttan gelecek daimi dişlere sürme rehberliği yaparlar. Değişim zamanı gelinceye kadar kendi pozisyonlarında, sağlıklı bir şekilde korunmaları gerekir. Çürümeye başlayan süt dişlerine dolgu veya kanal tedavisi gibi koruyucu tedaviler yapılarak, gerek süt dişi ve çevresindeki komşu dişler, gerekse sürecek olan daimi dişlerin ve ağız içi ortamın sağlıklı kalmasına çalışılır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Kliniğimizde her türlü pedodontik müdahale yapılabilmektedir:`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Oral hijyen eğitimi`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Süt ve daimi diş dolguları`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Koruyucu tedaviler(fissür sealant uygulaması, flor uygulamaları)`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Pedodontik kuron`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Yer tutucular`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Kanal tedavisi`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Çekim`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Ortodonti`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:56626,
                          paths:'/kanal',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Kanal Tedavisi",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Kanal Tedavisi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Kanal Tedavisi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Kanal tedavisi pulpa dokusu (dişin kan ve sinir içeren kısmı) kazayla, çürükle ya da enfeksiyonla açığa çıktığı zaman yapılan tedavidir. Özenli çalışmayı gerektiren, zaman alan bu tedavi 2-3 randevuyu kapsar. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kanal ve köklerin sayısı kişiden kişiye ve dişten dişe değişiklik gösterir. Tedavi kök kanallarının içindeki pulpa dokusunun temizlenmesi, kök kanal duvarlarının genişletilmesi ve en son temiz, sağlıklı hale gelen kök kanallarının doldurulması şeklindedir.",
                                classname:"content-p",
                              }
                            ]
                          },
                        },
                        {
                          id:5848237,
                          paths:'/kuron',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Kuron Köprüler",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Kuron Köprüler",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Kuron Köprüler",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Metal destekli porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Değerli metal destekli porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Zirkon porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Tüm porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Diş kuronları çürümüş, kırılmış ya da çatlamış bir dişi onarmak için hazırlanırlar. Ayrıca dişlerin görünüşünü güzelleştirmek, ark içerisindeki pozisyonlarını düzeltmek için de kullanılırlar. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Köprüler dişlere yapıştırılan sabit apareylerdir.
                                Bir ya da daha fazla diş eksikliğinde hazırlanırlar.
                                Doğal dişlerle aynı görünüme ve fonksiyona sahiptirler. Dişlerin hizasını korudukları gibi iyi bir fonksiyon ve estetik sağlarlar.
                                Kayıp diş ya da dişlerin yanında yer alan komşu dişler (ya da implantlar) köprü ayağı olarak kullanılırlar.
                                `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Köprü bu komşu dişlerin üzerine yapıştırılmaktadır.
                                Köprünün kayıp dişleri içeren parçası ortada 'pontik' adı verilen kısımdır ve diş eti üzerine oturur.

                                Köprü ayağı olarak kullanılacak olan komşu dişlere ait diş etlerinin ve çevrelerindeki kemik dokusunun sağlığı köprü başarısında çok önemli bir faktördür.
                                Bu nedenle köprü yapımı öncesinde dokular dikkatlice incelenir, Kuronlar farklı materyallerle hazırlanabilir,
                                `,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Metal destekli porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Değerli metal destekli porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Zirkon porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Tam porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"METAL DESTEKL PORSELEN KURONLAR: ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Klasik tip kuronlardır.
                                Kuronun içinde kullanılan metaller kuronun daha güçlü olmasını sağlar. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"DEĞERLİ METAL DESTEKLİ PORSELEN KURONLAR: ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Kuron içinde farklı içerikli metaller kullanılarak kuron daha az allerjik ve diş etiyle daha uyumlu hale getirilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"TAM PORSELEN KURONLAR: ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Daha çok ön dişlerde, estetiğin yüksek oranda sağlanmasını istediğimiz durumlarda kullanılan metalsiz modern tekniktir. Yapıştırılmaları için özel resin siman sistemleri kullanılmaktadır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"ZİRKONYUM: ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`İyi estetik sonuç elde ederken aynı zamanda güçlülük ve dayanıklılık sağlamak için zirkonyum kullanılır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Zirkonun dayanıklılığı ve translusensisi doğal dişlerle çok fazla benzerlik gösterdiği için estetik diş materyali olarak tercih edilirler.

                                Diş hekimlerimizin dikkatli muayene ve incelemelerinden sonra hastaya kendisine uygulanabilecek tedavi seçenekleri anlatılır, bu tedavi seçenekleri tek tek ele alınıp tartışılarak hastaya en uygun tedavi planlanır.
                                `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Muayene sonrasında tedavi şu aşamaları izler: `,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Dişlerden aşındırma yapılarak kuron için gerekli yer hazırlanır `,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Ölçü alınır`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Geçici köprü/kuron hazırlanır ve yapıştırılır`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Ölçü,kron/ köprünün hazırlanması için diş teknisyenlerine yollanır`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Bir sonraki randevuda, geçici restorasyon çıkarıldıktan sonra laboratuarda hazırlanmış olan kuron/köprü ağız içinde prova edilir`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Kuron/ Köprü cilalanıp son bitirme işlemi yapılır.`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Doğru kapanış ve pozisyonda köprü dişlere yapıştırılır.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:92161,
                          paths:'/protez',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Hareketli Protezler",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Hareketli Protezler",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Hareketli Protezler",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik: `Hareketli protezler ağızdaki kayıp dişlerin yerine akrilik rezinden yapılan takıp çıkarılabilen apareylerdir. Ağızda kalan dişler köprü yapımı için yeterince sağlıklı değilse ya da uygun pozisyonda değilse, kayıp dişlerden oluşan boşlukları doldurmak için parsiyel protez yapımı tercih edilmektedir.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik: `Protezin metal isketli ya da metal iskeletsiz olması ağzın durumuna, dişlerin sayısına bağlıdır. Tamamen dişsiz hastalarda total protezler yapılmaktadır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik: `Dişlerin şekil ve renginin belirlenmesinde de akrilik dişlerin çok çeşitli renk ve şekil seçeneği olması sayesinde hasta için en uygun olanı tercih edilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik: `Eksik dişler yüzünden kaybedilen estetik görünümü, çiğneme fonksiyonunu, konuşma problemlerini tekrardan hastaya kazandırmasına rağmen hastaların harekeli protezleri kullanmaya alışmaları kolay değildir, zaman alıcıdır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik: `Diş hekimlerimiz tedavi planınız yapılırken, size uygulanabilecek tüm protez seçenekleri hakkında sizi bilgilendirecektir.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:591924,
                          paths:'/periodontal',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Peridontal Tedavi",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Peridontal Tedavi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Peridontal Tedavi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Sağlıklı dişlere ve sağlıklı bir ağıza sahip olmada ilk adım sağlıklı diş etleridir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Diş yüzeyinde oluşan, bakteri içeriği zengin plak tabakası tükrük içeriğiyle birleşerek sertleşir ve diş taşı haline gelir. Diş taşları dişlerin yüzeyinde yer alabileceği gibi diş etinin üstünde ve altında da birikebilir.
                                Sigara, çay, kahve vb. gıdalar diş ve diş taşı yüzeylerinde renklenmeye neden olur. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Dişeti altında biriken diş taşları dişetini kolaylıkla iltihaplı hale getirir. Diş taşları ve dişetlerindeki iltihap zamanla çevre kemik dokusunda yıkıma sebep olur. Bu durum dikkate alınmazsa dişler çevresindeki kemik desteğini kaybederek zamanla mobil hale gelir. Önlem alınmadığı takdirde bu yıkım dişlerin ağızdan düşmesiyle sonlanır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Plak içinde bulunan bakteriler sadece diş ve dişetleri için tehlikeli değildir, bunlar akciğer, kalp gibi yaşamsal organlar için de öldürücü olabilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`İyi bir ağız hijyeni ,düzgün ağız bakımı ve sağlıklı dişetleri, yapılacak olan diğer restorasyonların başarısı için ilk adımdır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Dişeti iltihabının tedavisi diş taşlarının uzaklaştırılmasıyla mümkündür.

                                Kliniğimizde uygulanan dişeti tedavilerinde, ultrasonik temizleyici cihazlar ve özel el aletleriyle,gerek diş yüzeyindeki plağı ,gerekse dişeti altındaki, diş taşlarını temizliyor ve lekelenmeleri ortadan kaldırıyoruz. Ayrıca çok derin diştaşları ve tortuları da derin küretajla temizliyoruz. Son olarak kullandığımız özel macunlarla diş yüzeylerini fırçalayarak diş yüzeylerinin parlak ve pürüzsüz hale gelmesini sağlıyoruz. Bu şekilde pürüzsüz hale gelen diş yüzeylerine plağın tekrardan tutunması da zorlaşmış oluyor.

                                Bu prosedür kanamasız, sağlıklı dişetlerine sahip olmanızı amaçlamaktadır ve dişetlerinin durumuna göre bazen birkaç seans sürebilmektedir. Bu tedavi her 6 ayda bir tekrarlanmalıdır.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:65281321,
                          paths:'/konservatif',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Konservatif Tedavi",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Konservatif Tedavi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Konservatif Tedavi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Sağlıklı dişlere ve sağlıklı bir ağıza sahip olmada ilk adım sağlıklı diş etleridir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Diş yüzeyinde oluşan, bakteri içeriği zengin plak tabakası tükrük içeriğiyle birleşerek sertleşir ve diş taşı haline gelir. Diş taşları dişlerin yüzeyinde yer alabileceği gibi diş etinin üstünde ve altında da birikebilir.
                                Sigara, çay, kahve vb. gıdalar diş ve diş taşı yüzeylerinde renklenmeye neden olur. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Dişeti altında biriken diş taşları dişetini kolaylıkla iltihaplı hale getirir. Diş taşları ve dişetlerindeki iltihap zamanla çevre kemik dokusunda yıkıma sebep olur. Bu durum dikkate alınmazsa dişler çevresindeki kemik desteğini kaybederek zamanla mobil hale gelir. Önlem alınmadığı takdirde bu yıkım dişlerin ağızdan düşmesiyle sonlanır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Plak içinde bulunan bakteriler sadece diş ve dişetleri için tehlikeli değildir, bunlar akciğer, kalp gibi yaşamsal organlar için de öldürücü olabilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`İyi bir ağız hijyeni ,düzgün ağız bakımı ve sağlıklı dişetleri, yapılacak olan diğer restorasyonların başarısı için ilk adımdır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Dişeti iltihabının tedavisi diş taşlarının uzaklaştırılmasıyla mümkündür.

                                Kliniğimizde uygulanan dişeti tedavilerinde, ultrasonik temizleyici cihazlar ve özel el aletleriyle,gerek diş yüzeyindeki plağı ,gerekse dişeti altındaki, diş taşlarını temizliyor ve lekelenmeleri ortadan kaldırıyoruz. Ayrıca çok derin diştaşları ve tortuları da derin küretajla temizliyoruz. Son olarak kullandığımız özel macunlarla diş yüzeylerini fırçalayarak diş yüzeylerinin parlak ve pürüzsüz hale gelmesini sağlıyoruz. Bu şekilde pürüzsüz hale gelen diş yüzeylerine plağın tekrardan tutunması da zorlaşmış oluyor.

                                Bu prosedür kanamasız, sağlıklı dişetlerine sahip olmanızı amaçlamaktadır ve dişetlerinin durumuna göre bazen birkaç seans sürebilmektedir. Bu tedavi her 6 ayda bir tekrarlanmalıdır.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                      ],
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Deneme Başlığıdğr",
                        icerikContext:[
                          {
                            tag:"p",
                            icerik:"Deneme psidir.",
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Bu nu bilmiyorum psidir.",
                            classname:"content-p",
                          }
                        ]
                      },
                    },
                  ],
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Ağız ve Diş Sağlığı",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Teşhis ve tedavi plan",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Bütün muayene ve teşhisler bilgisayar teknolojisiyle ve tamamen steril bir ortamda yapılmaktadır. ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Tüm detayları kontrol etmek için özel bir ağız içi kamera sistemi kullanmaktayız.",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Dijital röntgen cihazımızla sizin için en rahat koşullarda ve de çok kısa sürede dişlerinizden röntgen çekmekteyiz. ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Acil olmayan durumlarda muayene için ücret alınmamaktadır.",
                        classname:"content-p",
                      }
                    ]
                  },
                },
                {
                  id:136,
                  paths:'/uroloji',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Üroloji",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Üroloji",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Kadında ve erkekte üriner sitemin dahili ve cerrahi hastalıklarıyla ilgilenen tıp alanıdır.",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Böbrek enjeksiyonları,taşları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Böbrek tümörleri",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Böbrek travmaları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Üreterin taş,enjeksiyon ve travmaları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mesane rahatsızlıkları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Sistit,kanlı sistit",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Prostat hastalıkları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"İdrar yolarında oluşan kistler ve buna bağlı oluşan komplikasyonlar",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Sünnet",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Bel soğukluğu",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"HPV gibi virütik hastalıklar",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Uzman doktorumuz tarafından işlemlerin teşhis tedavi ve operasyon sonrası kontrolleri kliniğimizde yapılmaktadır.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:137,
                  paths:'/kardiyoloji',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Kardiyoloji",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Kardiyoloji",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:`Kalp ve dolaşım sistemi hastalıklarını inceleyen bilim dalıdır. Kardiyoloji biliminin tanı ve sağaltımını (tedavi) sağlamak için çalıştığı hastalıklar arasında, günümüzün en önemli sağlık sorunları arasında yer alan bazı hastalıklar bulunmaktadır. Bu hastalıkların birkaçı şöyle sıralanabilir:`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Hipertansiyon",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Aterosklerotik kalp hastalıkları (koroner arter hastalığı gibi)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kalp ritmi bozuklukları (aritmiler)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Doğuştan kalp hastalıkları",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:`Nefroloji, endokrinoloji gibi dalların da ilgi alanına giren yüksek tansiyon, çeşitli hastalıklara bağlı olarak gelişen kalp yetmezliği, doğuştan ya da çeşitli hastalıklara bağlı olarak gelişen kalp kapak hastalıkları ve benzeri pek çok hastalık da kardiyolojinin tanı ve sağaltımı için uğraştığı hastalıklardandır.
                        Kardiyolijinin kullandığı tanı araçlarından bazıları şunlardır:`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ekokardiyografi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Elektrokardiyografi (EKG) ve ilgili tanı yöntemleri:",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kalp stres testi ('efor testi' ya da 'eforlu EKG' olarak da bilinir)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Taşınabilir EKG aygıtı ('Holter monitörü' olarak da bilinir)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kandaki kalp enzimlerinin düzeyleri",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Koroner anjiyografi",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Uzman doktorumuz tarafından işlemlerin teşhis tedavi ve operasyon sonrası kontrolleri kliniğimizde yapılmaktadır.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:138,
                  paths:'/gast',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Gastroenteroloji",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Gastroenteroloji",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Yemek borusu,mide,ince bağırsaklar,kalın bağırsaklar,karaciğer,safra kesesi, pankreas organlarını konu alan söz konusu bilim dalı; bu organlarınülser,gastrit,sarılık,siroz,spastik kolon(irritabl bağırsak sendromu: İBS),safra kesesi taşlarıve iltihabı, mide-bağırsakkanserleri,hemoroid(mayasıl,basur) gibi bilinen hastalıklarına çözüm arar. Teşhis amacıyla farklı tiplerde endoskoplar kullanılır. Yemek borusu, mide ve oniki parmak bağırsağını görüntülemek için özefagogastroduodenoskop (genel kullanımda kısaca endoskop denilir), kalın bağırsak için ise kolonoskop kullanılır.",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Gastroenterolojik hastalıklardaki klinik belirti ve bulgulardan başlıcaları şunlardır:",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kabızlık",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"İshal",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kusma",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Bulantı",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Karın ağrısı",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Retrosternal yanma hissi (Göğüs kemiği arkasında yanma hissi)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ağza acı su gelmesi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Karın ağrısı",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Uzman doktorumuz tarafından teşhis tedavi ve operasyon sonrası kontrolleri kliniğimizde yapılmaktadır.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:139,
                  paths:'/genel',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Genel Cerrahi",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Genel Cerrahi",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Vücutta sistemik ve yerel sorunların cerrahi yöntemlerle tedavisi yanında, genel prensipler (yara iyileşmesi, yaralanmaya metabolik ve endokrin cevap gibi) konuları içeren ve gelişimleri açısından pek çok cerrahi ve temel tıp dalları etkilemiş bir teknik disiplindir. Genel olarak yemek borusu, mide, ince bağırsak, kolon, karaciğer, pankreas, safra kesesi ve safra yolları dahil olmak üzere karın içeriğine odaklanan bir cerrahi uzmanlık branşıdır.",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Bugün 'Genel Cerrahi' denince:",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Tiroit cerrahisi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Meme cerrahisi",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Yemek borusu",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mide",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"İnce ve kalın barsaklar",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Makat hastalıkları (hemorroid...)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Karaciğer",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Pankreas",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Safra kesesi ve safra yolları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Fıtık cerrahisi",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"anlaşılmaktadır.",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Uzman doktorumuz tarafından cerrahi işlemlerinteşhis tedavi ve operasyon sonrası kontrolleri kliniğimizde yapılmaktadır.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:1991,
                  paths:'/deri',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Deri Hastalıkları",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Deri Hastalıkları",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Ciltte görülen hastalıklardır. Sayılmayacak kadar çok deri hastalığı vardır. Deri hastalıklarına genel olarak dermatoz, ilgili bilim dalına da dermatoloji ismi verilir. Deri hastalıkları hakkında genel bir fikir edinebilmek için birkaç bölüme ayırmak mümkündür.",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Deri Enfeksiyonları (mantar hastalıkları, siğiller)",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Bezi Hastalıkları (sivilce, saçlı deride kepeklenme)",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Allerjik Deri Hastalıkları (atopik egzema, kontakt egzema [temas egzeması],kozmetik alerjisi, böcek ısırıkları)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"İlaç Reaksiyonları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ürtiker (Kurdeşen)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Psoriasis (Sedef Hastalığı)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Vitiligo",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Behçet Hastalığı",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Deri Tümörleri",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Yanıklar",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Saç Hastalıkları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Tırnak Hastalıkları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Aşırı Terleme",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Cinsel Yolla Bulaşan Hastalıklar (Frengi).",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:1992,
                  paths:'/dahiliye',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Dahiliye",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Dahiliye",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Dahiliye (İç hastalıkları) bölümümüzde tüm dahili hastalıkların tanı ve tedavisi yapılmaktadır. İç hastalıkları departmanının alt birimleri olarak;",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:1993,
                  paths:'/kbb',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Kulak Burun Boğaz Hastalıkları",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Kulak Burun Boğaz Hastalıkları",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"KBB departmanına başvuran hastalarda endovisuel sistem ile muayeneleri yapılmakta ve gerekli arşivleme yapılmaktadır.",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kulak hastalıkları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Boğaz hastalıkları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Burun hastalıkları ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Uyku bozuklukları( burun eğriliklerine bağlı)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Horlama şikayetleri",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Baş ve boyun bölgesi tümör ve rahatsızlıkları",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ses ve konuşma bozuklukları",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"bölümde tanısı konan ve tedavi edilen hastalıkların başında gelmektedir.",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Burun içi eğriliği (deviasyon),",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Bademcik",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Geniz eti( adenoid), ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kulak operasyonları tüp yerleştirme",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Endoskopik sinüzit ameliyatı",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"gibi standart cerrahilerin yanı sıra ileri ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Endoskopik ameliyatlar, ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Baş ve boyun bölgesi tümörlerinin ameliyatları ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kulak zarı tamiri, ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"İşitme kayıpları ve kulak iltihaplarına yönelik ameliyatlar, ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ses hastalıklarına yönelik cerrahi müdahaleler ",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Odiyolojik ve timpanometrik araştırmaların da gerçekleştirildiği birimlerimizde diğer branşlarla koordineli olarak (özellikle Göğüs Hastalıkları,Dermatoloji )allerji testleri de yapılmaktadır.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
              ],
              componenti:0
            },
            //130bitis
            //140
            {
              id:140,
              paths:'/unitelerimiz',
              adi:"Ünitelerimiz",
              sidemenuVisible:true,
              exact:false,
              subMenu:[
                {
                  id:141,
                  paths:'/bla',
                  birimAdi:"Ünitelerimiz ",
                  adi:"Radyoloji Hizmetleri",
                  sidemenuVisible:true,
                  exact:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Radyoloji Hizmetleri",
                    icerikContext:[
                        {
                            tag:"li",
                            icerik:"Genel radyoloji",
                            classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Bilgisayarlı tomografi",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Mamografi",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Panoramik röntgen",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Dijital floroskopi",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Manyetik rezonans görüntüleme",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Ultrasonografi",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Renkli Doppler ultrasonografi",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Anjiografi",
                          classname:"content-p",
                        },
                    ]
                  },
                },
                {
                  id:14846846,
                  paths:'/diloymgfkdjhs',
                  birimAdi:"Ünitelerimiz ",
                  adi:"Laboratuvar Hizmetleri",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Laboratuvar Hizmetleri",
                    icerikContext:[
                      {
                        tag:"li",
                        icerik:"Klinik Biyoloji",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Tümör Belirteçleri(PSA,Kalsitonin, Koryonik gonodotropin (hCG),AFP,CEA,CA19-9,CA125,CA15-3,CA72-4 vb.)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Hormon Testleri(TSH,LH,FSH,ACTH,T3,T4,ADH,FT3,FT4,HOMO-IR vb.)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Lipid Metabolizması ile ilgili testler (LDL kolesterol,HDL kolesterol,Kolesterol Total vb.)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mikrobiyoloji",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Hematoloji",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Moleküler Mikrobiyoloji",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Alerji Testleri",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:52661,
                  paths:'/derma',
                  birimAdi:"Ünitelerimiz ",
                  adi:"Derma Kozmetik",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Derma Kozmetik",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:`Cilt, saç ve vücut bakımı için kullanılan, ilaç teknikleri kullanılarak geliştirilmiş, üretilmiş ve dermatolojik klinik testlerden geçirilmiş cilt ürünleri. Dermokozmetik ürünler sadece eczanelerde satılır ve dermatoloji veya plastik cerrahi uzmanları tarafından çeşitli cilt problemlerinin çözümü için ya doğrudan ya da ilaç tedavisine destek olarak reçete edilirler. Çoğu zaman da ilaç tedavisinin yan etkilerini azaltmak ya da gidermek için destekleyici amaçla kullanımları söz konusu olabilir. Örnek : akne tedavisi , sedef tedavisi. Ancak dermokozmetik ürünlerin sadece eczanelerde satılıyor olması , eczanelerde satılan her cilt bakım ürününün dermokozmetik olduğunu anlamına gelmez. Bazı cilt bakım ürünleri eczanede satılıyor olmasına rağmen "eczane kozmetiği" olarak tanımlanmalı ve sınıflandırılmalıdır.`,
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:8372,
                  paths:'/dermalaz',
                  birimAdi:"Ünitelerimiz ",
                  adi:"Dermatolojik Lazer Uygulamaları",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Dermatolojik Lazer Uygulamaları",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:`Kılcal Damar ve Bacak varisleri: Aşırı kiloya, bazı damar hastalıklarına, yıllar boyunca uzun süre ayakta kalmaya bağlı veya yapısal olarak yüzeysel bacak toplardamarlarında ortaya çıkan şişlik-lerdir. Tedavisi kliniğimizde uzman hekimler eşliğinde yapılmaktadır.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:`Epilasyon : İstenmeyen kılların lazerle yakılarak yok edilmesi esasına dayanır. Her cilt tipinde ve her mevsim lazerimizle tedavi edilmektedir.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:`Akne : Özellikle ergenlik döneminde daha çok olmak üzere her yaşta ortaya çıkan yüzdeki sivilce-lerdir. Değişik büyüklükte ve yoğunlukta olabilirler. Dermatoloji doktorumuzun belirleyeceği bazı akne hastaları lazerle tedavi olabilmektedir.`,
                        classname:"content-p",
                      }
                    ]
                  },
                },
                {
                  id:969,
                  paths:'/dishiz',
                  birimAdi:"Ünitelerimiz ",
                  adi:"Diş Hizmetleri",
                  sidemenuVisible:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Diloy Diloy",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Diloy Diloy",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Bu nu bilmiyorum psidir.",
                        classname:"content-p",
                      }
                    ]
                  },
                },
                {
                  id:9529,
                  paths:'/lazer',
                  birimAdi:"Ünitelerimiz ",
                  adi:"Lazer Operasyon ve Bölgesel İncelme",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Lazer Operasyon ve Bölgesel İncelme",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Lazer Epilasyon",
                        classname:"content-p-big",
                      },
                      {
                        tag:"p",
                        icerik:`Lazer epilasyonun başarılı, yan etki oluşturmadan uygulanması ve sizi negatif olarak etkileyecek durumlarda uygulanmaması için mutlaka dermatolog kontrolündeki bir hastaneye veya merkeze başvurun. Lazer epilasyon işleminizi; sağlık bakanlığı tarafından ruhsatlandırılmış sağlık kuruluşlarında, uzman doktor denetiminde, uluslararası normlara uygun (Amerikan FDA onayı, Avrupa CE belgesi) lazer epilasyon cihazlarında gerçekleştirmeniz sağlığınız açısından çok önemlidir.
                        Dermatoloğunuzun bilimsel verileri önde tutarak ve ticari kaygıları göz ardı ederek size en uygun lazer epilasyonu önerecektir.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Lazer epilasyon nasıl etkili olur?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`Lazer epilasyon kılın kalınlığına ve rengine odaklanır. Lazer rengi nedeniyle kıl kökleri tarafından emilir, yüksek enerji nedeniyle kökler ısınır ve yanar. Bu şekilde başarılı bir lazer epilasyon sonrasında kökler dökülür ve büyüme aşamasında olan kökler bir daha kıl üretemeyecek şekilde hasar alır.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Lazer epilasyon başarılı mıdır?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`Lazer epilasyon uygulamalarında başarılı sonuç elde etmek için cilt rengi ve kıl yapısına uygun lazer tipi ve dozu seçilmelidir. Seansların düzenli aralıklarla tekrarlanması, sonucun daha başarılı ve hızlı olmasını sağlamaktadır. Lazer epilasyon FDA tarafından onaylı bir tedavi olup, uygulanan bölgede kılların %80 inin yok olması tedavide tam başarı olarak kabul edilmektedir.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Lazer epilasyon güvenli midir?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`Lazer epilasyon tekniği yaklaşık 30- 40 yıldır güvenle tıpta kullanılmaktadır. Son 10- 15 yıldır da lazer epilasyon yaygın şekilde kullanılmaya başlanmıştır. Ayrıca lazer epilasyonun kanser yapıcı etkisi yoktur. Dermatoloji uzmanının değerlendirmesi mutlaka gereklidir. Bazı riskli olan benlere ve koyu lekelerin üzerine lazer uygulaması yapılmamalıdır. Merkezimizde FDA (Amerikan gıda ve ilaç onay dairesi) ve CE onaylı alexandrite ve ND-YAG lazer epilasyon cihazları kullanılmaktadır.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Etkili tedavi için kaç seans gereklidir?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`Vücudumuzdaki kılların büyüme (anajen), duraklama(katajen), dökülme (telojen)evreleri vardır. Bölgelere göre değişebilmekle beraber kıllarımızın yaklaşık %30-70'i telojen evrededir. Kılın ışığa en duyarlı olduğu evre anajen evredir. Diğer evrelerde ise melanin üretimi olmadığı için kıllar ışıktan etkilenmezler. Bu nedenle kılları anajen evrede yakalayabilmek için lazer epilasyon uygulamasının belli aralıklarla tekrar edilmesi gerekir. Lazer epilasyonda seans sayısı uygulama yapılan vücut bölgesi, cilt rengi, kıl rengi, kılın inceliği- kalınlığı, hormonal faktörlere göre değişir. Lazer epilasyonda kalıcı çözüm için ortalama 4-10 seans gerekir. Genel olarak erkeklerde tüm bölgeler ve kadınlarda yüz bölgesinde lazer epilasyon seansları uzun süreli olmakta ve tüyler incelerek kalabilmektedir. Genel olarak cilt rengi ne kadar açık ve kıl rengi ne kadar koyu ise lazer epilasyonda başarı o kadar artmaktadır. Genital, koltuk altı ve bacaklar lazer epilasyonda en iyi sonuçların alındığı alanlardır. Bütün bu faktörlere bağlı olarak başarı oranı %50-90 dır. Hormonal bozukluklar seans sayılarını ve aralarını etkileyebilir. Doktorunuz bu durumu değerlendirip gerekli incelemeleri yaptıktan sonra size bilgi verecektir. `,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Lazer epilasyonda seans aralıkları nasıl belirlenir?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`Lazer epilasyon seanslarına verilen aralıklar bölgeye göre 4 ile 8 hafta arasında değişir. Kılların dökülme ve çıkma süresine göre lazer epilasyon seans aralıkları değişebilmektedir.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Lazer epilasyonun yan etkisi var mıdır? ",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`Lazer epilasyon yapılırken, ağrı duyumu kişiye göre değişmektedir. Bu ağrı çoğunlukla dayanılabilir niteliktedir ve genellikle lastik çarpması, iğne dokunması hissi gibi tanımlanır.

                        Lazer epilasyon seansı sonrasında birkaç dakika süren kızarıklık, şişlik gelişebilir, fakat bu yan etkiler geçicidir ve hastaların günlük aktiviteleri etkilemez.

                        Lazer epilasyonda uygulama yapılan ciltte yüzeysel kabuklanma bazen oluşabilen geçici bir problemdir. Çoğunlukla bu kabuklanma günler içerisinde düzelir ve deri normale döner. Nadir vakalarda deride renk değişikliği dediğimiz açılma ya da koyulaşma gözlenebilir. Neredeyse tüm vakalarda deri 6-12 ay içinde normale döner. Skar dokusu denilen deride kalınlaşma çok nadir görülen bir yan etkidir ve kalıcı olabilir.

                        Eğer lazer epilasyon uygulaması dermatoloji uzmanlarının gözetiminde yapılır, doğru lazer tipi ve dozu seçilirse; herhangi bir yan etki görülmez.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Lazer epilasyonda yaz aylarında uygulama yapılabilir mi?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`Tedavi yaz aylarında da uygulanabilir. Ancak, uygulama yapılacak kişiler hekimin önereceği güneşten korunma yöntemlerini dikkatle uygulamalıdır. Ayrıca bronz tene uygulama yapılmamalı, uygulama sonrasında güneşte kalınmamalıdır. Bronz tene uygulama yapıldığında hem yan etkiler görülmekte, hem de başarı oranı düşmektedir. Bronzlaşmış kişilerin epilasyon seanslarına başlaması için en az 1 ay beklemesi gerekmektedir.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Lazer epilasyon öncesinde dikkat edilmesi gerekenler nelerdir?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:`Lazer epilasyon uygulamasından 1 ay öncesinden itibaren cımbız, ağda, epilatör gibi kılları kökünden koparan işlemler yapılmamalıdır. Uygulamadan 3 gün önce bölgenin tıraşlanması, kılların 2-3 mm uzaması için yeterli olacaktır. Tedavi öncesi sarartma veya boyama gibi işlemler yapılmamalıdır.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`Uygulama öncesi doz tespiti ve lazerle tanışma amacıyla test atışı uygulaması yapılabilir. Test sonucunda en uygun dalga boyu tespit edilir ve ardından tedavi uygulamasına geçilir.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`Kıl oluşumundaki olgunlaşma evrelerinin tamamlanması için vücut bölgeleri için 1-2 ay ara ile 4-6 seans yüz bölgesindeki kıllar için birer ay ara ile ortalama 6-12 seans lazer epilasyon uygulanması gerekebilir. Seansların daha sık veya geniş aralıklarla uygulanması kıl döngüsünü bozarak tedavi sürecini olumsuz etkilemektedir.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`Cildi soyan işlemler (peeling gibi) ve tedaviler (akne, leke ilaçları) lazer epilasyon seansından 15 gün öncesine kadar bırakılmalıdır.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Lazer epilasyon sonrasında dikkat edilmesi gerekenler nelerdir?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:`Lazer epilasyon uygulamasından sonra 2 gün güneşe çıkmamalı ve solaryuma girilmemelidir. Mutlaka doktorunuzun önerisiyle en az SPF 30 faktörlü güneş kremleri en az 2 hafta kullanılmalıdır. Özellikle yüz bölgesi için güneş kremi kullanımına dikkat etmek gerekmektedir.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`Lazer epilasyon uygulanan bölgede deri üzerinde görülen kılların çoğu seansta dökülür. Kökleri 3 ile 10 gün içinde uzayarak dökülmeye başlar.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`Lazer Epilasyon uygulanan bölgede kişinin cilt hassasiyetine göre değişen sürede; 1 saat ile birkaç gün arasında kızarıklık oluşması normaldir. Telaş etmenize gerek yoktur. Bazı kişilerde işlem sonrasında uygulama yapılan bölgede hafif kabarıklık, nadiren kabuklanma ve sivilce oluşabilmekte ve tedaviye gerek kalmadan kendiliğinden geçebilmektedir.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`Lazer epilasyondan hemen sonra ılık duş alınabilir. Bir hafta kese, peeling gibi cildi tahriş edecek uygulamalar yapılmamalıdır.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`Kıl yoğunluğu her seansta %15-20 arası azalmaktadır, farkı ancak 3. seanstan itibaren belirgin olarak görebilirsiniz. Seanslarınıza düzenli devam etmeniz lazer epilasyon işleminden maksimum verimi almanızı sağlayacaktır.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`Geniş bölgeler (kol-bacak-sırt-göğüs-genital) için her seanstan 2 hafta sonra ücretsiz kontrol seansı hakkınız mevcuttur (20. günden daha geç yapılan kontroller tedavinin seyrini olumsuz etkiler). Küçük bölgelerde ise tedavinin seyrine katkısı olmadığından kontrol seansı uygulamaya gerek yoktur.`,
                        classname:"content-p",
                      },
                    ]
                  },
                  subMenu:[
                    {
                        id:142,
                        paths:'/lazerkilcal',
                        birimAdi:"TIBBİ BİRİMLER ",
                        adi:"Lazer Kılcal Damar Tedavisi",
                        exact:false,
                        sidemenuVisible:true,
                        componenti:0,
                        icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Lazer Kılcal Damar Tedavisi",
                            icerikContext:[
                                {
                                    tag:"p",
                                    icerik:"Lazer Kılcal Damar Tedavisi",
                                    classname:"content-p-big",
                                },
                                {
                                    tag:"p",
                                    icerik:`Genetik yatkınlık, uzun süre ayakta durmak, gebelik dönemleri, topuklu ayakkabı giymek ve aşırı kilo artışı bacaklarda kılcal damar genişlemelerine neden olmaktadır. Yüz de ise güneşe maruziyet, genetik yatkınlık, hassas ve ince cilt tipine sahip olmak yine kırmız- mor kılcal damar artışlarının nedenleridir. Bunlar çok fazla sayıda olmadığı sürece herhangi bir şikayete neden olmazlar. Fazla sayıda kılcal damarlar özellikle bacaklarda dönem dönem ağrılara neden olabilir. Kılcal damarlar birçok kişide kozmetik sorun oluşturur.

                                    Yüz ve bacaklarda ya da vücudun herhangi bir yerinde oluşan kırmızı, mor renkli ince damarların tedavisi lazer cihazları ile ortalama 1-6 seansta yok edilebilir. En iyi sonuçlar tüm dünyada da olduğu gibi Nd-YAG lazer sistemleri ile alınmaktadır.`,
                                    classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"1.	Lazer ile kılcal damar tedavisi kaç seans yapılmalıdır?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"Kılcal damarların yaygınlığına, yerleşim yerine, genişliğine ve derinliğine göre bazen 1 seansta yok edilebildiği gibi, bazen de 6 seansa kadar uygulama gerekebilir.",
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"2.	Seans aralıkları ne kadar olmalıdır? ",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"Ortalama 4-8 hafta aralıklarla yapılmaktadır.",
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"3.	Hangi damarlar lazer tedavisine uygundur?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"Yüz ve bacaklardaki mavi-mor renkte, maksimum 4 mm genişlikteki damarlar lazere en iyi cevap veren damarlardır.",
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"4.	Tedavi öncesi ve sonrası yapılması gerekenler nelerdir?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"Güneşten korunmak şarttır. Kış aylarında yapılması gereklidir. Tedavi sonrası uygulama yerinde geçici kızarıklık, hafif ödem ya da renk değişiklikleri görülebilir.",
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"5.	Tedavi edilen damarlar tekrar oluşabilir mi?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"Genellikle tedavi edilen damarlar tamamen geri dönüşümsüz kaybolur. Ancak altta yatan bir venöz yetmezlik, varis oluşumu gibi durumlar var ise başka alanlarda tekrar kılcal damarların oluşumu gözlenebilir.",
                                  classname:"content-p",
                                },
                            ]
                        },
                    },
                    {
                        id:1461515,
                        paths:'/botoks',
                        birimAdi:"ÜNİTELERİMİZ ",
                        adi:"Botoks",
                        sidemenuVisible:true,
                        exact:false,
                        componenti:0,
                        icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Botoks",
                            icerikContext:[
                              {
                                  tag:"p",
                                  icerik:"Botoks",
                                  classname:"content-p-big",
                              },
                              {
                                  tag:"p",
                                  icerik:`Botox yüzdeki kırışıklıkların ve çizgilerin tedavisinde, yeni oluşabilecek kırışıklıkların önlenmesinde (antiageing) kullanılan oldukça başarılı bir tedavi yöntemidir. `,
                                  classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Botox hangi kırışıklıkların tedavisinde kullanılır?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Botox tedavisine en iyi cevap veren kırışıklıklıklar; alın, göz etrafı ve kaşların arasındaki kırışıklıklardır. Botox ayrıca gülme çizgileri, boyun ve ense kırışıklıklarının tedavisinde de kullanılabilir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kırışıklıklar nasıl oluşur?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Kırışıklıkların bir bölümü deri altında yer alan kasların mimik ve yüz hareketleri ile kasılması sonucu oluşur. Bu kasların gevşemesi kırışıklıkların azalmasına, yok olmasına neden olur. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Botox nasıl etki eder?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Botox botilinum toksin A içeren bir ilaçtır. Botilinum toksin nöromüsküler blokaj yapan bir ajandır, yani kasa enjekte edildiğinde kasta felce neden olur, bunu sinirle kas arasıdaki iletişim boşluğuna asetil kolin denen maddenin salınmasını engelleyerek oluşturur. Kaslar sinir tarafından uyarılamayınca da felç oluşur. Sinir yenilendiğinde ise kas yeniden kasılmaya başlar. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Botox dermatolojide başka hastalıkların tedavisinde kullanılıyor mu?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Evet. Botox, lokalize (koltuk altı, avuçlar ve ayak tabanları) aşırı terleme tedavisinde de kullanılan ve başarılı sonuçlar veren bir tedavi yöntemidir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Botox tedavisi nasıl yapılır?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"İşlem basittir ve muayenehane ortamında uygulanabilir. Anestezi gerektirmez. Botox uygulaması acısızdır ve injeksiyon yerinde herhangi bir iz bırakmaz. Çok küçük miktardaki Botox sulandırıldıktan sonra ince iğnelerle derinin hemen altındaki kaslara injekte edilir. Uygulamadan sonra uygulanan alanda herhangi bir değişiklik olmaz ve günlük normal aktivitelere hemen dönülebilir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Botox'un etkisi ne zaman başlar ve ne kadar sürer?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Botox' un etkisi 2-7 günde görülür. Bu etki 3-6 ay süre devam eder, daha sonra yeni enjeksiyonlara ihtiyaç duyulur. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Uygulamadan sonra nelere dikkat etmek gerekir?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Uygulama alanına 2-3 saat dokunulmamalı, dik pozisyonda kalınmalıdır.",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"2-3 saat kasları kasarak çalıştırmalıdır (kaş kaldırmak, kızmak, gülmek gibi)",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Banyo yapılabilir, ancak makyaj yapılmamalıdır.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Botox'un yan etkiler nelerdir?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Toksin çok düşük miktarlarda enjekte edildiği için, tedavi güvenli yan etkiler minimaldir. Enjeksiyon sırasında hafif yanma hastaların yarısında meydana gelir. Nadiren enjeksiyon alanında morarma olabilir. %1 oranında göz kapağı veya kaşlarda düşüklüğe neden olabilir. Ortalama dört haftada geriler.",
                                classname:"content-p",
                              },
                          ]
                        },
                    },
                    {
                      id:684936858,
                      paths:'/dolgu',
                      birimAdi:"ÜNİTELERİMİZ ",
                      adi:"Dolgu",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Dolgu",
                        icerikContext:[
                          {
                              tag:"p",
                              icerik:"Dolgu Uygulamaları",
                              classname:"content-p-big",
                          },
                          {
                              tag:"p",
                              icerik:`Kırışıklıklar ve çizgiler neden oluşur ?`,
                              classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Yaşlanma ile birlikte cildin hyalüronik asit içeriği azalır ve cildin su tutma kapasitesi düşer. Deride bulunan kolajen ve elastik lifler kırılmaya ve eskimeye başlar. Bu kırılmalar doğal yaşlanma sürecimizin bir parçası olmakla birlikte, fazla kaş çatma, gözleri kısarak bakma, gülümseme ve diğer yüz mimikleri de çizgilerin oluşmasına katkıda bulunurlar. Sigara içmek ve kirli hava gibi çevresel faktörler de yaşlanmayı hızlandırırlar. Yüz kırışıklıklarını doldurmak için en sık olarak kullanılan işlem yüz dolgusu tedavisidir. `,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Hyaluronik asit (restylane, teosyal, juvederm vb) nedir ?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Hyaluronik asit, laboratuar koşullarında üretilen hayvansal hammadde içermeyen, berrak kristal jel görünümlü bir maddedir (Cildimizde bulunan hücreler arası dolgu maddesidir). Deriye injekte edildiği zaman, vücudun kendi hyaluronik asiti ile birleşerek hacim oluşturur. Hacminin 1000 katı kadar su çekme kapasitesine sahiptir. Bu hacim ile dudakların dolgunlaştırılması, çizgilerin, kırışıklıkların doldurulması ve yüz kıvrımlarının tedavi edilmesi sağlanmaktadır. Hızlı ve kolay uygulanmasının yanı sıra, yüz dolgusu işlemi görülebilir sonuçların hemen elde edilmesine olanak sağlar. `,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Dolgu tedavisi sonrasında beklentilerim neler olabilir ?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Hyalüronik asit enjeksiyonundan sonra doğal bir görünüm sağlanır. Yüz dolgusu veya dudak dolgusu işlemlerini öğle yemeği tatilinde yaptırıp günlük hayatınıza devam edebilirsiniz. Herhangi bir ön test yaptırmanıza gerek yoktur. Ancak uygulayan kişinin uzman ve tecrübeli olması önemlidir.
                            Dolgu enjeksiyonlarından sonra normal aktivitelerinize hemen geri dönebilirsiniz. Etkisi kullanılan materyale göre 6-12 ay devam eder.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Dolgu tedavisinin yan etkisi var mıdır ?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Hyakuronik asit 1996'dan beri dünya üzerinde 70 ülkede 2 milyondan fazla kişiye güvenle uygulanmıştır.Çok ender olmakla birlikte tedavi edilen bölgede birkaç saat süren hafif bir şişme olabilir. Makyaj ile kolaylıkla kapatılabilecek hafiflikte olan morluklar görülebilir. Allerjik reaksiyonlar çok nadir görülürler. Enjeksiyon bölgesinde kızarıklık, kaşınma veya sertlik oluşabilir. Hyaluronik asit ile yüz dolgusu tedavisi, genellikle estetik operasyona bir alternatif olarak uygulanır. Hyaluronik asit kullanımı güvenli olup tedavi öncesi herhangi bir test uygulaması gerektirmemektedir. `,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Dolgu tedavisi ağrılı mıdır?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Lokal anestezik bir krem ile 45 dakika süren bir anestezi yeterli olmaktadır. Yüz dolgusu çok rahat tolere edilebilen bir tedavidir. Dudak sinir yoğunluğu açısından çok zengin bir bölge olduğu için dolgunlaştırma işleminde diş hekimlerinin uyguladığı anestezi tercih edilebilir. `,
                            classname:"content-p",
                          },
                        ]
                      },
                    },
                    {
                      id:5286727,
                      paths:'/bölhesel',
                      birimAdi:"ÜNİTELERİMİZ ",
                      adi:"Bölgesel İncelme",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                          icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                          icerikHeader:"Bölgesel İncelme",
                          icerikContext:[
                              {
                                  tag:"p",
                                  icerik:"Deneme psidir.",
                                  classname:"content-p",
                              },
                              {
                                  tag:"p",
                                  icerik:"Bu nu bilmiyorum psidir.",
                                  classname:"content-p",
                              }
                          ]
                      },
                      subMenu:[
                        {
                          id:143,
                          paths:'/kavitasyon',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Kavitasyon",
                          sidemenuVisible:true,
                          exact:false,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Kavitasyon",
                            icerikContext:[
                                {
                                    tag:"p",
                                    icerik:"Power Max İle Ultrakavitasyon",
                                    classname:"content-p-small",
                                },
                                {
                                    tag:"p",
                                    icerik:`Ultrakavitasyon yöntemi, bölgesel incelme ve selülit görünümünün azaltılmasında gözle görülür sonuçlar almasından ötürü ilgiyle karşılandı. Özellikle zaman problemi olan, yoğun çalışan kesimleri mutlu edecek bu uygulama, vücutta biriken yağ dokusunu bu güçlendirilmiş ultrason yardımıyla cilt dışından yağları eriterek şekillendirme esasıyla çalışıyor. Cerrahi bir operasyon olmaması işlemin iyileşme süresini ve konforunu artırıyor.`,
                                    classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"Ultrakavitasyon sistemi nedir? Nasıl çalışır?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`Ultrakavitasyon sistemi, bölgesel yağlanma ve selülitle ultrason yardımıyla savaşan, cerrahi olmayan bir yöntemdir. Cildin dış yüzeyine uygulanan yeni ultrasonun yayılması, yağ dokusundaki hücre sıvısında ani ve yüksek basınç değişikliklerine neden olur. Bununla birlikte oluşan köpüklenme önce genişleme, sonra patlama yaratır. Kavitasyon denilen bu etki; yağı sıvılaştırıp hücrelerin duvarlarını tahrip ederek depo yağların yapısını bozar. Bu dokudaki yağ hücreleri ve açığa çıkan yağ asitleri parçalanarak, lenf yolları sayesinde buralardan uzaklaştırılmaya çalışılır. Serbestleşen bu yağlar, adalelerde yakılarak ya da üriner sistem ve karaciğer yoluyla vücuttan dışarı atılırlar. Biz bu teknolojiyi önceden tecrübe sahibi olduğumuz hipoosmolar lipotomi yöntemiyle beraber kullanarak bahsedilen etkili sonuçları alıyoruz. Zaten tavsiye edilen ve bilimsel olarak da ispatlanmış gerçek etki mekanizması da bu metot ile elde edilmektedir.`,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"Hangi amaçlar ile uygulanır?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`Kurtulmak istenilen, yerleşmiş bölgesel yağları öncelikle eritmek, selülitli bölgelerin görünümünü azaltmak için kullanılan güçlü bir sistemdir. Makinenin bir diğer özelliği de ayarı değiştirilerek güçlü ultrason dalgaları aynı zamanda cilt sıkılaştırması için fayda sağlamaktadır.`,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"Uygulamanın ağrı ve acı verme durumu söz konusu mudur?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`Ultrakavitasyon yöntemi, güvenli ve acısız bir ultrason uygulamasıdır. Uygulama esnasında ve sonrasında birkaç saat kadar süren hafif bir ısı hissedilebilir.
                                  Tedavi süreci nasıldır?`,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"İki yöntem ile de kullanılır.",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"li",
                                  icerik:`Islak yöntem: Hipoozmolar sıvı enjeksiyonu yardımıyla, yağ hücreleri şişirilir. Böylece sıvılaştırma ve patlatma daha etkin biçimde gerçekleştirilir; buna hipoosmolar lipotomi diyoruz.`,
                                  classname:"content-p",
                                },
                                {
                                  tag:"li",
                                  icerik:`Kuru yöntem: Sıvı verilmeden o bölgeye direk uygulama yapılır, 6-8 seansta daha yavaş bir süreçte sonuç alınır. Haftada 1- 2 gün uygulanır. İğneden korkanlar için tavsiye ediyoruz, diğer sistemlerle de desteklenebilmektedir.`,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"Erkeklere de bu uygulamalar yapılabilir mi?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`Bu sistemde erkek, kadın ayrımı yoktur. Herkese uygulanabilir.`,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"Sonuçları görmek için ne kadar süre geçmeli ve bu arada neler yapılmalı?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`Özel bir diyet, spor takviyesi gerekmemekle birlikte, nasıl olsa böyle bir uygulama yaptırdım diye fazla yemek yemek kaybedilen yağlara yenisini ekler. Bazen bu yanılgıya düşüldüğünü görmekteyiz. Yine de serbestleşen yağ asitlerini harcamak ve yenilerini depolamamak için ultrakavitasyon ile hipoosmolar lipotomi ertesi, yaşa ve bedene uygun bir beslenme ve hareket etmenin dışında özel bir çaba gerekmiyor.

                                  İlk seanstan itibaren yağlı bölgede elle tutulur bir yumuşama, ardından da sertleşip küçülme hissediliyor. Gözle görülür sonuçlar 15. günden itibaren gözlenebilmektedir.`,
                                  classname:"content-p",
                                },
                            ]
                          },
                        },
                        {
                          id:190,
                          paths:'/radyofrekans',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Radyofrekans",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Radyofrekans",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Radyofrekans - Velashape İle İncelme",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Vücut şekillerinden ve kilolarından bağımsız olarak yirmi yaşın üzerindeki kadınların büyük bir çoğunluğu selülit problemi yaşıyor. Kadınlar geleneksel pahalı medikal prosedürlerle sınırlanan çözümlerden daha etkin ve uzun süreli çözümlere yöneliyorlar.

                                Dünyadaki tüm kadınların ortak hayali olan pürüzsüz ve güzel bir vücut... Teknolojinin gelişimiyle birlikte cerrrahi müdahele olmaksızın selülitlerden kurtulmak ve belirli bölgelerde biriken yağlanmayı gidermek amacıyla çeşitli cihazlar geliştirilmiştir.

                                VelaShape ameliyat olmadan selülit görüntüsünün azaltılması hatta derecesine göre yok edilmesi konusunda en gelişmiş ve estetik dünyasında bu hedefe ulaşmayı başardığını klinik ortamda kanıtlamış tek cihazdır. VelaShape Amerikan Besin ve İlaç Dairesi FDA'den "Sellüliti etkili biçimde tedavi eden ilk medikal cihaz" onayı almıştır.

                                Diğer vakum cihazlarında kılcal damar problemi olan kişilere (en azından problem olan bölgelere) uygulama yapılamazken, VelaShape bu hastalara da kolayca uygulanabilir. Yeni Radyo Frekanslı İncelme Tedavisi sıcak bir masaj gibidir ve çoğu hasta tedaviden keyif alır. Uygulama sırasında veya öncesi/ sonrası özel elbise veya çorap gerekmez.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"VelaShape Hangi Bölgelere Uygulanır ?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Bacaklar ve basen",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Üst kol",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Bel, yan bölge",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Karın bölgesi",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Yüz ve boyun",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"VelaShape Ne Sıklıkla Uygulanır ?, Kaç Seansta Sonuç Alınır ? ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Haftada 2 kez olmak üzere selülit derecesine göre 8-16 seans olarak önerilmektedir. Selülit derecesi bir veya iki olan birçok hasta 10 seans yeterli olmaktadır. Hatta ilk bir kaç seansdan sonra bile gözle görülür bir fark olur.
                                Selülit tedavisi sonucu elde edilen sonucun kalıcı olmasını sağlamak amacıyla ayda bir seans devam edilebilir. Her bir seans 45- 60 dakika sürer. Bu süre uygulama yapılan alanın büyüklüğüne göre artıp azalabilir.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"İşlem Sırasında Neler Hissedilir?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`VelaShape selülit tedavisi veya bölgesel incelme tedavisi yapılan deri yüzeyinde cildin ısınmasına bağlı olarak hafif kızarıklık oluşur. Hemen işlem ardından günlük aktivitenize başlayabilir, işinize dönebilirsiniz.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"VelaShape ile Selülit ve Bölgesel İncelme Tedavisi Güvenli midir? ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Cilt tipi ve rengi ayırt edilmeksizin, 18 yaş üstü tüm bireylerde güvenle uygulanabilir.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"VelaShape Tedavisine Nasıl Başlayabilirim?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`İlk iş olarak konsültasyon amaçlı bizi arayarak kliniğimizden ücretsiz görüşme randevusu alabilirsiniz. Tedaviye başlamaya karar vermenizin ardından uzman doktorumuz tarafından muayeneniz yapılır. VelaShape yönteminin sizin için güvenli olduğuna karar verdikten sonra selülitlerinizin derecesine veya şikayete sebep olan bölgesel yağlanma yoğunluğuna göre sizin için en efektif sonuçları alacak şekilde gerekli seans sayısı ve uygulama sıklığı belirlenir. Gerekiyorsa mezoterapi gibi diğer tedavilerle kombine edilmesi önerilir`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:959919,
                          paths:'/powerplate',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Powerplate",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Powerplate",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Radyofrekans - Velashape İle İncelme",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Vücut şekillerinden ve kilolarından bağımsız olarak yirmi yaşın üzerindeki kadınların büyük bir çoğunluğu selülit problemi yaşıyor. Kadınlar geleneksel pahalı medikal prosedürlerle sınırlanan çözümlerden daha etkin ve uzun süreli çözümlere yöneliyorlar.

                                Dünyadaki tüm kadınların ortak hayali olan pürüzsüz ve güzel bir vücut... Teknolojinin gelişimiyle birlikte cerrrahi müdahele olmaksızın selülitlerden kurtulmak ve belirli bölgelerde biriken yağlanmayı gidermek amacıyla çeşitli cihazlar geliştirilmiştir.

                                VelaShape ameliyat olmadan selülit görüntüsünün azaltılması hatta derecesine göre yok edilmesi konusunda en gelişmiş ve estetik dünyasında bu hedefe ulaşmayı başardığını klinik ortamda kanıtlamış tek cihazdır. VelaShape Amerikan Besin ve İlaç Dairesi FDA'den "Sellüliti etkili biçimde tedavi eden ilk medikal cihaz" onayı almıştır.

                                Diğer vakum cihazlarında kılcal damar problemi olan kişilere (en azından problem olan bölgelere) uygulama yapılamazken, VelaShape bu hastalara da kolayca uygulanabilir. Yeni Radyo Frekanslı İncelme Tedavisi sıcak bir masaj gibidir ve çoğu hasta tedaviden keyif alır. Uygulama sırasında veya öncesi/ sonrası özel elbise veya çorap gerekmez.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"VelaShape Hangi Bölgelere Uygulanır ?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Bacaklar ve basen",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Üst kol",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Bel, yan bölge",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Karın bölgesi",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Yüz ve boyun",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"VelaShape Ne Sıklıkla Uygulanır ?, Kaç Seansta Sonuç Alınır ? ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Haftada 2 kez olmak üzere selülit derecesine göre 8-16 seans olarak önerilmektedir. Selülit derecesi bir veya iki olan birçok hasta 10 seans yeterli olmaktadır. Hatta ilk bir kaç seansdan sonra bile gözle görülür bir fark olur.
                                Selülit tedavisi sonucu elde edilen sonucun kalıcı olmasını sağlamak amacıyla ayda bir seans devam edilebilir. Her bir seans 45- 60 dakika sürer. Bu süre uygulama yapılan alanın büyüklüğüne göre artıp azalabilir.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"İşlem Sırasında Neler Hissedilir?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`VelaShape selülit tedavisi veya bölgesel incelme tedavisi yapılan deri yüzeyinde cildin ısınmasına bağlı olarak hafif kızarıklık oluşur. Hemen işlem ardından günlük aktivitenize başlayabilir, işinize dönebilirsiniz.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"VelaShape ile Selülit ve Bölgesel İncelme Tedavisi Güvenli midir? ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Cilt tipi ve rengi ayırt edilmeksizin, 18 yaş üstü tüm bireylerde güvenle uygulanabilir.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"VelaShape Tedavisine Nasıl Başlayabilirim?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`İlk iş olarak konsültasyon amaçlı bizi arayarak kliniğimizden ücretsiz görüşme randevusu alabilirsiniz. Tedaviye başlamaya karar vermenizin ardından uzman doktorumuz tarafından muayeneniz yapılır. VelaShape yönteminin sizin için güvenli olduğuna karar verdikten sonra selülitlerinizin derecesine veya şikayete sebep olan bölgesel yağlanma yoğunluğuna göre sizin için en efektif sonuçları alacak şekilde gerekli seans sayısı ve uygulama sıklığı belirlenir. Gerekiyorsa mezoterapi gibi diğer tedavilerle kombine edilmesi önerilir`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:378235893,
                          paths:'/enjeksiyon',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Enjeksiyon Lipofiz",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Enjeksiyon Lipofiz",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Enjeksiyon Lipoliz",
                                classname:"content-p-big",
                              },
                              {
                                tag:"p",
                                icerik:"Lipoliz Nedir?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Diyet ya da spor ile çözülemeyen, vücudun belirli bölgelerindeki yağlanma sorunları için; artık Avrupa ve Amerika'da enjeksiyon lipoliz yöntemi uygulanmaktadır. Yağ yakıcı, eritici ve yağ yakımını hızlandıran bazı maddelerin cilt altı yağ dokusuna direk enjekte edilmesidir.

                                Birçok kişide kilo fazlalığı olmadığı halde vücudun belirli bölgelerinde aşırı yağ birikimi oluşmaktadır. Kadınlarda daha çok bacak, kalça, karın, bel yan tarafları, erkeklerde ise özellikle karın ve bel en çok yağ birikimine yatkın yerlerdir. Bölgesel incelme programlarında kavitasyon ve radyofrekans ile birlikte kullanıldığında etki yaklaşık 3 kat artmaktadır.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Hangi Maddeler Kullanılıyor?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`En popüler zayıflama yöntemlerinden olan lipoliz yönteminde fosfatidil kolin adı verilen bir etken madde kullanılıyor. Lesitin adı verilen bu madde insanlar için hayati bir önem taşır.

                                Hemen hemen tüm hücrelerde bulunur ve yaşamsal aktivitelerin hepsinde rol alır. Yaklaşık yedi yıldan beri tıpta yüksek kolesterolü düşürmek için kullanılan bu ilaçlar, vücuda yerleşen ve tedaviye cevap vermeyen yağ dokusu içine enjekte edildiğinde, onları küçülttüğü ve erittiği görünmüştür. Bu sonuçlardan yola çıkarak vücutta istenmeyen bölgelerde yerleşen yağ fazlalıkların tedavisinde kullanılabileceği düşünülmüştür. Bu bölgesel fazlalıklar ağırlıklı olarak bel her iki yan tarafı, bacak, göbek ve diz içleri, ayak bileği ve kol bölgelerindeki hafif fazlalıklardır.
                                Beraberinde yağ yakımını hızlandıran karnitin-L maddesi de kullanılabilir.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Tedavi Nasıl Yapılıyor?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Her türlü yağ bezesinin küçültmesinde ve yok edilmesinde bu yöntem kullanılabilir. Kadınlarda, erkeklere göre nispeten daha başarılı sonuçlar veren bu tedavi, mezoterapi uygulamasına benzer bir yöntemle fazlalıkların olduğu bölgelere, çok ince iğneler ile ilaç enjeksiyonu yapılarak uygulanıyor.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kaç Seans Gereklidir?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Cilt altı yağ kitlesinin yoğunluğuna göre değişmekle birlikte ortalama 3- 6 seans uygulanmaktadır. Çoğu yağların çözümü daha kolay olduğu için 3 seansta hasta tedavi olabilmektedir. Seans sıklığı ve sayısı verilecek olan ilaç miktarına göre de değişebilmektedir.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Uygulama Ağrılı Mıdır?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Uygulama sırasındaki ağrı dayanılmaz değildir. Birkaç gün süren ödem ve hassasiyet olabilir. Yaklaşık 1- 2 haftada iyileşen morarmalar meydana gelebilir. Kişiler, uygulamadan hemen sonra günlük normal aktivitelerine dönebilirler.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Yan Etkileri Nelerdir?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Bu konuda eğitim ve deneyime sahip uzman hekimler tarafından uygulandığında herhangi bir yan etkiye rastlanmamıştır.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                      ],
                    },
                    {
                      id:377392,
                      paths:'/medikal',
                      birimAdi:"ÜNİTELERİMİZ ",
                      adi:"Medikal Cilt Bakımı",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                          icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                          icerikHeader:"Medikal Cilt Bakımı",
                          icerikContext:[
                              {
                                  tag:"p",
                                  icerik:"Medikal Cilt Bakımı",
                                  classname:"content-p-big",
                              },
                              {
                                  tag:"p",
                                  icerik:"Yağlı cilt özel akne bakımları",
                                  classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Aşırı sebum salgısı ciltte parlamaya ve dengesizliğe yol açar. Yağlı cilt özel akne bakımları ile siyah nokta, gözenek ve sivilce lekelerinde azalma sağlanıp, yağlanma dengelenebilir.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Lifting bakımı",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Cildinizin daha genç görünmesi için yerleşik yağ hücrelerini azaltmak ve yüz hatlarını şekillendirmek için yenilikçi bir metod.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"İğnesiz botoks bakımı",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Kırışıklıkların önlenmesinde ve azaltılmasında etkili cerrahi olmayan bir bakımdır.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Havyar bakımı",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Erken yaşlanmaya karşı savaşan bir bakımdır.
                                Sadece cansız değil olgun ciltler için de yoğun bir yenileme sağlar.
                                `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Leke tedavisi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Leke, normalde cilt rengini veren melanin isimli pigmentin vücudun belirli bölgelerinde cilt altında düzensiz kümelenmesi ile meydana gelir. Oldukça etkili bakımlarla lekelerinizden kurtulmanız mümkün.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kimyasal peeling",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Derinin yenilenmesi yaş ilerledikçe yavaşlar.
                                Kimyasal peeling tedavisi ile sağlıklı ve genç hücrelerin cildin yüzeyine gelmesi, problemli hücrelerin atılması sağlanır.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Kimyasal Peeling tedavisi aşağıdaki problemleri çözmek için kullanılır;`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Akne (sivilce) tedavisi`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Aknelerin iyileştikten sonra bıraktığı izlerin (skarlarının) tedavisi`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Kırışıklıklar ve ince çizgilerin giderilmesi`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Kuru-cansız cilt tedavisi`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Ciltteki açık gözeneklerin kapanması`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`İstenmeyen renk değişikliklerinin (yaşlılık, güneş, gebelik ve ilaç lekeleri) giderilmesi`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Yaşlanma etkilerini geciktirmek ve cildi gençleştirmek için her sağlıklı cilde 6 seans peeling tedavisi uygulanmaktadır.`,
                                classname:"content-p",
                              },
                          ]
                      },
                    },
                    {
                      id:8948363,
                      paths:'/prp',
                      birimAdi:"ÜNİTELERİMİZ ",
                      adi:"Prp",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Prp",
                        icerikContext:[
                          {
                              tag:"p",
                              icerik:"PRP (Platelet Rich Plasma) Tedavisi",
                              classname:"content-p-big",
                          },
                          {
                              tag:"p",
                              icerik:"PRP nedir?",
                              classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`"Platelet rich plasma (PRP)" kanda bulunan platelet yani trombosit adı verilen hücrelerden zengin plazma uygulaması adı verilen yöntemin kısaltılmış adıdır. Bir kişiden alınan az miktardaki kanın özel bir işlemden geçirilerek bileşenlerine ayrıştırılması ve elde edilen az miktardaki "platelet yönünden zenginleştirilmiş plazmanın" yine aynı kişiye cilt gençleştirme amaçlı enjeksiyon yoluyla geri verilmesi işlemidir.
                            PRP enfeksiyon riskini azaltmak amacıyla kalp ameliyatlarında, iyileşme sürecini hızlandırmak amacıyla tenisçi dirseği, tendon sakatlığı gibi rahatsızlıklarda da kullanılmaktadır. Aynı zamanda diş hekimleri tarafından implant tedavisinde de yapılabilmektedir. PRP vücutta enjekte edildiği bölgelerde kök hücreleri uyarıp aktif hale geçirirerek dokuların yenilenmesine yardımcı olan bir sistemdir.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"PRP Uygulamasında amaç nedir?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Plateletler yani trombosit hücreleri, kanın pıhtılaşmasını sağlayan vücudumuzdaki hasarlı damarları ve diğer dokuları onaran büyüme faktörleri içeren hücrelerdir. Dokularımızda herhangi bir hasar olduğunda plateletler aracılığıyla onarım süreci başlar. PRP uygulamasında, hedef bölgeye kan dolaşımıyla taşınabilenden çok daha fazla sayıda platelet ve içeriğinde bulunan büyüme faktörlerini ulaştırılabilmektedir.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"PRP İle Cilt Gençleştirme Nasıl Sağlanır?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Derimizin yaşlanması, aynı yaralanma sürecinde olduğu gibi bazı fiziksel özelliklerini kaybetmesinden kaynaklanır. Bu nedenle cilt gençleştirme amaçlı uygulamalarda, vücudumuzun bir yarayı iyileştirirken yaptıklarını çeşitli yöntemlerle taklit ederiz. Örneğin lazer, peeling gibi yöntemlerle derimize mikro düzeyde hasarlar veririz ve iyileşme sürecini tetikleriz. Bu hasar sonunda büyüme faktörleri salınır ve iyileşme süreci başlar. Dermo kozmetik ürünler de benzer şekilde derimizi yeniden yapılandıran maddelerin ve sentetik olarak elde edilen büyüme faktörlerinin aracılığıyla iyileşme sürecini başlatır.
                            Plazma içinde konsantre olarak bulunan plateletler cilde enjekte edildiğinde bunların bünyesinde bulunan büyüme faktörleri, kollajen üretimi ve yeni kılcal damarların oluşmasını uyarmakta ve cildin kendini hızla yenilemesini sağlamaktadır.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Havyar bakımı",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Erken yaşlanmaya karşı savaşan bir bakımdır.
                            Sadece cansız değil olgun ciltler için de yoğun bir yenileme sağlar.
                            `,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"PRP İle Cilt Yenileme Prosedürü Nasıl Uygulanır? ",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Platelet açısından zengin plazma elde etmek amacıyla özel bir filtre ve santrifüj kullanılır. Plateletlerin yaklaşık % 97'si ayrışır ve plazma içinde normalin 6 - 10 katı daha fazla yoğunluğa sahip olur.
                            P.R.P tedavisi hastadan kan alınması ile başlar. Özel bir filtre ve santrifuj yardımıyla otolog beyaz kan hücreleri içeren platelet açısından zengin plasma hazırlanır. Son olarak platelet açısından zengin otolog beyaz kan hücreleri içeren plazma (PRP) tedavi bölgesinde cilde enjekte edilir. Enjeksiyon bölgelerinde plateletler ve beyaz kan hücreleri birlikte, yoğun şekilde büyüme faktörlerinin serbest kalmasını sağlar. Büyüme faktörleri kollajen ve hyaluronik asit üretimini arttırarak yaraların iyileşmesi, kırışıklık, leke ve akne izleri gibi cilt problemlerinin önemli ölçüde giderilmesini, cildin yenilenmesini sağlar.
                            P.R.P, dolgu enjeksiyonu veya mezoterapi şeklinde uygulanır.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"PRP Uygulaması Ne Kadar Sürer?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Yaklaşık 30 dakikalık bir uygulamadır.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`PRP İle Cilt Yenileme enjeksiyon dışında başka şekilde uygulanabilir mi?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Evet, maske veya krem olarak da hazırlanıp uygulamak mümkündür.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`PRP İle Cilt Yenileme Hangi Durumlarda Etkilidir?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"li",
                            icerik:`Estetik amaçlı uygulamalarda yüz, boyun, dekolte, eller, bacak içleri, kollar gibi vücut bölgelerine gençleştirme için`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`Lazer, peeling gibi uygulamalardan hemen sonra, derinin hızla yapılanmasını sağlamak için`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`Deride yıllarca ultraviyole ışınlarına maruz kalmanın sonucunda oluşan kırışıkların düzelmesi, çöküntülerin giderilmesine yardımcı olmak için`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`Esneklik ve parlaklığın kazandırılması ve devamlılığı için`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`İyileşmesi uzun süren yara, çatlak ve deri niteliğinin zarar gördüğü durumların kontrolünü sağlamak için`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`Saç dökülmesi tedavisinde tek başına veya diğer tedavilerle kombine olarak kullanılabilir.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`PRP Güvenilir Bir Cilt Gençleştirme Yöntemi midir?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Hastanın kendisinden alınan kan steril ve kapalı bir kit yardımıyla kullanılır, bu nedenle PRP güvenilir bir uygulamadır.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`PRP' nin Etkisi Ne Zaman Görülür? `,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Uygulamadan hemen sonra ciltte sağlıklı bir parlaklık ortaya çıkar. 3 veya 4 uygulamadan sonra kalıcı bir belirgin etki görülür.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`PRP İle Cilt Gençleştirme Etkisi Kalıcı mıdır?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`15 günde bir yapılacak 3 veya 4 uygulamadan sonra 10- 12 ayda bir tekrarlanırsa kürlerin etkisi kalıcı bir gençleştirici etkiye eşdeğerdir.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`PRP Yönteminin Avantajları Nelerdir?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Diğer yöntemlerle sağlanan olumlu sonuçlar belli bir süre devam eder, ancak PRP'nin olumlu sonuçları tamamen uygulanan kişiye aittir kaybolmaz.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`PRP İle Cilt Yenileme İşlemi Ağrılı mıdır?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Hafif bir rahatsızlık hissi dışında ciddi bir acı hissedilmez. Uygulama öncesi anestetik kremler uygulanabilir.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`PRP Kimlere Yapılmaz?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Platalet sayısı yetersiz hastalar ve kanser hastalarında yapılmaz.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`PRP Kök Hücre İle Cilt Gençleştirme Anlamına mı Gelir? `,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`PRP enjekte edildiği bölgede kök hücreleri uyarır ve kök hücrelerin aktif hale geçmesini sağlar. Serum içeriğinde kök hücre yoktur, yoğun olarak trombositler (platelet) ve beyaz kan hücreleri bulunmaktadır. PRP'nin ise tamir edici kök hücrelerinin yeniden yapımına etkisi vardır.`,
                            classname:"content-p",
                          },
                        ]
                      },
                    },
                    {
                      id:25251513312,
                      paths:'/mezoterapi',
                      birimAdi:"ÜNİTELERİMİZ ",
                      adi:"Mezoterapi",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                          icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                          icerikHeader:"Mezoterapi",
                          icerikContext:[
                            {
                                tag:"p",
                                icerik:"Power Plate",
                                classname:"content-p-big",
                            },
                            {
                                tag:"p",
                                icerik:"Power Plate Nedir?",
                                classname:"content-p-small",
                            },
                            {
                              tag:"p",
                              icerik:`Yerçekimi nedeniyle hepimizin kütlesinin kilogram olarak tanımlanan bir ağırlığı vardır. Bu çekim gücüne karşı koyarak hareket eden insan vücudunda kaslar gelişir ve güçlenir.
                              Tüm dünyada yaygın olarak kullanılan bu teknikte, Power Plate'in ürettiği vibrasyon, insan vücuduna bir enerji olarak yollanır. Tercih edilen frekanstaki salınımlar vücut kaslarının gerilme yönünde uyarır. Uygulama süresince kaslar sürekli olarak gerilir/ gevser ve çalışır. Günümüzde birçok sporcunun antremanlarını üzerine ilave ağırlıklar takarak yapmasının nedeni de budur. Vücudun her bölgesindeki kaslar uygulamanın yapıldığı kısa süre içinde yoğun bir disiplin içersinde çalışır. Power Plate kontrol panelinden seçilecek 40 Hz. salınım hızı ile kaslarımız saniyede 40 kez gerilir ve gevşer. Örneğin, 1 dakika bu salınım altında çalışan karın kaslarımız yüzlerce kez mekik hareketi yapmışçasına yorulur.`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`Çalışma tamamlanıp dinlenmeye geçildiğinde, vücut kendini dengelemek için metabolizmayı ayarlar. Bu ayarlama ile vücudumuzun performans çizgisi yukarı çekilmiştir. Bir sonraki çalışmada vücut performansını bu yeni çizgiyi baz alarak yukarıya çekmeye çalışır. Vücudun performans arttıcı bu girişimine "supercompensation" denir.

                              Vibrasyon Platformları ile düzenli olarak çalışma yapmanın yararları üzerine sürdürülen çalışmalar göstermiştir ki: Whole Body Vibration diğer çalışma programlarına göre % 85 daha az zaman gerektirir; buna karşın kasların çalışmasını ve direncini % 25 daha fazla arttırır.
                              10 Dakikalık Power Plate çalışması yaklaşık 1 saatlik Fitness Çalışmasına eşdeğerdir...`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Esneklik kazandırır`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Vücuttaki tüm kasların derinlemesine çalışmasını sağlar,`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Eklem sakatlıkları riskini minimuma indirir`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Kan akışı ve oksijenlenmeyi arttırır`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Hormanların daha yüksek düzeyde çalışmasını sağlar`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Vücüt suyunun drenajını arttırır`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:"Osteoporoz etkisini azaltır",
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Sırt ve bel ağrısını azaltır, bu bölgeleri güçlendirir`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Selülit oluşumunu engeller, selüliti azaltır`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Kemik yoğunluğunu arttırır`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Kollajen üretimini arttırır`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Zihinsel ve bedensel stresi azaltır`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`Günde sadece 10 dakika çalışarak Genç kalın....`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`Power Plate'in kullanıldığı alanlar`,
                              classname:"content-p-small",
                            },
                            {
                              tag:"p",
                              icerik:`Fizik Tedavi: Vibrasyon uygulaması kasları, kas dokusunu, kemik yapısını ve eklem bağlarını güçlendirdiğinden, eklem sakatlanması riskini azaltılır. Ayrıca bu bölgelerde görülen hasarların daha kısa süre içinde onarılmasını kolaylaştırır.`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`Spor: Genelde uygulanan kas güçlendirici spor programları ile birlikte uygulandığında, kasların çok daha kısa sürede güçlenmesine yardımcı olur. Bunun yanısıra tek başına kullanıldığında benzer etkiyi görmek mümkündür. Günde sadece 10 dakikalık bir uygulama ile vücudun her bölgesindeki kaslar güçlenmektedir.`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`Fitness: Geleneksel fitness (esneme/gevşeme) çalışmalarına uyum sağlayabildiği gibi, bu çalışmalarda ısınma ve soğuma programı olarak kullanılabilir. Step, Aerobics ve diğer fitness programlarından önce ısınma amacıyla kullanıldığında sporcuların ana çalışma programına ayıracakları zaman artmaktadır`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`Rehabilitasyon: Kas zafiyetini (atrophy) ve kas gevşekliğini (hypotonia) ortadan kaldırır, ağrıları azaltır, algılama sorunlarını (Proprioceptive disturbances) giderir. Tüm vücutta rahatlama yaratır.`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`Medikal: Kan dolaşımını düzenler, kemik erimesi (osteoporosis) bozukluklarının düzeltilmesine katkı sağlar. Çalışma esnasında kalp atış hızı sabit kaldığından vücuda ilave yük getirmez.`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`Kozmetik: Vücutta sıvı birikmesini engeller, birikmiş sıvıların drenajına yardımcı olur. Serbest yağların parçalanmasını hızlandırır, en önemlisi kas yapısını geliştirdiği için bölgesel selülitleri giderir, vücudun sıkı bir dokuya sahip olmasına yardımcı olur, selülit oluşumunu engeller. Vücuttaki tonik dengesini ayarlayarak dokuların sıkılığını arttırır.`,
                              classname:"content-p",
                            },
                          ]
                      },
                    },
                  ],
                },
              ],
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Ünitelerimiz",
                icerikContext:[
                  {
                    tag:"li",
                    icerik:"Ünitelerimiz baby",
                    classname:"content-li",
                  }
                ]
              },
            },
            //140bitis
            {
              id:170,
              paths:'/doktorlarimiz',
              adi:"Doktorlarımız",
              sidemenuVisible:true,
              exact:false,
              componenti:2,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Doktorlarımız",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Deneme psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  }
                ],
                tab:{
                  tablist:[
                    {
                      id:0,
                      name:"Erdal ARAS",
                      title:"Uzm. Dr.",
                      bolum:"Radyoloji Hizmetleri",
                      tarih:"07.10.1945",
                      birthlocation:"Çorum",
                      lang:[
                        {id:0,adi:"İngilizce"},
                      ],
                      education:[
                        {id:0,adi:"HACETTEPE ÜNİVERSİTESİ TIP FAKÜLTESİ",yil:"1900"},
                      ],
                      icerik:"İCerik",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/erdal-aras.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:1,
                      name:"Levent İLHAN",
                      title:"Dr.",
                      bolum:"Pratisyen Hekim",
                      tarih:"11.09.1964",
                      birthlocation:"Erzincan",
                      lang:[
                        {id:0,adi:"İngilizce"},
                      ],
                      education:[
                        {id:0,adi:"ERCİYES ÜNİVERSİTESİ TIP FAKÜLTESİ",yil:"1900"},
                      ],
                      icerik:"İCerik2",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/yok.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:2,
                      name:"Ercan Akpınar",
                      title:"Dr.",
                      bolum:"Pratisyen Hekim",
                      tarih:"16.05.1970",
                      birthlocation:"Erzurum",
                      education:[
                        {id:0,adi:"Hacettepe Üniversitesi Tıp Fakültesi",yil:"1900"},
                      ],
                      lang:[
                        {id:0,adi:"ingilizce"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/ercan-akpınar.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:3,
                      name:"Ömer Orçun Koçak",
                      title:"Op. Dr.",
                      bolum:"Kadın Hastalıkları ve Doğum",
                      tarih:"31.08.1977",
                      birthlocation:"İstanbul",
                      education:[
                        {id:0,adi:"İstanbul Üniversitesi, Cerrahpaşa Tıp Fakültesi Kadın Hastalıkları ve Doğum Anabilim Dalı (2001–2007).",yil:"1900"},
                        {id:1,adi:"İstanbul Üniversitesi, İstanbul Tıp Fakültesi (1994–2001).",yil:"1900"},
                        {id:2,adi:"İstanbul Eğitim ve Kültür Vakfı (İSTEK), Florya Özel Bilge Kağan Lisesi (1988-1994)",yil:"1900"},
                        {id:3,adi:"Yeşilköy Hamdullah Suphi Tanrıöver İlköğretim Okulu, İstanbul (1983–1988)",yil:"1900"},
                      ],
                      lang:[
                        {id:0,adi:"ingilizce"},
                      ],
                      deneyim:[
                        {id:0,adi:"Özel Bodrum Hastanesi , Bodrum-Muğla (Temmuz 2011’den beri) ",yil:"1900"},
                        {id:1,adi:"Özel Anamed Hastanesi , Mersin ( Eylül 2010 – Temmuz 2011) ",yil:"1900"},
                        {id:2,adi:"Bingöl Özel Hastanesi, Bingöl (Ekim 2009 – Ağustos 2010 )",yil:"1900"},
                        {id:3,adi:"T.S.K 54’üncü Mekanize Piyade Tugayı, Harbiye Kışlası, Aile Reviri, Edirne (Kasım 2008 - Eylül 2009).",yil:"1900"},
                        {id:4,adi:"T.C. Sağlık Bakanlığı, Bingöl Doğum ve Çocuk Bakımevi Hastanesi, Bingöl (Zorunlu Hizmet kapsamında, Mart 2007-Eylül 2008 arası).",yil:"1900"},
                        {id:5,adi:"Türk Böbrek Vakfı, İstanbul Hizmet Hastanesi, Kadın Hastalıkları ve Doğum Departmanı (2006–2007 gece nöbetleri).",yil:"1900"},
                        {id:6,adi:"İstanbul Cerrahi Hastanesi (2007 gece nöbetleri).",yil:"1900"},
                        {id:7,adi:"Avcılar Medicana Hastanesi (2005–2007 gece nöbetleri)",yil:"1900"},
                        {id:8,adi:"Prof. Dr. Vildan Ocak (İ.Ü. CTF Kadın Hastalıkları ve Doğum ABD, Perinatoloji Bilim Dalı Başkanı) Özel Muayenehanesi, Mecidiyeköy, (2005–2007 yılları arasında ultrasonografist olarak)",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/omerorcun.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:4,
                      name:"Cengiz Kayahan",
                      title:"Doç. Dr.",
                      bolum:"Genel Cerrahi",
                      tarih:"24.10.1959",
                      birthlocation:"Ankara",
                      education:[
                        {id:1,adi:"İstanbul Üniversitesi, İstanbul Tıp Fakültesi",yil:"1900"},
                      ],
                      lang:[
                        {id:0,adi:"ingilizce"},
                      ],
                      deneyim:[
                        {id:0,adi:"2011-2017-Medicana International Hospital Samsun ",yil:"1900"},
                        {id:1,adi:"2009-2011-Medicalpark Özel Ordu Hastanesi",yil:"1900"},
                        {id:2,adi:"2008-2009-Özel Keçiören Hastanesi Ankara",yil:"1900"},
                        {id:3,adi:"2008-2008-Özel Caria Hastanesi Marmaris Muğla",yil:"1900"},
                        {id:4,adi:"2007-2008-Muayenehane Hekimliği",yil:"1900"},
                        {id:5,adi:"2004-2007-Emeklilik / Muayenehane Hekimliği",yil:"1900"},
                        {id:6,adi:"2000-2004-GATA Acil Tıp AD Öğr. Üyesi",yil:"1900"},
                        {id:7,adi:"2000 -Genel Cerrahi Doçenti",yil:"1900"},
                        {id:8,adi:"1999-2000-GATA Acil Tıp AD Öğretim Görevlisi",yil:"1900"},
                        {id:10,adi:"1993-1999-GATA Genel Cerrahi AD Yard. Doç.liği",yil:"1900"},
                        {id:11,adi:"1994:Siirt 30 Ytk.Seyy.Cerr.Hast Bştbp.liği (4 ay)",yil:"1900"},
                        {id:12,adi:"1996:Siirt 30 Ytk.Seyy.Cerr.Hast Bştbp.liği (4 ay)",yil:"1900"},
                        {id:13,adi:"1992-1993-Gölcük Dz. Hst. Genel Cerrahi Uzmanlığı",yil:"1900"},
                        {id:14,adi:"1988-1992-GATA Genel Cerrahi AD Uzmanlık Eğitimi",yil:"1900"},
                        {id:15,adi:"1987-1988-Dz.K.K.lığı TCG Turgut Reis Gemi Tabipliği",yil:"1900"},
                        {id:16,adi:"1986-1987-GATA Askeri Tıbbi Deontolojii Eğitimi",yil:"1900"},
                        {id:17,adi:"1985-1986-Dz.K.K.lığı Tuzla Dz H.O. Revir Tabipliği",yil:"1900"},
                        {id:18,adi:"1984-1985-Ank.Altındağ M.S.O.Tabipliği",yil:"1900"},
                        {id:19,adi:"1982-1984-Giresun Eynesil Sağ. Ocağı Tabipliği",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/cengiz-kayahan.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:5,
                      name:"Mehmet Demircioğlu",
                      title:"Dr.",
                      bolum:"İç Hastalıkları",
                      education:[
                        {id:1,adi:"2000 İstanbul Ü.Cerrahpaşa Tıp Fak İç Hastalıkları",yil:"1900"},
                        {id:2,adi:"1989 İstanbul Üniversitesi Cerrahpaşa Tıp Fakültesi",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"2011 - 2014 Memorial Antalya Hastanesi",yil:"1900"},
                        {id:1,adi:"2008 - Marmaris Caria Hastanesi",yil:"1900"},
                        {id:2,adi:"2004 - 2017 Acıbadem Sağlık Grubu ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/dr-mdemir.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:7,
                      name:"Muaffak Bağdatlı",
                      title:"Op. Dr.",
                      bolum:"Ortopedi ve Travmatoloji",
                      tarih:"16.11.1978",
                      birthlocation:"Bornova",
                      education:[
                        {id:1,adi:"1990 Çukurova Üniversitesi Tıp Fakültesi Kulak, Burun ve Boğaz Hastalıkları",yil:"1900"},
                        {id:2,adi:"1983 Çukurova Üniversitesi Tıp Fakültesi",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"2013 - Halen Acıbadem Sağlık Grubu ",yil:"1900"},
                        {id:1,adi:"2004 - 2013Universal Hospital Bodrum Hastanesi ",yil:"1900"},
                        {id:2,adi:"1998 - 2002Ankara Egitim ve Arastirma HastanesiOrtopedi ve Travmatoloji Asistanlik Programi ",yil:"1900"},
                        {id:3,adi:"1996 - 1997 Gediz Hastanesi ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/dr-murat.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:8,
                      name:"Alp Mustafa Günay",
                      title:"Dr.",
                      bolum:"İç Hastalıkları ve Gastroenteroloji",
                      tarih:"03.09.1966",
                      birthlocation:"Eskişehir",
                      mail:"alp.mustafa.gunay@acibadem.com.tr",
                      education:[
                        {id:1,adi:"2000'de Gastroenteroloji Yandal Uzmanı olarak mezun olmuş",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"Gülhane Askeri Tıp Akademisi'nden 1990'da Tıp Doktoru",yil:"1900"},
                        {id:1,adi:"1997'de İç Hastalıkları Uzmanı",yil:"1900"},
                        {id:2,adi:"9 yabancı, 16 yerli yayını ve 27 yabancı, 24 yerli bildirisi mevcuttur. ",yil:"1900"},
                        {id:3,adi:"2010'da da Avrupa Birliği Tıp Uzmanları Derneği'nden (EUMS) Avrupa Gastroenteroloji ve Hepatoloji Uzmanlık Sertifikası (EBGH) almıştır. ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/alp-mustafa-gunay.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:9,
                      name:"Tahir Aşkın Laçin",
                      title:"Dt.",
                      bolum:"Ağız ve Diş Sağlığı",
                      tarih:"1956",
                      lang:[
                        {id:0,adi:"İngilizce"},
                      ],
                      education:[
                        {id:1,adi:"ANKARA ÜNİVERSİTESİ DİŞ HEKİMLİĞİ FAKÜLTESİ - 1980",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"T.C. Ziraat Bankası Kadıköy Sağlık Polikliniği - Diş hekimi / İstanbul (1982-2007)",yil:"1900"},
                        {id:1,adi:"Diş Hekimi - Özel Muayenehane / İstanbul (2007-2009)",yil:"1900"},
                        {id:2,adi:"İstanbul Dentestetik Diş Polikliniği - Diş Hekimi, Mesul Müdür / İstanbul (2009-2011)",yil:"1900"},
                        {id:3,adi:"Dentalform Estetik Diş Klıniği - Diş Hekimi / İstanbul (2011-2015)",yil:"1900"},
                        {id:4,adi:"Diş Hekimi, Clinic International BMC Tip Merkezi, Bodrum (2015-... ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/tahir-askin-lacin.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:11,
                      name:"Özgür Bayındır",
                      title:"Dr.",
                      bolum:"Acil Hekimi",
                      tarih:"11.04.1973",
                      birthlocation:"Tire",
                      lang:[
                        {id:0,adi:"İngilizce"},
                        {id:1,adi:"İspanyolca"},
                      ],
                      education:[
                        {id:1,adi:"İSTANBUL ÜNİVERSİTESİ ÇAPA TIP FAKÜLTESİ 1991-1998",yil:"1900"},
                        {id:1,adi:"İZMİR BORNOVA ANADOLU LİSESİ 1985-1991",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"LÖSEMİLİ ÇOCUKLAR VAKFI 1999-2000",yil:"1900"},
                        {id:1,adi:"BRISTOL MYERS SQVIBA, CLINIC RESEARCH ASSOCIATE 2000-2001",yil:"1900"},
                        {id:2,adi:"CLINIC INTERNATIONAL TIP MERKEZİ, BODRUM 2001- ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/dr-ozgur.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:14,
                      name:"Fikret Mert Acar",
                      title:"Uzm. Dr.",
                      bolum:"Kardiyolji",
                      tarih:"25.03.1979",
                      birthlocation:"Manisa",
                      education:[
                        {id:1,adi:"2003-Hacettepe Üniversitesi Tıp Fakültesi",yil:"1900"},
                        {id:2,adi:"2010-İstanbul Üniversitesi Kardiyoloji Enstitüsü",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/fikret-mert-acar.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:15,
                      name:"Pelin Akıncı Demiray",
                      title:"Dt.",
                      bolum:"Ağız ve Diş Sağlığı",
                      tarih:"15.08.1992",
                      lang:[
                        {id:0,adi:"İngilizce"},
                      ],
                      birthlocation:"Edirne",
                      education:[
                        {id:1,adi:"İSTANBUL ÜNİVERSİTESİ DİŞ HEKİMLİĞİ FAKÜLTESİ",yil:""},

                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/yok.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:16,
                      name:"Alp Kemal Okyay",
                      title:" Uzm.Dr.",
                      bolum:"Aile Hekimliği",
                      tarih:"25.03.1967",
                      lang:[
                        {id:0,adi:"İngilizce"},
                      ],
                      birthlocation:"Ankara",
                      education:[
                        {id:1,adi:"ANKARA ÜNİVERSİTESİ TIP FAKÜLTESİ",yil:""},
                        {id:2,adi:"ANKARA EĞİTİM VE ARAŞTIRMA HASTANESİ AİLE HEKİMLİĞİ",yil:""},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/yok.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                  ]
                }
              },
            },
            {
              id:192,
              paths:'/anlasmali',
              adi:"Anlaşmalı Kurumlar",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              subMenu:[
                {
                  id:1678361,
                  paths:'/yerlisigortalar',
                  birimAdi:"Anlaşmalı Kurumlar ",
                  adi:"Yerli Sigortalar",
                  sidemenuVisible:true,
                  exact:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Yerli Sigortalar",
                    icerikContext:[
                        {
                            tag:"p",
                            icerik:"Anlaşmalı Olduğumuz Kurumlar",
                            classname:"content-p-big",
                        },
                        {
                          tag:"li",
                          icerik:"AMERİCAN LİFE SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"EUREKO SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"YAPIKREDİ SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"ANADOLU SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"GROUPAMA SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"DEMİR HAYAT SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"AXA SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"ACIBADEM SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"ERGO SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"PROMED",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"RAY SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"GÜNEŞ SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"AK SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"MEDNET",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"ALLİANZ SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"AMERİCAN LİFE SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"MAPFRE GENEL YAŞAM SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"AKBANK T.A.Ş.TEKAÜT SANDIĞI VAKFI",
                          classname:"content-p",
                        },
                    ]
                  },
                },
                {
                  id:16267247278361,
                  paths:'/yabancisigortalar',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Yabancı Sigortalar",
                  sidemenuVisible:true,
                  exact:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Yabancı Sigortalar",
                    icerikContext:[
                      {
                          tag:"p",
                          icerik:"Anlaşmalı Olduğumuz Kurumlar",
                          classname:"content-p-big",
                      },
                      {
                        tag:"p",
                        icerik:"İngiltere",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:"Assistance International",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"ChargeCare International",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Intergroup Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"AXA Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Europ Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Thompson Travel Insurances",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Cega",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"First Assist",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Speciality Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Hollanda",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:"Avero Achmea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Zorgenzekerheid",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"IZZ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"VGZ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ohra-Delta Lloyd",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial-Elvia",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"DSW",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Europeesche Verzekeringen",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Eurocenter",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"SOS International (Ass.Company)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"ANWB(Ass.Company)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Eurocross International (Ass.Company)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Neckermann Travel Insurances",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"FBTO",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Zilveren Kruis Achmea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"IZA",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"DeFriesland",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Unive",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Menzis",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"CZ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"OZ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Groene Land Achmea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Trias",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Fransa",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:"AXA Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Europ Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mutuaide Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial-Elvia",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"France Secours",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Belçika",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:"Eurocross Belgium ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Interassistance-Europese",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Thomas Cook Travel Insurances",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ethias",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Interpartner Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial-Elvia",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:85683273,
                  paths:'/oteller',
                  birimAdi:"Anlaşmalı Kurumlar ",
                  adi:"Oteller",
                  sidemenuVisible:true,
                  exact:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Otelleri",
                    icerikContext:[
                        {
                            tag:"p",
                            icerik:"ANlaşmalı Olduğumuz Oteller",
                            classname:"content-p-small",
                        },
                        {
                          tag:"li",
                          icerik:"Voyage Torba (Torba)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Voyage Bodrum Charm (Bardakçı)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Voyage Hebilköy (Türkbükü)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Blue Dreams Hotel (Torba)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Bodrum Holiday Resort (İçmeler)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Club Ersan (İçmeler)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Forever Hotel (İçmeler)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Manuela Hotel (Bitez)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Işıl Club Milta",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Rixos Hotel",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Jumeirah Hotel",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Vogue Hotel",
                          classname:"content-p",
                        },
                    ]
                  },
                }
              ],
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Deneme Başlığıdğr",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Deneme psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  }
                ]
              },
            },
            {
              id:200,
              paths:'/fotogaleri',
              adi:"Foto Galeri",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Galeri",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Hastanemizden Kareler",
                    classname:"content-p",
                  },
                ],
                fotogallery:true
              },
            },
            {
              id:210,
              paths:'/ik',
              adi:"İnsan Kaynakları",
              sidemenuVisible:true,
              exact:false,
              insan_kaynaklari:true,
              componenti:0,
              emaillang:{
                kisisel:"Kişisel Bilgiler",
                egitim:"Eğitim Bilgileri",
                sonmezunOlunan:"Son Mezun Olunan Okul :",
                yabanciDil:"Yabancı Dil",
                pozisyon:"Pozisyon :",
                basvuruTarih:"Başvuru Tarihi",
                adSoyad:"Ad Soyad :",
                dogumYeri:"Doğum Yeriniz :",
                dogumTarih:"Doğum Tarihiniz :",
                cinsiyet:"Cinsiyet :",
                uyruk:"Uyruğu",
                mesaj:"Mesaj :",
                gonder:"Gönder",
                cep:"Cep Telefonu :",
                randevu_birim:"Randevu İstediğiniz Birim :",
                randevu_tarih:"Randevu İstediğiniz Tarih :",
                randevu_saat:"Randevu İstediğiniz Saat :",
                aciklama:"Açıklama"
              },
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"İŞ BAŞVURU FORMU",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:`Vizyon Misyon ve kurumsal değerlerimiz doğrultusunda nitelikli,eğitim düzeyi yüksek, yeniliklere açık, kendisini ve işini geliştirme potansiyeli olan, takım çalışması yapabilen kişileri istihdam etmek ve bu kişileri sürekli eğitim programları, etkin performans ve ödüllendirme sistemleri ile geliştirmeyi hedefliyoruz. `,
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:`İş Başvuru Formu:`,
                    classname:"content-p-small",
                  },
                ]
              },
            },
            {
              id:220,
              paths:'/gorusoneri',
              adi:"Görüş ve Önerileriniz",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              email_gorus:true,
              emaillang:{
                adSoyad:"Ad Soyad :",
                mesaj:"Mesaj :",
                gonder:"Gönder"
              },
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Görüş ve Önerileriniz",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Görüş ve önerileriniz bizim için önemlidir. Lütfen görüş ve önerilerinizi bizimle paylaşın. ",
                    classname:"content-p",
                  },
                ]
              },
            },
            //randevu
            {
              id:230,
              paths:'/e_randevu',
              adi:"E-Randevu",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              emaillang:{
                adSoyad:"Ad Soyad :",
                mesaj:"Mesaj :",
                gonder:"Gönder",
                cep:"Cep Telefonu :",
                randevu_birim:"Randevu İstediğiniz Birim :",
                birimlang:[
                  {adi:"Aile Hekimliği"},
                  {adi:"Genel Cerrahi"},
                  {adi:"Kardiyoloji"},
                  {adi:"Gastroenteroloji"},
                  {adi:"Üroloji"},
                  {adi:"Deri ve Zührevi Hastalıklar"},
                  {adi:"Kadın Hastalıkları ve Doğum"},
                  {adi:"İç Hastalıkları"},
                  {adi:"Ortopedi ve Tramvatoloji"},
                  {adi:"Göz Hastalıkları"},
                  {adi:"Kulak Burun Boğaz"},
                  {adi:"Diş Hekimliği"},
                ],
                randevu_tarih:"Randevu İstediğiniz Tarih :",
                randevu_saat:"Randevu İstediğiniz Saat :",
                aciklama:"Açıklama"
              },
              email_gorus:false,
              email_randevu:true,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"E-Randevu",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Online randevu talebinizi burdan oluşturabilirsiniz..",
                    classname:"content-p",
                  },
                ]
              },
            },
            {
              id:19451812142,
              paths:'/hastahikayeleri',
              adi:"Hasta Hikayeleri",
              sidemenuVisible:false,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Hasta Hikayeleri",
                icerikContext:[
                  {
                    tag:"div",
                    icerik:"Hasta Hikayeleri",
                    classname:"content-p-big",
                  },
                  {
                    tag:"p",
                    icerik:"",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Tops Nadine, hotel Voyage Bodrum Thanks very much",
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:"Hallo,",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Via deze weg willen we jullie erg hard bedanken voor de goede zorgen die jullie hebben gegeven.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Dankzij jullie dokters,verplegers,al het andere personeel en vooral de nederlandstalige mevrouw voelden we ons meer dan comfortabel in",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Clinic International. We bedanken jullie hiervoor,en willen we ook nog meegeven dat we onze reis hebben voortgezet naar Kos.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Ondertussen zijn we terug thuis en daarom dit mailtje.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Nogmaals bedankt,",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Nadine Tops,",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Ik was bij jullie van 29/05 tot 30/05/2013.",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"div",
                    icerik:"Dear Dr. Ozgur and all your wonderful staff ...",
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:`At first: I'm sorry it has taken me so long to write this....
                    I want to thank you and your staff for the care I received during my recent stay at Clinic International. Thank you for all you do. It means a lot. You guys do a lot for so many. I dont know where I would be without your great help.`,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`I especially want to thank Dr Omur- one of the sweetest and friendliest doctor-, Nurse Duygu ( my angel) and her fiancé Mehmet, Ebru, Baris , Bulent and Steve. But I never met a doctor in my life like Dr. Ali, that has shown so much compassion for his patient as he did for me! I have to tell that every single one of them gave me excellent care/ attention/ emotional support! My stay in the hospital was a bit more tolerable because of your excellent and very profesisional care ! We need a Clinic International in every town.`,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Also, a big thank you to the drivers of the buses. They were very polite caring of their passengers.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Thank you all much more than I can ever express !",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`with Kind Regards
                    Leyla Karnaz ( Holland )`,
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:"Met vriendelijke groet,",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"div",
                    icerik:"Dear Clinic-international staff,",
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:`Today I visited an eye specialist in The Netherlands. Everything was ok. No problems with the cornea, eye-pressure and sight.`,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`My specialist complimented you with your great care and professional treatment. She said: “you were treated better and more efficient than you would have been in The Netherlands”.`,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Greetings to the medical team, dr. Ömür aydoğan, Laverne Costello Clarke and my personal “Florence Nightengale’ (the stunning blonde ;-)).",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"The last two have made my stay in your clinic very comfortable. Mr. Clarke supplied me with a WiFi-code, so I could talk to my family with LiveView and could find some information about the herpes-zoster-virus.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`Thank you very much! You were great!`,
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"div",
                    icerik:"Dear doctor, ",
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:`I'll like to thank you for the marvellous care!!!
                    Now, at home, I realise how gratefull I am to meet you at the moment I was afraid, miserable, sick and lonely!
                    Except y'are a friend of Octay, I even know your name and I never ask about....
                    Congratulations for the hole team, everybody was friendly, kind and helpfull in the clinic. `,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`With kind regards, `,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Mieke Demandt",
                    classname:"content-p",
                  },
                ]
              },
            },
            {
              id:1891486481,
              paths:'/iletisim',
              adi:"Acil İleitişim Ulaşım",
              sidemenuVisible:false,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Acil İleitişim Ulaşım",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Bilgilerimiz",
                    classname:"content-p-big",
                  },
                  {
                    tag:"p",
                    icerik:"Adres",
                    classname:"content-p-small",
                  },
                  {
                    tag:"p",
                    icerik:"Kıbrıs Şehitleri Cad. No 181 (Antik Tiyatro Çaprazı) 48400 Bodrum - Türkiye",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Telefon",
                    classname:"content-p-small",
                  },
                  {
                    tag:"p",
                    icerik:"0252 313 30 30",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Faks",
                    classname:"content-p-small",
                  },
                  {
                    tag:"p",
                    icerik:"0252 316 00 08",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Email",
                    classname:"content-p-small",
                  },
                  {
                    tag:"p",
                    icerik:"info@clinicinternational.com.tr",
                    classname:"content-p",
                  },
                  {
                    tag:"map",
                    icerik:"info@clinicinternational.com.tr",
                    classname:"content-p",
                  },
                ]
              },
            },
            {
              id:5146918581,
              paths:'/check_up',
              adi:"Check Up",
              sidemenuVisible:false,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Check Up",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Check Up Paketleri",
                    classname:"content-p-big",
                  },
                  {
                    tag:"p",
                    icerik:`CHECK-UP PAKETİ
                    ERKEK (40 YAŞ ALTI)`,
                    classname:"content-p-small",
                  },
                  {
                    tag:"li",
                    icerik:"Diş Taşı Temizliği",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Akciğer Grafisi",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Ekg",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Tam Kan Sayımı",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Tam İdrar Tahlili",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Sedimantasyon",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"li",
                    icerik:"Açlık Kan Şekeri",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Ürik Asit",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Üre",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Kreatinin",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Total Kolestrol",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Hdl Kolestrol",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"li",
                    icerik:"Ldl Kolestrol",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Trigliserit",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Hbsag",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Anti HCV",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Anti Hbs",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Anti Hiv",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"li",
                    icerik:"Aso",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Crp",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Ast, Alt",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Tsh",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"FT3, FT4",
                    classname:"content-p",
                  },
                  //40
                  {
                    tag:"p",
                    icerik:`CHECK-UP PAKETİ
                    ERKEK (40 YAŞ ÜSTÜ)`,
                    classname:"content-p-small",
                  },
                  {
                    tag:"li",
                    icerik:"Diş Taşı Temizliği",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Akciğer Grafisi",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Ekg",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Kontrol PSA",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Tam Kan Sayımı",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Tam İdrar Tahlili",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"li",
                    icerik:"Sedimantasyon",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Açlık Kan Şekeri",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Ürik Asit",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Üre",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Kreatinin",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Total Kolestrol",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"li",
                    icerik:"Hdl Kolestrol",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Ldl Kolestrol",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Trigliserit",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Hbsag",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Anti HCV",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Anti Hbs",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Anti Hiv",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"li",
                    icerik:"Aso",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Crp",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Ast, Alt",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Tsh",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"FT3, FT4",
                    classname:"content-p",
                  },
                  //40
                  {
                    tag:"p",
                    icerik:`CHECK-UP PAKETİ
                    KADIN`,
                    classname:"content-p-small",
                  },
                  {
                    tag:"li",
                    icerik:"Diş Taşı Temizliği",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Akciğer Grafisi",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Ekg",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Kontrol PSA",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Tam Kan Sayımı",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Tam İdrar Tahlili",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"li",
                    icerik:"Sedimantasyon",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Açlık Kan Şekeri",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Ürik Asit",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Üre",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Kreatinin",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Total Kolestrol",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"li",
                    icerik:"Hdl Kolestrol",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Ldl Kolestrol",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Trigliserit",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Hbsag",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Anti HCV",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Anti Hbs",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Anti Hiv",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"li",
                    icerik:"Aso",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Crp",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Ast, Alt",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Tsh",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"FT3, FT4",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"RF",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"p",
                    icerik:`Gastroenterolojik Check-Up`,
                    classname:"content-p-small",
                  },
                  {
                    tag:"li",
                    icerik:"AKŞ",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"KREATİNİN",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"KOLESTEROL",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"LDL-C",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"AST",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"ALT",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"li",
                    icerik:"ALP",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"GGT",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"LDH",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"DR./İNDR. BİLİRÜBİN",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"ALBÜMİN",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"PTZ",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"li",
                    icerik:"TSH",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"HBSAg",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Anti-HCV",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"CEA",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"CA199",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"AFP",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"HbA1C",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"li",
                    icerik:"HpSAg",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"Anti-TTG IgA",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"FERRİTİN",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"B12",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"GGK",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"EKG",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"AKCİĞER GRAFİSİ",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"TÜM BATIN USG",
                    classname:"content-p",
                  },
                  {
                    tag:"li",
                    icerik:"ÜST (ÖZEFAGASTRODUODENOSKOPİ) ve ALT GİS (İLEOPANKOLONOKSOPİ) ENDOSKOPİSİ",
                    classname:"content-p",
                  },

                ]
              },
            },
            {
              id:3253291,
              paths:'/video',
              adi:"Video Galeri",
              sidemenuVisible:false,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Video Galeri",
                icerikContext:[
                  {
                    tag:"div",
                    icerik:"AĞIZ VE DİŞ SAĞLIĞI ÇOCUKLUKTAN BAŞLIYOR",
                    classname:"content-p-big",
                  },
                  {
                    tag:"p",
                    icerik:"",
                    classname:"content-p",
                  },
                  {
                    tag:"video",
                    icerik:"https://www.youtube.com/watch?v=nqIJq64YEDk",
                    classname:"content-p-small",
                  }
                ]
              },
            },
           ]
        },
        en: {
          baslik:"ENTRUST YOUR HEALTH",
          anasayfa:"HOME",
          veri:[
            //110
            {
              id:110,
              paths:'/',
              adi:"Home",
              sidemenuVisible:false,
              exact:true,
              componenti:1,
              slider:[
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webgeçiştirmeyin.jpg",
                  link:"http://bit.ly/2EgnDBD"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webmiyom.jpg",
                  link:"http://bit.ly/2H1njUR"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webtansiyon.jpg",
                  link:"http://bit.ly/2nLiwiw"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webterleme.jpg",
                  link:"http://bit.ly/2G0IK7e"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webtipIIdiyabet.jpg",
                  link:"http://bit.ly/2nQdX6t"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webtüpmide.jpg",
                  link:"http://bit.ly/2C8TC0i"
                },
                {
                  img:"http://media.kenttv.net/20180204062659_kenttv_haber_606.jpg?v=1",
                  link:"https://www.kenttv.net/haber.php?id=40834"
                },
                {
                  img:"http://media.kenttv.net/20180221040704_kenttv_haber_606.jpg?v=1",
                  link:"http://www.kenttv.net/haber.php?id=41015"
                },
              ],
              widget:[
                {
                  id:"widget1",
                  link:"/hastahikayeleri",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"PATİENT STORİES",
                  widgetpic:"http://www.kadinhaberleri.com/images/haberler/kil_donmesi_kadinlarda_olur_mu_h600513.jpg"
                },
                {
                  id:"widget2",
                  link:"/iletisim",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"COMMUNICATION",
                  widgetpic:"https://www.framestr.com/wp-content/uploads/2017/08/emergency-contact-form.jpg"
                },
                {
                  id:"widget3",
                  link:"/check_up",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"CHECK-UP",
                  widgetpic:"https://acibademmobil-wbeczwggocwcfpv3pwm.stackpathdns.com/wp-content/uploads/2017/01/bakicim-check-up-doktor-muayenesi.jpg"
                },
                {
                  id:"widget4",
                  link:"/hakkimizda",
                  classname:"widgetsecond",
                  classwrap:"widget-box2",
                  title:"VİDEO GALLERY",
                  widgetpic:"http://ahps.org.sg/wp-content/uploads/photo-gallery/41144817-HEALTH-word-cloud-concept-Stock-Photo-health.jpg"
                },
                {
                  id:"widget5",
                  link:"/hakkimizda",
                  classname:"widgetsecond",
                  classwrap:"widget-box2",
                  title:"HEALTHY INFORMATION",
                  widgetpic:"https://i.pinimg.com/736x/c4/db/c0/c4dbc0f1f6c23496f433ac82bc4cef83--healthy-tips-healthy-eating.jpg"
                },
              ],
              haberlerbaslik:"NEWS and ANNOUNCEMENTS",
              haberler:[
                {
                  id:"widget1",
                  link:"/hakkimizda",
                  mesaj:`6 yıldır Bodrum'da başka sağlık kuruluşlarında çalışan Kardiyoloji ( Kalp Hastalıkları ) Uzmanı Doktor Fikret Mert Acar kurumumuzda SGK anlaşmalı olarak çalışmaya başlamıştır, kendisine başarılar diliyoruz.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
                {
                  id:"widget2",
                  link:"/hakkimizda",
                  mesaj:`5 Yıldır Bodrum'da başka bir sağlık kuruluşunda çalışan Kadın Hastalıkları ve Doğum Uzmanı Op.Dr. Ömer Orçun Koçak merkezimizde SGK anlaşmalı olarak çalışmaya başlamıştır, kendisine başarılar diliyoruz.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
                {
                  id:"widget3",
                  link:"/hakkimizda",
                  mesaj:`Genel Cerrahi Uzmanı Doçent Doktor Cengiz Kayahan SGK anlaşmalı olarak hasta kabulüne başlamıştır kendisine başarılar diliyoruz.Hekimimiz özellikle obezite cerrahisi (mide küçültme ameliyatları ) , tiroid cerrahisi ( guatr ameliyatları ) bağırsak ameliyatları, ve meme cerrahisi konusunda üstün deneyime sahiptir.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
                {
                  id:"widget4",
                  link:"/hakkimizda",
                  mesaj:`Kulak Burun Boğaz Uzmanı Op. Dr. Lemi Özer merkezimizde SGK anlaşmalı olarak çalışmaya başlamıştır. 3 yıl boyunca Acıbadem Bodrum Hastanesi'nde görev yapan hekim, başarılı meslek hayatına tıp merkezimizde devam edecektir.  Kendisine başarılar diliyoruz.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
              ],
              icerikveri:{
                icerikImage:"https://paratic.com/dosya/2015/11/kadinlari-cezbeden-birbirinden-cekici-araba-modelleri-bugatti-veyron.jpg",
                icerikHeader:"Deneme Başlığıdğr",
                icerikContext:"Yukardakinin aynısı"
              },
            },
            //110bitis
            //120
            {
              id:120,
              paths:'/creator',
              adi:"About Us",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"About Us",
                icerikContext:[
                  {
                    tag:"div",
                    icerik:"Bodrum is a holiday paradise and a dream come true for thousands of people from around the world. However, sometimes unexpected diseases and accidents can turn your well planned holiday up side down. ",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"It becomes a source of distress and anxiety when unexpected situations such as accidents, diseases and injuries happen in unfamiliar country,city or culture . ",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"We understand your fears very well and strive to create a comfortable, safe and peaceful environment for you in Clinic International with our friendly and modern service concept. We are at your service for 24 hours to find out solutions to your problems as soos as possible(ASAP) and make you feel at home. ",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Private Clinic International Medical Center is a boutique clinic with its outstanding, friendly and experienced staff, fast and definitive service approach. We are increasing the service quality by following and updating the medical and technological developments in the health sector with respect to patient rights and ethics.",
                    classname:"content-warning",
                  },
                ]
              },
            },
            //120bitis
            //130
            {
              id:130,
              paths:'/tibbibirimler',
              adi:"Medical Units",
              sidemenuVisible:true,
              exact:false,
              subMenu:[
                {
                  id:131,
                  paths:'/kadin',
                  birimAdi:"Medical Units ",
                  adi:"Gynecology",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Gynecology",
                    icerikContext:[
                      {
                        tag:"li",
                        icerik:"Women's health inspection",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Pregnancy monitoring",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Control examination after birth",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ultrasonography",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Menapouse follow-up",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Family planning application",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Infertility follow-up",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mestruel disorders follow up",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Cervical control and biopsies",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Birth",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Gynecological operations",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Pap-smear test (Cervical smear)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Breast examination",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Endometrial biopsy",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Intrauterine device (RIA) insertion",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Gynecology is made by our experienced gynecologist in our clinic.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:132,
                  paths:'/ortopedi',
                  birimAdi:"Medical Units ",
                  adi:"Orthopaedics and Traumatology",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Orthopaedics and Traumatology",
                    icerikContext:[
                      {
                        tag:"li",
                        icerik:"Sports Injuries",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Muscular-skeletal system fractures and soft tissue injuries' surgical and non-surgical treatments",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Spine fractures, injuries and infections",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Surgical intervantion of knee, hip and shoulder joint calcifications",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Every arthroscopic approach",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Peripheral nevre impingements, carpal nerve syndrome,ulnar tunnel syndrome,",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Posterior interosseous syndrome",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Open and closed reduction of fractures",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Closed fructure surgeries ( minimal invasive surgery)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Corrective surgery of orthopedic malformations",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"All kinds of joint prosthesis placement (hip, elbow, finger, shoulder..)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Surgery of curvature of the spine and fractures",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:133,
                  paths:'/cocuk',
                  birimAdi:"Medical Units ",
                  adi:"Peadiatrics",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Peadiatrics",
                    icerikContext:[
                      {
                        tag:"li",
                        icerik:"Examinations related to all stages of childhood diseases between the ages of 0-15 including follow up and treatments",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Growth development monitoring",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Routine and special vaccines",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Child education consultatıons",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Nutrition and breastfeeding counseling",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Newborn screening tests made by our specialist",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Our clinic is made by our specialist physician.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:134,
                  paths:'/acil',
                  birimAdi:"Medical Units ",
                  adi:"Emergency Services",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Emergency Services",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Emergency service is the department where first intervention and treatment is made for many emergencies such as incisions, fractures, falls, high fever, abdominal pain, stomach bleeding, stroke, traffic accidents, heart attacks, heart rhytm disorders, cardiopulmonary arrest, assaults, poisonings, suicide attempts. ",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"In our emergency department , all patients receive first intervention treatment for all emergency diseases.",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"In our emergency department, 9 patient rooms, 3 observation rooms and an emergency room are available.",
                        classname:"content-p",
                      }
                    ]
                  },
                },
                {
                  id:135,
                  paths:'/agiz',
                  birimAdi:"Medical Units ",
                  adi:"Dentistry",
                  sidemenuVisible:true,
                  componenti:0,
                  subMenu:[
                    {
                      id:135256,
                      paths:'/imp',
                      birimAdi:"Medical Units ",
                      adi:"İmplantoloji ve Cerrahi Uygulamalar",
                      sidemenuVisible:true,
                      subMenu:[
                        {
                          id:262626131236,
                          paths:'/implant',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"İmplant",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"İmplant",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"İmplantoloji",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Diş kayıplarında tercih edilebilecek en iyi yöntem implanttır.
                                İmplantlar insan vücudu tarafından kolay kabuledilebilen ve çene kemiği içinde kök görevini gören titanyumdan yapılırlar.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`İmplantoloji denenmiş ve başarı sağlanmış bir tedavidir. Günümüz modern implantların %90’ı enaz 15 yıl kullanım sağlamaktadır. İmplantın başarısında kemiğin durumu çok önemlidir. Operasyon öncesinde hem klinik hem de radyolojik araştırma detaylı olarak yapılarak kemiğin durumu değerlendirilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Operasyon lokal anestezi altında yapılır ve bir implantın yerleştirilmesi yaklaşık 15-20 dakika alır.
                                Diş etinden flap kaldırılıp implant yerleştirildikten sonra diş etine tekrar dikiş atılır. İmpalantın yerleştirilmesinden sonra iyileşme 2-4 ay arası sürecektir. Kemik dokusu ve implantın kaynaştığı bu süre içinde beklemek gerekmektedir. Bu sürenin ardından implant kemik içine adapte olur. Daha sonra üst yapı yapılması için kapalı implantın üzeri açılır ve abutmentlar yerleştirilerek ölçü alınır. Daha sonra üst kuronlar hazırlanır, bu işlem 5-7 gün almaktadır.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:32424364,
                          paths:'/gom',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Gömük Diş Çekimi",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Gömük Diş Çekimi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Diş Çekimi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Basit Çekim",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Komplike Çekim ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Cerrahi Operasyon",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Diş çevresinde iyileşmeyen kronik iltihap",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dolgu ya da kuronla tamir edilemeyecek kadar diş dokusu kaybı olan dişler",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Çevre kemik dokusunda aşırı rezorbsiyon oluşmuş ve bu nedenle mobil hale gelmiş dişler",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Pozisyonu bozuk dişler",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Ortodontik tedavi için yer kazanılması gereken durumlarda diş çekimi yapılır.",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Diş çekimi öncesinde dijital röntgen çekilerek, olası komplikasyon riski minimize edilir.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kliniğimizde bütün komplike çekimler ve ağız içi cerrahi operasyonlar cerrahi uzmanı diş hekimleri tarafından yapılmaktadır.",
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                      ],
                      componenti:0,
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Deneme Başlığıdğr",
                        icerikContext:[
                          {
                            tag:"p",
                            icerik:"Teşhis ve tedavi plan",
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Bu nu bilmiyorum psidir.",
                            classname:"content-p",
                          }
                        ]
                      },
                    },
                    {
                      id:134546,
                      paths:'/kozmadis',
                      birimAdi:"TIBBİ BİRİMLER ",
                      adi:"Kozmetik Diş Hekimliği",
                      sidemenuVisible:true,
                      componenti:0,
                      subMenu:[
                        {
                          id:1515135,
                          paths:'/bleaching',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Bleaching",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Bleaching",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Bleaching",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Birçok araştırmaya göre dişler yüz estetiğini belirleyen ikinci en önemli kriterdir. Dişlerin renkleride kesinlikle biçim ve dizilişleri kadar önemlidir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Beyazlatma, parlak beyaz bir gülümsemeye sahip olmayı sağlayan en kolay ve ağrısız tedavidir. Basit ve güvenli olan bu tedavi son derece çarpıcı sonuçlar ortaya çıkarmaktadır. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kliniğimizde BDHF tarafından tavsiye edile, sonderece güvenli bir beyazlatma sistemi uygulanmaktadır. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Hastalarımız diş hekimi koltuğunda rahat bir şekilde uzanıp müzik dinlerken beyazlatma işlemi şu işlemlerde gerçekleştirilir.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"İlk adımda hastanın dudaklarını ve dişetlerini özel bariyerlerle izole edilerek beyazlatma ilacının dişetileri ve yumuşak dokularla teması engellenir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Ardından beyazlatma jeli dişlerin yüzeyine uygulanarak ışınla aktivasyonu sağlanır. Daha sonra jel diş yüzeyinden uzaklaştırılarak renk değişimi kontrol edilir. Bu işlem istenilen renge ulaşılıncaya kadar birkaç kez tekrar edilebilir.",
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:34621689,
                          paths:'/porselen',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Porselen Laminate",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Porselen Laminate",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Porselen Laminate",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Porselen laminate dişlerinizin şeklini, boyutunu, rengini ya da pozisyonunu değiştirerek gülüşünüzü güzelleştirmek amacıyla hazırlanana ince porselen bir tabakadır. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"En az miktarda diş materyali aşındırılarak, uzun süreli, mükemmel bir görünüşün sağlandığı koruyucu bir tekniktir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Özel rezin simanlarla dişe yapıştırılırlar ve düşmeleri çok zordur. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Porselen yüzeyi lekelenmelere, boyanmalara karşı oldukça dayanıklıdır, estetik ve doğal bir görünüm sağlarlar ve de diş etiyle çok uyumludurlar. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Laminatelerin hazırlanması 1 hafta kadar sürmektedir, bu sürede de 2-3 randevu gerekmektedir:
                                İlk randevuda diş yüzeylerinden bir miktar aşındırma yapılarak dişler laminate için hazırlanır. Ardından ölçü alınıp geçici restorasyon materyali ile açık dentin yüzeyi kapatılır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"İkinci randevuda geçiciler kaldırıldıktan sonra laboratuar tarafından hazırlanmış olan laminateler prova edilip, hasta ve hekim hemfikir ise özel resin simanlarla yapıştırılır. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Tedavi planları kişiden kişiye değişiklik göstermektedir. Sizin için en uygun tedavi seçeneği ve planlaması için dişhekimine muayene olmanız şarttır.",
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:5848237,
                          paths:'/bonding',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Bonding",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Bonding ve Estetik Dolgular",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Bonding",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Dental bonding estetik amaçla kullanılan bir tür dolgu materyalidirDişlerdeki küçük boşlukları kapatmak, dişleri yeniden şekillendirmek ve mine yüzeyinde oluşan doğal hasarı kapatmak için rutin olarak kullanılan estetik restorasyon materyalleridir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kompozit dolgu materyalinin çeşitli renk tonları vardır. Böylece dişinizin kendi rengine kolaylıkla uyum sağlar. Geleneksel amalgam dolgularınız kompozit dolgularla değiştirilebilir. Böylece dişleriniz sağlıklı bir ağız ortamıyla beraber estetik ve doğal bir görünüşe sahip olmuş olur. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kliniğimizde yüksek kalitede, dayanıklı ve kuvvetli kompozit materyali kullanmaktayız. Kullandığımız tüm malzemeler FDA, ADA ve Türk Sağlık Bakanlığı tarafından onaylıdır.",
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:9840059,
                          paths:'/smile',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Smile Design",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Smile Design",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Smile Design",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Gülüşünüzden memnun musunuz?",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dişlerinizin şekli ve rengi sizi rahatsız ediyor mu?",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Hiç başka birinin gülüşüne sahip olmak istediniz mi?",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dişetlerinizin görüntüsü sizi mutlu ediyor mu?",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Gülmek,yüzümüzdeki pek çok kas grubunun çalışmasıyla oluşan ve dişlerimizin göründüğü bir mimiktir. Gülüşümüzü etkileyen pek çok faktör vardır. Güzel bir gülüş için öncelikle yüz biçiminizle ,dudak yapınızla uyumlu dişlere sahip olmanız gerekir. İşte gülüş dizaynı, size kişisel özellikleriniz ve isteklerinizle doğallık ve fonksiyonu birleştirerek sizin için en ideal gülümsemeyi oluşturmak için vardır. Pırıl pırıl bir gülümseme hepimizin hoşuna gider. O kişi hakkında olumlu hisler hissetmemizi sağlar. Araştırmalar gösteriyor ki, dişler gözlerden sonra en önemli ikinci estetik unsurdur. Ahenkli bir gülümseme iletişimde her zaman avantaj sağlar. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"GÜLÜŞÜMÜZÜ ETKİLEYEN FAKTÖRLER",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"CİNSİYET",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"YÜZ ŞEKLİ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"YAŞ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"DUDAKLAR",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"DİŞ ETLERİ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"DİŞLER",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`1-CİNSİYET: Kadın ve erkek anatomisi birbirinden farklıdır. Erkeklerde yüz hatları daha keskin ve belirgindir.Alın burun, çene ucu orantısı kadın yüzüyle farklılıklar gösterir.Kadınlarda geçişler daha yumuşak burun ve kaş kemerleri daha siliktir. Dişlerde de aynı paralellik vardır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kadınlarda:",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Gülme hattı yukarı doğru kavislidir ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dişlerin köşeleri daha yumuşak döner. ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Komşu dişlerin köşeleri arasında minik aralıklar vardır.",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Ortadaki iki diş yandaki dişlerden biraz daha uzundur. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Erkeklerde:",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Gülme hattı daha düzdür. ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dişlerin hatları daha belirgindir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Komşu dişler daha düz bir hatta birleşirler ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`2-YÜZ ŞEKLİ: Yüz şeklinizle diş formlarınız arasında benzerlikler bulunur. Genellikle uzun yüzlü kişilerde diş formları da uzun; kare veya yuvarlak yüzlü kişilerde diş formları da kare veya yuvarlak olur. Klasik diş hekimliğinde bu benzerlikler korunmaya çalışılarak restorasyon yapılırdı. Ancak estetik yönden bir şeyleri değiştirmek istediğinizde bu benzerlikleri tersine çevirerek farklı ifadeler veren illüzyonlar elde edilebilir. Örneğin uzun yüzlü bir kişiye dikdörtgen formda uzun dişler yapılırsa yüzü olduğundan da uzun görünecektir. Oysa oval veya daha geniş formlar denenerek yüzdeki hoş olmayan uzunluk kamufle edilebilir. Veya yuvarlak yüzlü bir kişiye daha ince uzun formda dişler yapılarak yüzünün daha ince görünmesi sağlanabilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`3-YAŞ: Yaşlanmaya karşı yürütülen savaş her yıl milyonlarca insanı da içine alarak büyüyor. Kadın ve erkek fark etmeden hepimiz genç ve güzel görünmeyi sürdürmek istiyoruz.Kozmetik dişhekimliği bu konuda oldukça önemli yardımlar yapabilir.Gülüşümüz yüzümüzdeki en önemli gençlik kriterlerinden biridir. Yaşla birlikte dişlerimizin renginde bir koyulaşma,boylarında kısalma meydana gelir.

                                Dokuların sarkmasıyla beraber üst dudağımız da yerçekimi etkisiyle aşağıya doğru sarkar. Dikey boyutumuz kısaldığı için çene ucu-burun ucu birbirine olması gerekenden çok yaklaşır. Konuşurken ve gülerken üst dişlerimizin daha az, alt dişlerimizin daha çok, gözükmesi sonucunda daha yaşlı bir ifadeye sahip oluruz. Birçok kişi çok yanlış bir inanışla yaşlanınca sadece takma dişler yaptırabileceklerini sanıyorlar. Oysa dikey boyut yükseltilmesi, diş beyazlatma, kozmetik düzenleme, bonding, laminate veneer, implant gibi yöntemlerle 20 yıl önceki gülüşünüze kavuşmanız hiç de hayal değildir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`4-DUDAKLAR: Tıpkı bir çerçevenin tabloyu şekillendirdiği gibi dudaklar da dişlerimizi ve gülüşümüzü şekillendirirler.Gülüş dizaynı ;dudakların kalın,ince, uzun,kısa vs. olmalarına göre düzenlenerek var olan kusurlar kapatılabilir `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`5-DİŞETLERİ: Dişetleri dişlerimizi en yakından çerçeveleyen aksesuarlardır. Dişlerimiz güzel olsa bile sağlıksız(şiş, kırmızı ve parlak)dişetleri gülüşümüzün çirkin görünmesine neden olurlar.
                                bazen de dişetlerimiz sağlıklıdır ama gülünce gereğinden fazla gözükürler.Ya da dişeti çekilmesi oluşmuştur ve gözükmelerini istediğimiz kadar dişetimiz yoktur

                                Bu gibi durumlarda gülüşümüzde oluşan rahatsız edici görüntüler Varolan dişetlerine sağlık kazandırılarak ve ya dişetlerine küçük estetik müdahaleler yapılarak düzenlenebilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`6-DİŞLER: Günümüzde modern dişhekimliği konseptinde estetik sınırlar içinde dişlerimizi güzel göstermek için yapılabilecek çok fazla alternatifimiz var.Önemli olan sorunları doğru tespit edebilmek ve soruna yönelik, hedefi bulan,minimal müdahale ile maksimum estetiği sağlayabilen tedavileri seçmektir.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:591924,
                          paths:'/dispirlantasi',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Diş Pırlantası",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Diş Pırlantası",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Diş Pırlantası",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Çeşitli şekillerde ve renklerdeki küçük taşlardır. Bazısı kıymetli metal ve taşlardan yapılmış olabilir (altın-gümüş-pırlanta) ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Gülüşünüzün size özel ve süslü olmasını sağlarlar.
                                Diş yüzeyine özel diş bağlayıcılarıyla yapıştırılırlar.
                                Yapıştırılmaları sırasında dişten herhangi bir aşındırma yapılmaz ve eğer istenirse daha sonra diş yüzeyinden tamamen uzaklaştırılabilirler.`,
                                classname:"content-p",
                              }
                            ]
                          },
                        },
                      ],
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Deneme Başlığıdğr",
                        icerikContext:[
                          {
                            tag:"p",
                            icerik:"Deneme psidir.",
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Bu nu bilmiyorum psidir.",
                            classname:"content-p",
                          }
                        ]
                      },
                    },
                    {
                      id:13428286,
                      paths:'/rutin',
                      birimAdi:"TIBBİ BİRİMLER ",
                      adi:"Rutin Dental Tedaviler",
                      sidemenuVisible:true,
                      componenti:0,
                      subMenu:[
                        {
                          id:7848373,
                          paths:'/pedodonti',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Pedodonti",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Pedodonti - Çocuklarda diş tedavisi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Pedodonti - Çocuklarda diş tedavisi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Ağız ve diş sağlığı bebeklikten itibaren çok önemlidir. Her öğün beslenmenin ardından bebeğin yanak- dil- dişeti kretleri ve çevresi temiz gazlı bezle silinip temizlenmelidir. İlk süt dişlerinin sürmesiyle birlikte, bu temizlik işlemine dişler de eklenir. Aksi takdirde kalan yemek artıkları ağız içerisinde kötü kokuya, dişler üzerinde de çürüğe neden olur. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Çocuklarla tedavi her zaman yoğun ilgi ve sabır ister. İlk diş hekimi tecrübesi hayat boyu etkili olacağı için çocuk hasta ile karşılıklı uzlaşma, onu tedavi için doğru motive etme çok önemlidir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Süt dişleri, alttan gelecek daimi dişlere sürme rehberliği yaparlar. Değişim zamanı gelinceye kadar kendi pozisyonlarında, sağlıklı bir şekilde korunmaları gerekir. Çürümeye başlayan süt dişlerine dolgu veya kanal tedavisi gibi koruyucu tedaviler yapılarak, gerek süt dişi ve çevresindeki komşu dişler, gerekse sürecek olan daimi dişlerin ve ağız içi ortamın sağlıklı kalmasına çalışılır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Kliniğimizde her türlü pedodontik müdahale yapılabilmektedir:`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Oral hijyen eğitimi`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Süt ve daimi diş dolguları`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Koruyucu tedaviler(fissür sealant uygulaması, flor uygulamaları)`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Pedodontik kuron`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Yer tutucular`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Kanal tedavisi`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Çekim`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Ortodonti`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:56626,
                          paths:'/kanal',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Kanal Tedavisi",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Kanal Tedavisi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Kanal Tedavisi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Kanal tedavisi pulpa dokusu (dişin kan ve sinir içeren kısmı) kazayla, çürükle ya da enfeksiyonla açığa çıktığı zaman yapılan tedavidir. Özenli çalışmayı gerektiren, zaman alan bu tedavi 2-3 randevuyu kapsar. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kanal ve köklerin sayısı kişiden kişiye ve dişten dişe değişiklik gösterir. Tedavi kök kanallarının içindeki pulpa dokusunun temizlenmesi, kök kanal duvarlarının genişletilmesi ve en son temiz, sağlıklı hale gelen kök kanallarının doldurulması şeklindedir.",
                                classname:"content-p",
                              }
                            ]
                          },
                        },
                        {
                          id:5848237,
                          paths:'/kuron',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Kuron Köprüler",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Kuron Köprüler",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Kuron Köprüler",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Metal destekli porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Değerli metal destekli porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Zirkon porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Tüm porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Diş kuronları çürümüş, kırılmış ya da çatlamış bir dişi onarmak için hazırlanırlar. Ayrıca dişlerin görünüşünü güzelleştirmek, ark içerisindeki pozisyonlarını düzeltmek için de kullanılırlar. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Köprüler dişlere yapıştırılan sabit apareylerdir.
                                Bir ya da daha fazla diş eksikliğinde hazırlanırlar.
                                Doğal dişlerle aynı görünüme ve fonksiyona sahiptirler. Dişlerin hizasını korudukları gibi iyi bir fonksiyon ve estetik sağlarlar.
                                Kayıp diş ya da dişlerin yanında yer alan komşu dişler (ya da implantlar) köprü ayağı olarak kullanılırlar.
                                `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Köprü bu komşu dişlerin üzerine yapıştırılmaktadır.
                                Köprünün kayıp dişleri içeren parçası ortada 'pontik' adı verilen kısımdır ve diş eti üzerine oturur.

                                Köprü ayağı olarak kullanılacak olan komşu dişlere ait diş etlerinin ve çevrelerindeki kemik dokusunun sağlığı köprü başarısında çok önemli bir faktördür.
                                Bu nedenle köprü yapımı öncesinde dokular dikkatlice incelenir, Kuronlar farklı materyallerle hazırlanabilir,
                                `,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Metal destekli porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Değerli metal destekli porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Zirkon porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Tam porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"METAL DESTEKL PORSELEN KURONLAR: ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Klasik tip kuronlardır.
                                Kuronun içinde kullanılan metaller kuronun daha güçlü olmasını sağlar. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"DEĞERLİ METAL DESTEKLİ PORSELEN KURONLAR: ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Kuron içinde farklı içerikli metaller kullanılarak kuron daha az allerjik ve diş etiyle daha uyumlu hale getirilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"TAM PORSELEN KURONLAR: ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Daha çok ön dişlerde, estetiğin yüksek oranda sağlanmasını istediğimiz durumlarda kullanılan metalsiz modern tekniktir. Yapıştırılmaları için özel resin siman sistemleri kullanılmaktadır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"ZİRKONYUM: ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`İyi estetik sonuç elde ederken aynı zamanda güçlülük ve dayanıklılık sağlamak için zirkonyum kullanılır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Zirkonun dayanıklılığı ve translusensisi doğal dişlerle çok fazla benzerlik gösterdiği için estetik diş materyali olarak tercih edilirler.

                                Diş hekimlerimizin dikkatli muayene ve incelemelerinden sonra hastaya kendisine uygulanabilecek tedavi seçenekleri anlatılır, bu tedavi seçenekleri tek tek ele alınıp tartışılarak hastaya en uygun tedavi planlanır.
                                `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Muayene sonrasında tedavi şu aşamaları izler: `,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Dişlerden aşındırma yapılarak kuron için gerekli yer hazırlanır `,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Ölçü alınır`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Geçici köprü/kuron hazırlanır ve yapıştırılır`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Ölçü,kron/ köprünün hazırlanması için diş teknisyenlerine yollanır`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Bir sonraki randevuda, geçici restorasyon çıkarıldıktan sonra laboratuarda hazırlanmış olan kuron/köprü ağız içinde prova edilir`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Kuron/ Köprü cilalanıp son bitirme işlemi yapılır.`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Doğru kapanış ve pozisyonda köprü dişlere yapıştırılır.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:92161,
                          paths:'/protez',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Hareketli Protezler",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Hareketli Protezler",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Hareketli Protezler",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik: `Hareketli protezler ağızdaki kayıp dişlerin yerine akrilik rezinden yapılan takıp çıkarılabilen apareylerdir. Ağızda kalan dişler köprü yapımı için yeterince sağlıklı değilse ya da uygun pozisyonda değilse, kayıp dişlerden oluşan boşlukları doldurmak için parsiyel protez yapımı tercih edilmektedir.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik: `Protezin metal isketli ya da metal iskeletsiz olması ağzın durumuna, dişlerin sayısına bağlıdır. Tamamen dişsiz hastalarda total protezler yapılmaktadır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik: `Dişlerin şekil ve renginin belirlenmesinde de akrilik dişlerin çok çeşitli renk ve şekil seçeneği olması sayesinde hasta için en uygun olanı tercih edilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik: `Eksik dişler yüzünden kaybedilen estetik görünümü, çiğneme fonksiyonunu, konuşma problemlerini tekrardan hastaya kazandırmasına rağmen hastaların harekeli protezleri kullanmaya alışmaları kolay değildir, zaman alıcıdır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik: `Diş hekimlerimiz tedavi planınız yapılırken, size uygulanabilecek tüm protez seçenekleri hakkında sizi bilgilendirecektir.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:591924,
                          paths:'/periodontal',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Peridontal Tedavi",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Peridontal Tedavi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Peridontal Tedavi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Sağlıklı dişlere ve sağlıklı bir ağıza sahip olmada ilk adım sağlıklı diş etleridir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Diş yüzeyinde oluşan, bakteri içeriği zengin plak tabakası tükrük içeriğiyle birleşerek sertleşir ve diş taşı haline gelir. Diş taşları dişlerin yüzeyinde yer alabileceği gibi diş etinin üstünde ve altında da birikebilir.
                                Sigara, çay, kahve vb. gıdalar diş ve diş taşı yüzeylerinde renklenmeye neden olur. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Dişeti altında biriken diş taşları dişetini kolaylıkla iltihaplı hale getirir. Diş taşları ve dişetlerindeki iltihap zamanla çevre kemik dokusunda yıkıma sebep olur. Bu durum dikkate alınmazsa dişler çevresindeki kemik desteğini kaybederek zamanla mobil hale gelir. Önlem alınmadığı takdirde bu yıkım dişlerin ağızdan düşmesiyle sonlanır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Plak içinde bulunan bakteriler sadece diş ve dişetleri için tehlikeli değildir, bunlar akciğer, kalp gibi yaşamsal organlar için de öldürücü olabilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`İyi bir ağız hijyeni ,düzgün ağız bakımı ve sağlıklı dişetleri, yapılacak olan diğer restorasyonların başarısı için ilk adımdır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Dişeti iltihabının tedavisi diş taşlarının uzaklaştırılmasıyla mümkündür.

                                Kliniğimizde uygulanan dişeti tedavilerinde, ultrasonik temizleyici cihazlar ve özel el aletleriyle,gerek diş yüzeyindeki plağı ,gerekse dişeti altındaki, diş taşlarını temizliyor ve lekelenmeleri ortadan kaldırıyoruz. Ayrıca çok derin diştaşları ve tortuları da derin küretajla temizliyoruz. Son olarak kullandığımız özel macunlarla diş yüzeylerini fırçalayarak diş yüzeylerinin parlak ve pürüzsüz hale gelmesini sağlıyoruz. Bu şekilde pürüzsüz hale gelen diş yüzeylerine plağın tekrardan tutunması da zorlaşmış oluyor.

                                Bu prosedür kanamasız, sağlıklı dişetlerine sahip olmanızı amaçlamaktadır ve dişetlerinin durumuna göre bazen birkaç seans sürebilmektedir. Bu tedavi her 6 ayda bir tekrarlanmalıdır.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:65281321,
                          paths:'/konservatif',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Konservatif Tedavi",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Konservatif Tedavi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Konservatif Tedavi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Sağlıklı dişlere ve sağlıklı bir ağıza sahip olmada ilk adım sağlıklı diş etleridir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Diş yüzeyinde oluşan, bakteri içeriği zengin plak tabakası tükrük içeriğiyle birleşerek sertleşir ve diş taşı haline gelir. Diş taşları dişlerin yüzeyinde yer alabileceği gibi diş etinin üstünde ve altında da birikebilir.
                                Sigara, çay, kahve vb. gıdalar diş ve diş taşı yüzeylerinde renklenmeye neden olur. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Dişeti altında biriken diş taşları dişetini kolaylıkla iltihaplı hale getirir. Diş taşları ve dişetlerindeki iltihap zamanla çevre kemik dokusunda yıkıma sebep olur. Bu durum dikkate alınmazsa dişler çevresindeki kemik desteğini kaybederek zamanla mobil hale gelir. Önlem alınmadığı takdirde bu yıkım dişlerin ağızdan düşmesiyle sonlanır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Plak içinde bulunan bakteriler sadece diş ve dişetleri için tehlikeli değildir, bunlar akciğer, kalp gibi yaşamsal organlar için de öldürücü olabilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`İyi bir ağız hijyeni ,düzgün ağız bakımı ve sağlıklı dişetleri, yapılacak olan diğer restorasyonların başarısı için ilk adımdır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Dişeti iltihabının tedavisi diş taşlarının uzaklaştırılmasıyla mümkündür.

                                Kliniğimizde uygulanan dişeti tedavilerinde, ultrasonik temizleyici cihazlar ve özel el aletleriyle,gerek diş yüzeyindeki plağı ,gerekse dişeti altındaki, diş taşlarını temizliyor ve lekelenmeleri ortadan kaldırıyoruz. Ayrıca çok derin diştaşları ve tortuları da derin küretajla temizliyoruz. Son olarak kullandığımız özel macunlarla diş yüzeylerini fırçalayarak diş yüzeylerinin parlak ve pürüzsüz hale gelmesini sağlıyoruz. Bu şekilde pürüzsüz hale gelen diş yüzeylerine plağın tekrardan tutunması da zorlaşmış oluyor.

                                Bu prosedür kanamasız, sağlıklı dişetlerine sahip olmanızı amaçlamaktadır ve dişetlerinin durumuna göre bazen birkaç seans sürebilmektedir. Bu tedavi her 6 ayda bir tekrarlanmalıdır.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                      ],
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Deneme Başlığıdğr",
                        icerikContext:[
                          {
                            tag:"p",
                            icerik:"Deneme psidir.",
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Bu nu bilmiyorum psidir.",
                            classname:"content-p",
                          }
                        ]
                      },
                    },
                  ],
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Ağız ve Diş Sağlığı",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Diagnosis and treatment plan",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"All examinations and diagnoses are made in completely sterile environment using the computer technology",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"To check all the details we use a special intra-oral camera system",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"With our digital x-ray device, x-ray of your theeth can be taken in a short time with very comfortable conditions.",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"No fee is charged for non- emergency examinations",
                        classname:"content-p",
                      }
                    ]
                  },
                },
                {
                  id:136,
                  paths:'/uroloji',
                  birimAdi:"Medical Units ",
                  adi:"Urology",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Urology",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"The site's internal and surgical diseases in men and women is a branch of medicine that deals with urinary.",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Injections kidney stones",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kidney tumors",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Renal trauma",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kidney stone,grout and trauma",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Bladder disorders",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Cystitis,cystitis bloody",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Diseases of the prostate",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Urine and the cysts related complications that occur that occur in the ways of",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Circumcision",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Gonorrhea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Viral diseases such as HPV",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Of the process our specialist doctors by diagnosis, treatment and post-operative checks are performed in our clinic.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:137,
                  paths:'/kardiyoloji',
                  birimAdi:"Medical Units ",
                  adi:"Cardiology",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Cardiology",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:`Science that studies the diseases of the heart and circulatory system. The Science of Cardiology diagnosis and treatment (therapy) that have worked to provide today's most important health problems among the diseases there are some diseases that are located between. A few of these diseases can be listed as follows`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Hypertension",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Atherosclerotic heart disease (such as coronary artery disease)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Heart rhythm disturbances (arrhythmias)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Congenital heart disease",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:`Nephrology, Endocrinology disciplines also of interest, such as high blood pressure, various diseases, developing heart failure, congenital or valvular heart disease induced by various diseases, and similar diseases are among the diseases for the diagnosis and treatment of Cardiology, working.
                        The use of Cardiology diagnostic tools include:`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ecocardiography",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Electrocardiography (ECG) and related diagnostic methods:",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Cardiac stress test ('stress test' or ''stress test ECG also known as')",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"A portable ECG device ('Holter monitor')",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"The levels of cardiac enzymes in the blood",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Coronary angiography",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Of the process our specialist doctors by diagnosis, treatment and post-operative checks are performed in our clinic.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:138,
                  paths:'/gast',
                  birimAdi:"Medical Units ",
                  adi:"Gastroenterology",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Gastroenterology",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Esophagus,stomach,small intestines,large intestines,liver,gall bladder, pancreas of the subject area in question is the Science of organs; these organs ulcer,gastritis,jaundice,hepatitis,irritable bowel(irritable bowel syndrome: IBS),gall bladder stones and infections, gastro-intestinal cancers hemorrhoids(chilblains,piles) of known diseases, such as searches for a solution. For diagnostic purposes, different types of endoscopes are used. Esophagus, stomach and duodenum to display esophagogastroduodenoscopy (short endoscope in general use), colonoscopy for colon is used.",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"An example of gastroenterolojik diseases clinical symptoms and signs.",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Constipation",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Diarrhea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Vomiting",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Nausea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Abdominal pain",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Continuous murmur burning sensation (burning sensation behind the sternum)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Bitter water in the mouth come",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Abdominal pain",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"By our expert doctor for diagnosis, treatment and post-operative checks are performed in our clinic.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:139,
                  paths:'/genel',
                  birimAdi:"Medical Units ",
                  adi:"General Surgery",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"General Surgery",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Besides systemic and surgical treatment methods in the body of local issues, general principles (wound healing, injury, metabolic and endocrine contains topics like the answer, and that affected many branches of surgery and basic medical terms in a technical development discipline. Generally, the esophagus, stomach, small intestine, colon, liver, pancreas, gallbladder and bile ducts, including a branch of a surgical specialty that focuses on abdominal contents.",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Today 'General Surgery' at the mention of:",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Thyroid surgery",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Breast surgery",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Esophagus",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Stomach",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Small and large intestines,",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Diseases of the anus (hemorrhoids...),",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Liver",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Pancreas",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Gall bladder and bile ducts,",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Hernia Surgery ",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"it is understood.",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Expert on surgical procedures by our doctors diagnosis, treatment and post-operative checks are performed in our clinic.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:1991,
                  paths:'/deri',
                  birimAdi:"Medical Units ",
                  adi:"Skin Diseases",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Skin Diseases",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Common diseases of the skin. Disease too much skin. Dermatosis diseases of the skin generally, is the name given to the respective branches of Dermatology. Skin diseases can be split into several sections in order to get a general idea.",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Skin infections (fungal diseases, warts)",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Gland diseases (acne, dandruff on the scalp)",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Allergic skin diseases (atopic eczema, contact eczema [contact eczema],cosmetic allergies, insect bites)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Drug reactions",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Urticaria (hives)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Psoriasis (psoriasis)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Vitiligo",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Behçet's disease",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Skin tumors",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Burns",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Hair Disorders",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Nail diseases",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Excessive sweating",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Sexually Transmitted Diseases (Syphilis).",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:1992,
                  paths:'/dahiliye',
                  birimAdi:"Medical Units ",
                  adi:"Internal Medicine",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Internal Medicine",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Diagnosis and treatment of all internal diseases are performed in the internal medicine department. As sub-units of the Internal Medicine department;",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:1993,
                  paths:'/kbb',
                  birimAdi:"Medical Units ",
                  adi:"Otorhinolaryngology",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Otorhinolaryngology",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"In our otorhinolaryngology (ear, nose and throat) department endoscopic diagnostic methods used , audiometric and tymparometric tests  are performed  and diagnosis and treatment of the diseases that are listed below are carried out by our experienced doctor.",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Rips Tympanic Membrane",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"The ear canal and auricula diseaseas",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Draining middle ear diseases ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Loss of hearing",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Dizziness and balance disorders",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Sinusitis",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Allergic Rhinitis",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Difficulty in nasal breathing and nasal figure disorders",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Tonsil problems",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Examination of hoarseness and vocal records",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Adenoid Disorders ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Snoring and sleep  Apnea ( Cessation of breathing )",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"",
                        classname:"content-p",
                      },
                    ]
                  },
                },
              ],
              componenti:0
            },
            //130bitis
            //140
            {
              id:140,
              paths:'/unitelerimiz',
              adi:"Units",
              sidemenuVisible:true,
              exact:false,
              subMenu:[
                {
                  id:141,
                  paths:'/bla',
                  birimAdi:"Units ",
                  adi:"Radiology Services",
                  sidemenuVisible:true,
                  exact:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Radiology Services",
                    icerikContext:[
                        {
                            tag:"li",
                            icerik:"General radiology",
                            classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Computerized tomography",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Mammography",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Panoramic x-ray",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Digital fluoroscopy",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Magnetic resonance imaging",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Ultrasonography",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Color Doppler ultrasonography",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Angiography",
                          classname:"content-p",
                        },
                    ]
                  },
                },
                {
                  id:14846846,
                  paths:'/diloymgfkdjhs',
                  birimAdi:"Units ",
                  adi:"Laboratory Services",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Laboratory Services",
                    icerikContext:[
                      {
                        tag:"li",
                        icerik:"Clinical Biology",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:" Tumor markers(PSA,TSH, chorionic gonadotropin (HCG),AFP,CEA,CA19-9,mean ca125,CA15-3,CA72-4, etc.)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Hormone tests(TSH,LH,FSH,AC, TS,T3,T4,ADH,FT3,FT4,HOMO-IR, etc.)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Tests related to lipid metabolism (LDL cholesterol,HDL cholesterol,total cholesterol, etc.)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Microbiology",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Hematology",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Molecular Microbiology",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Allergy Tests",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:52661,
                  paths:'/derma',
                  birimAdi:"Units ",
                  adi:"Derma Cosmetics",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Derma Cosmetics",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:`Skin, hair and body care, used for using pharmaceutical techniques are developed, manufactured and dermatological skin products that have undergone clinical testing. Dermocosmetics sold in pharmacies and only by experts in dermatology or plastic surgery for the solution of various skin problems, either directly or as a support for drug treatment are prescribed. Most of the time, or may resolve to reduce the side effects of medication aimed at supporting the use. Example : acne treatment , psoriasis treatment. Dermocosmetic products are only sold in pharmacies however , pharmacies doesn't mean that every skin care product that is sold in mechanical damage. Although there are some skincare products sold in pharmacies "drugstore cosmetics" as defined and should be classified.`,
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:8372,
                  paths:'/dermalaz',
                  birimAdi:"Units ",
                  adi:"Dermatological Laser Applications",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Dermatological Laser Applications",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:`the capillary and varicose veins of the leg: excess weight, some vascular diseases, resulting in the leg veins due to prolonged standing swelling for years as superficial or structural-what are. Treatment are performed to the accompaniment of specialist physicians in our clinic.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:`the hair removal : is based on the destruction by burning of unwanted hair laser. Treated with the laser every skin type and every season.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:`the acne :, that arise at any age especially during adolescence more of the face acne-what. Can be of different size and intensity. Dermatology some acne patients can be treated with laser, which is determined by the doctor.`,
                        classname:"content-p",
                      }
                    ]
                  },
                },
                {
                  id:969,
                  paths:'/dishiz',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Diş Hizmetleri",
                  sidemenuVisible:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Diloy Diloy",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Diloy Diloy",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Bu nu bilmiyorum psidir.",
                        classname:"content-p",
                      }
                    ]
                  },
                },
                {
                  id:9529,
                  paths:'/lazer',
                  birimAdi:"Units ",
                  adi:"Laser Eplition and Regionel Slimming",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Laser Eplition and Regionel Slimming",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Laser Epilation",
                        classname:"content-p-big",
                      },
                      {
                        tag:"p",
                        icerik:`Laser epilation mus be done under the consultancy and control of a dermatologist in order not to have any side effects and negative impacts.It is very important to have hair removal process in health institutions authorized by the health ministry under the contol of a health care specialist or a doctor with laser epilation devices in compliance with international norms (U.S FDA approval, European CE certificate ). Keeping a head of the scientific data , your dermatologist will offer the most suitable laser epilation for you.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"How can laser epilation be effective?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`Laser epilation focuses on the thickness and color of the hair. Laser is absorbed by hair follicles because of its color. Due to high energy, the roots heats and lights. In this way, roots are poured after a successful laser epilation and in the growth phrase gets damaged and become unable to produce hair.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Is laser epilation successful?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`To achieve a succuessful result, the appropriate laser type and dose suitable for the skin color and hair structure must be selected in laser removable applications.Sessions repeated at regular intervals, makes the outcome quicker and more successful. Laser epilation is a treatment approved by IDA and considered as a complete success if the region the treatment applied has 80% hair removal.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Is laser epilation safe?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`Laser epilation is a technology safely used in medicine for about 30-40 years. It has been widely used last 10-15 years.Also, it has no effect on corcinogenicity. Dermatologist’s assessment is a necessity.Laser must not be applied on the risky moles and dark spots. In our medical center; alexandrite and ND-YAG laser epilation devices that are approved by FDA (U.S food and drug approval agency ) and CE are used. `,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"How many sessions are necessary for effective treatment?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`The hairs in our body has growth(anagen), pause (catagen) and shedding(telogen) phrases. Approximately 30-70% of them are in shedding(telojen) phrase. The most light-sensitive phrase of the hair is anagen stage. As there is no melanin production, the hairs are not effected from the light in other stages. For this reason, the laser hair removal application must be repeated at regular intervals in order to catch the hairs in anagen stage. The number of sessions in laser epilation application depends on the region of the body, skin color, hair color, thinness-thickness of the hair and hormonal factors. Average 4-10 sessions is essential for a permanent solution in laser epilation.
                        In general, in men, the hairs in all regions of the body and in women the hairs in face, the laser epilation sessions last long-term and the hairs become thinner and remain on the body.The success of laser epilation increases when skin color is lighter and the hair color is darker. Genitals, armpits and legs are areas best results received in laser epilation. Depending on all these factors, the success rate is 50-90%. Hormanal disorders may effect the number of sessions. Your doctor will inform you after making necessary examinations and evaluations. `,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"How session intervals determined in laser epilation?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`In laser epilation sessions, the spaces given vary from 4-8 weeks depending on the region. Laser epilation session intervals vary according to the the shedding and coming out of hairs.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Does laser epilation have any side-effects? ",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`When applying laser epilation, the pain perception varies from person to person. This pain is usually bearable and defined as rubber shock and as the feeling of touching the needle.

                        After the laser epilation session, redness and swelling which last a few minutes may occur but these side effects are temproray and do not affect patients’ daily activities. In laser epilation application, crusting on the skin surface is sometimes a temporary problem that can occur on the applied skin. The crust, usually resolves within days and the skin returns to normal. In rare cases, discoloration and darkening can be observed in the skin. In almost all cases, skin returns to normal within 6-12 months. Skin thickining called as scar tissue is a rare side effect that can be permanent. If the application of laser epilation is done under the supervision of expert dermatologists and if the right type and dose are selected; no side effects will be seen.
                        `,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Can laser epilation be applied in summer?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`Treatment can be applied in summer. However, the patients should carefully apply the sun protection methods recommended by the physician. In addition, no application must be made to bronze skin and no sun bath must be taken after the application. When application is implemented to bronze skin, both several side effects appear and the success rate decreases. Tanned individuals should wait at least 1 month to start laser epilation. `,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"What should be considered prior to laser epilation?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:`One month before the application of laser hair removal ; operations like epilator, waxing, tweezers should not be applied. The hair would be sufficient for prolongation of 2-3 mm when shaved three days before the application. Prior to treatment, yellowing and staining should not be made.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`Test shot can be applied to meet laser and determine dose before the application. After the test, most suitable wavelength is detected and the right therapy is applied.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`For the completion of the ripening stages of hair formation and for the body parts average 6-12 sessions of laser hair application may be needed.The implementation of sessions at more frequent and large intervals has a negative effect on the healing process as it breakes the cycle of hair..`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`Skin peeling and treatments such as acne, blemishes drugs should be left up to 15 days before laser epilation session.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"What should be considered after laser epilation?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:`No sunbathe and solarium for two days after the laser epilation application. Sunscreen with SPF 3D factor should be used for at least 2 weeks with doctor's prescription. Especially, sunscreen on face should be applied.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`Most of the hairs on the skin are removed by laser epilation. The roots start to elongate and begin to fall between 3-10 days.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`For an hour or a few days redness may occur on the laser applied skin due to the person's skin sensivity.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`A warm shower can be taken immediately after laser epilation. Applications irritating the skin such as peeling should not be applied for one week.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`The hair density is reduced to 15-20% in each session, you can see the difference after the 3rd session. To continue your sessions regularly will let you get maximum efficiency from your laser epilation `,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`2 weeks after each session, a control session is free of charge for large regions (arm-leg-back-chest-genital). (Controls made after 20 days will negatively affect the course of therapy).`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`No control session is needed for small regions as it does not contribute to the course of treatment.`,
                        classname:"content-p",
                      },
                    ]
                  },
                  subMenu:[
                    {
                        id:142,
                        paths:'/lazerkilcal',
                        birimAdi:"Units ",
                        adi:"Capillary Treatment with Laser",
                        exact:false,
                        sidemenuVisible:true,
                        componenti:0,
                        icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Capillary Treatment with Laser",
                            icerikContext:[
                                {
                                    tag:"p",
                                    icerik:"Capillary Treatment with Laser",
                                    classname:"content-p-big",
                                },
                                {
                                    tag:"p",
                                    icerik:`Genetic predisposition, prolonged standing, pregnancy periods, wearing high-heeled shoes and excessive weight gain cause extension in capillaries in the legs. The sun exposure, genetic predisposition, thin and sensitive skin type cause red-purple capillary in the face. Large number of capillaries may cause pain especially in the legs. Many people have a lot of cosmetic problem because of capilarries.
                                    Red-purple thin veins that appear in the face or in anywhere on the body can be treated and destroyed with 1-6 sessions.The best results are taken by Nd-YAG laser system in all around the world. `,
                                    classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"1. How many sessions should be done for he treatment of capillary vessels with laser?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"According to the width and depth, prevalence of cappillaries, can either be destroyed with one session or six sessions may be required.",
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"2. What should be the session intervals? ",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"Sessions are made 4-8 weeks intervals.",
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"3. Which veins are suitable for laser treatment?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"Blue-purple, maximum 4 mm wide veins in the face and legs give the best results in laser treatment.",
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"4. What to do before and after the treatment?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"Sun protection is essential. Should be done during the winter period. Temporary redness, mild swelling or color changes may occur after the treatmant.",
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"5. Can treated veins may occur again?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"Generally, treated veins will disappear completely irreversible. However if there are situations such as an underlying venous insufficiency, the formation of varicose veins ; reformation of capillaries may be observed in other areas.",
                                  classname:"content-p",
                                },
                            ]
                        },
                    },
                    {
                        id:1461515,
                        paths:'/botoks',
                        birimAdi:"Units ",
                        adi:"Botox Treatment",
                        sidemenuVisible:true,
                        exact:false,
                        componenti:0,
                        icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Botox Treatment",
                            icerikContext:[
                              {
                                  tag:"p",
                                  icerik:"Botox Treatment",
                                  classname:"content-p-big",
                              },
                              {
                                  tag:"p",
                                  icerik:`Botox is a highly successful method used for the treatment of facial lines and wrinkles, the prevention of new wrinkles that may occur (antiaging). `,
                                  classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Which wrinkles treatment Botox is used?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"In Botox treatment, wrinkles in the forehead, around the eyes and between the eye brows give the best results. Botox is also used for to treat laugh lines and neck wrinkles. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"How do wrinkles form?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Some of the wrinkles occur due to the facial movements and contraction of muscles under the skin. Relaxation of these muscles reduce wrinkles and cause extinction. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"What is the affect of Botox?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Botox is a medicine containing botulium toxin A. Botilinum is a neuromuscular blocking agent, when injected into muscle that causes muscle paralysis, blocking the release of acetylcholine to the communication gap between nevre and muscle. When the muscle not warned by the nevre is paralyzed. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Is Botox used to treat other diseases in dermatology?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Yes. Botox is a a successful method used for the treatment of localized (underarms, palms and soles) excessive sweating. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"How Botox treatment is made?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"The process is simple and can be applied in inspection. It does not require anesthesia.Botox is a painless application and does not leave any scars upon injection. After reconstitution, a very small amount of Botox is injected with fine needles just under the skin to the muscle. After the application, no change will be at the applied body region and the person can immediately return to normal daily activities. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"When Botox starts and how long it takes effect?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Botox' s effect is seen in 2-7 days. This effect will continue 3-6 months then new injections are required. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"What should be considered after the application?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"The applied body region intact for 2-3 hours and adhered in an upright position.",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"The applied body region intact for 2-3 hours and adhered in an upright position.",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Bath can be made, make up should not be made.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"What are the side effects of Botox?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"As the toxin injected to a very small amount, treatment is safe with minimum side effects. Slight burning occurs in half of the patients during the injection. There may be 1% decrease in eyelid and eyebrow. Declines in 4 weeks.",
                                classname:"content-p",
                              },
                          ]
                        },
                    },
                    {
                      id:684936858,
                      paths:'/dolgu',
                      birimAdi:"Units ",
                      adi:"Filling Applications",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Filling Applications",
                        icerikContext:[
                          {
                              tag:"p",
                              icerik:"Filling Applications",
                              classname:"content-p-big",
                          },
                          {
                              tag:"p",
                              icerik:`What causes wrinkles and lines?`,
                              classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Hyaluronic acid content of the skin decreases with aging and decreases the skin’s water-holding capacity. Collagen and elastic fibers in the skin begins to break and tear. These breaks are a part of natural aging process, but more frowning, smiling and other facial expressions contribute to the formation of lines on the face. Smoking cigarette and environmental factors such as polluted air, also accelarates the aging process.The most frequently used process to fill facial wrinkles is facial filler treatment `,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"What is hyalunoric acid (Restylane, teosyal and juvederm etc.)?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Hyaluronic acid is a clear crystal looking gel substance that is produced under laboratory conditions and does not contain any animal material. (Filler materail between the cells in our skin). When injected into the skin combines with the body’s own hyaluronic acid and creates volume. Has a towing capacity up to 1000 times of its volume. Plumpled lips, filled lines and wrinkles and treated facial folds are provided with this volume. It is a quick and easy implementation and face filling process allows to obtain visible results immediately. `,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"What should the expectations be after the filler treatment?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Hyaluronic acid provides a natural look after injection. You can have your facial filling and lip filling procedures done at lunch break and continue to your daily life. You do not need to have any previous test. However, it is important to have it applied by experienced specialists. You can immediately return back to your daily activities after filler injections. Effects will continue about 6-12 months according to the material used.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Does filler treatment have any side effects?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Hyaluronic acid has been confidently applied to more than 2 million people and in 70 countries around the world since 1996. Rarely, a slight swelling in the treated area might be seen for a few hours. Allergic reactions can rarely be seen. Redness, itching or stiffness may occur at the injection site. A face filling treatment with Hyaluronic acid is usually applied as an alternative in aesthetic surgery. Hyaluronic acid is a safe application that does not require any testing before treatment. `,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Is the filler treatment painful?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Anesthesia , lasting 45 minutes with a local anesthetic cream is good enough. Facial filler is a treatment that can easily be tolerated. As lip is a region rich in nerve density,in filling process,anesthesia applied by dentists can be preffered. `,
                            classname:"content-p",
                          },
                        ]
                      },
                    },
                    {
                      id:5286727,
                      paths:'/bölgesel',
                      birimAdi:"ÜNİTELERİMİZ ",
                      adi:"Regional Slimming",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                          icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                          icerikHeader:"Regional Slimming",
                          icerikContext:[
                              {
                                  tag:"p",
                                  icerik:"Deneme psidir.",
                                  classname:"content-p",
                              },
                              {
                                  tag:"p",
                                  icerik:"Bu nu bilmiyorum psidir.",
                                  classname:"content-p",
                              }
                          ]
                      },
                      subMenu:[
                        {
                          id:143,
                          paths:'/kavitasyon',
                          birimAdi:"Units ",
                          adi:"Ultracavitation with Max Power",
                          sidemenuVisible:true,
                          exact:false,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Ultracavitation with Max Power",
                            icerikContext:[
                                {
                                    tag:"p",
                                    icerik:"Ultracavitation with Max Power",
                                    classname:"content-p-small",
                                },
                                {
                                    tag:"p",
                                    icerik:`Ultracavitation method gave visible results in reducing cellulite and regional thinning and greeted with interest.
                                    Especially, this application is very suitable for people who have long working hours and time problem. It works by dissolving the fatty tissue that is accumulated in the body by the help of strengthened ultrasound and forming the body.Recovary time and comfort of the transaction increases as it is not a surgical operation. `,
                                    classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"What is ultracavitation system? How does it work?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`Ultracavitation system is a non-surgical method fighting with regional adiposity and cellulite with the help of ulrtasound.Spread of new ultrasound applied to the outer surface of the skin causes a sudden, high pressure changes in the cell fluid of adipose tissue.Then, the expansion of the foaming creates the explosion.This effect called cavitation ; destroys the walls of the fat cells by fat liquefaction and destroys the structure of the body fat depot.In this tissue, fat cells and fatty acids that are released break down and tried to schism from these places by lymphatic pathways. The fats released excreted from the body either by burning in the muscles or through urinary system or liver. We are using this technology with lipotomi hipoosmalar method and getting cost effective results. The true recommended action mechanism that is scientifically proven is obtained by this method.`,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"What is the purpose of its use?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`It is a powerful system used in reducing cellulite, melting and getting rid of the primarily located regional fat. `,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"Does the application give any pain?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`Ultracavitation method is a safe and painless ultrasound application. During and after the application a light and sensible heat can be felt for several hours. `,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"Can be used with two methods.",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"li",
                                  icerik:`1. Wet method: Using liquid injection, the fat cells are inflated. Thus, liquefaction and blasting are carried out more effectively and this is called hipoosmolor lipotomi.`,
                                  classname:"content-p",
                                },
                                {
                                  tag:"li",
                                  icerik:`2. Dry method: It is applied directly to that area without giving liquid, results are obtained after 6-8 sessions. It is applied 1-2 days a week.This method is recommanded for those who fear the needle and can be supported by other systems.`,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"Can these also be applied to men?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`There is no discrimination in this system. It is applicable to both men and women.`,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"What is the time period to see the results and what should be done meantime?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`Although a special diet and sports supplementation is not required, to eat more after the application will add new fats to the ones lost in ultracavitation. After the first session, a palpable softening and then downsizing in the faty region can be felt.Visible results can be obtained after 15 days.`,
                                  classname:"content-p",
                                },
                            ]
                          },
                        },
                        {
                          id:190,
                          paths:'/radyofrekans',
                          birimAdi:"Units ",
                          adi:"Radiofrequency-Slimming with Velashape",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Radiofrequency-Slimming with Velashape",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Radiofrequency-Slimming with Velashape",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Majority of women over the age of twenties has cellulite problems regardless of their body shape and weight.Nowadays, women prefer more effective and long-term solutions than limited traditional and expensive medical solutions.The common dream of all women in the world is to have a nice and smooth body… With the development of technology, various devices have been developed in order to eliminate accumulated fat in certain areas and to get rid of cellulite without any surgical intervantion. VelaShape is the only well -developed device that reduce and even manage to destroy cellulite that has proven its success in the aesthetic world in the clinical settings. Velshape received “ the first medical device to treat cellulitis effectively “ approval from U.S Food and Drug Administration FDA. VelaShape can also be easly applied to patients with capillary vascular disease.New Radio Frequency slimming treatment is like a warm massage that most patients enjoy. During or after the application, no special socks or clothes are needed.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Which regions can velaShape be applied ?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Legs and butt",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"The upper arm",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Waist and side region",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Abdominal region",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Face and neck",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"How often VelaShape is applied ?, After how many sessions can a result be obtained ? ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`For 2 times a week, 8-16 sessions are recommanded according to the degree of cellulite. 10 sessions is sufficient for patients that have cellulite with a degree one or two. In fact, even after a few sessions, there will be a noticeable difference. Sessions can continue once a month in order to ensure the same results after cellulite treatment. Each session lasts about 45-60 minutes. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"What is felt during the procedure?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`After VelaShape cellulite and regional thinning treatment, due to the heating of the skin, redness occurs on the skin surface. You can start your daily activities after the application and return back to your work. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Is cellulite and regional slimming treatment safe with VelaShape? ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Regardless of color or skin type, it can safely be applied to all individuals over the age of 18.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"How can I start VelaShape treatment?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`The first step is to call our clinic to get an appointment for free consultation. After you decide to start treatment, inspection is made by our experienced doctor. Once you decide the VelaShape method is safe for you, according to the intensity of regional adiposity and the degree of cellulite or grievances, the number of sessions determined for you to get the most effective results. If necessary, it can be combined with other treatments such as mesotherapy.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:959919,
                          paths:'/powerplate',
                          birimAdi:"Units ",
                          adi:"Powerplate",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Powerplate",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"What is power plate?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Due to gravity, we all have weight defined in kilograms.By acting against the gravity, the muscles in the humanbody develops and strengthens. In this world widely used technique, the vibration produced by the power plate is transmitted to the humanbody as an energy. During the practice, the muscles continuously stretch, relax and run. During the application period, all the muscles in the body run in a dicipline. With 40 Hz. release rate choicen from the power plate , our muscles relax and strech 40 times per second. After work is completed, the body starts to relax and the body metabolism adjusts itself .The body’s performance line was taken up with this adjustment. This initiative; increasing the body’s performance is called “supercompensatiom”. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"10 minute Power Plate study is equvailent to approximately 1 hour Fitness study…",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Adds flexibility,",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Allows to run all the muscles in the body,",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Minimize the risk of joint injuries,",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Increases the oxygenation and blood flow,",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Allows the hormones to run at a higher level,",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Increases body water drainage,",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:`Reduces the effect of osteoporosis,`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Reduces back and low back pain and strengthens these areas,",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:`Prevents formation of cellulite, reduce cellulite`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Increases bone density,",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:`Increases collogen production,`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Reduces mental and physical stress,",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Stay young with just 10 minutes of Power Plate study...`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Fields where Power Plate is applied: `,
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Physical Therapy: Vibration application strengthens the muscles, muscle tissue, bone structure and the bonds of the joints and reduces the risk of joint injury.`,
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Sports: In general, when applied with the muscle strengthening fitness programs, the muscles strengthens in a shorter time.Similar effects can be seen when used alone. By only 10 minutes application a day, all the muscles in a body strengthens.`,
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Fitness: As it can easly be adapted to traditional studies like fitness( stretching/relaxation), can also be used as healing and cooling programs in these studies.`,
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Rehabilitation: Eliminates muscle weakness(antrophy) and slack muscles (hypotonia), reduces pain, fixes perception problems(proprioceptive disturbances). All parts of the body relaxes. `,
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Medical: Improves blood circulation, contributes to correction of disorders of bone(osteoporosis) resorption.As the heart rate remains constant during the operation, it does not impose additional burden to the body.`,
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Cosmetics: Prevents the accumulation of fluid in the body, helps to drain accumulated fluids.Accelerates the degradation of fat free, develops muscle structure and resolves regional cellulite, helps the body to have a tight tissue and prevents the formation of cellulite. Increases the firmness of tissues by adjusting the balance of tonic in the body.`,
                                classname:"content-p-small",
                              },
                            ]
                          },
                        },
                        {
                          id:378235893,
                          paths:'/enjeksiyon',
                          birimAdi:"Units ",
                          adi:"Injection Lipolysis",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Injection Lipolysis",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Injection Lipolysis",
                                classname:"content-p-big",
                              },
                              {
                                tag:"p",
                                icerik:"What is Lipolysis?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Fat problems of the certain regions in the body that can not be solved either with diet or fitness can now be solved with lypolysis injection method that is used in Europe and America. Is the direct injection of some substances to the subscutaneous fat tissue that accelerate fat burning and melting. Many people have accumulation of excess fat in certain areas of the body although they are not overweight. Leg, hip, abdomen, sides of the waist in women, abdomen and waist in men are the places of the body that are most prone to fat accumulation. Its effects increase approximetaly threefold when used in conjunction with cavitation and radiofrequency in regional thinning programs.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Which materials are used?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`One of the most popular slimming method which is called lypolsis; an active substance named phosphatolyl choline is used. The substance called lecithin is vital for people. It is found in all cells and plays a role in vital activities. It has been considered to be used in the treatment of excess fat settled in the unwanted regions of the body. With fat burning accelerating agent, L-carnitine can be used.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"How is treatment made?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`This method is used eliminating all kinds of glands.This theraphy which gives more successful results in women then in men; is applied by giving drug injection to the excess areas of the body with thin needles`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"How many sessions are required?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Depending on density of suboutaneous fat mass ,average 3-6 sessions are applied.The patient can be treated in 3 sessions as the fats can be solved easily. The frequency and the number of sessions can vary according to the amount of the drug given.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Is the application painful?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`During the application pain is not unbearable. There might be edema and tenderness for afew days. Approximately, in 1-2 weeks healing bruises may occur. People can return back to their daily activities after the application`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"What are the side effects?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`When performed by experienced and trained specialists, any adverse side effects were seen. `,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                      ],
                    },
                    {
                      id:377392,
                      paths:'/medikal',
                      birimAdi:"Units ",
                      adi:"Medical skin care",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                          icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                          icerikHeader:"Medical skin care",
                          icerikContext:[
                              {
                                  tag:"p",
                                  icerik:"Medical skin care",
                                  classname:"content-p-big",
                              },
                              {
                                  tag:"p",
                                  icerik:"Special acne care for oily skin",
                                  classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Excessive secretion of sebum on the skin leads to an imbalance and shine on the skin.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Lifting care",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"It is an innovative method for your skin to look younger, to reduce fat cells and to shape facial contours.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Needleless botox treatment",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"A non-surgical effective care used in preventing and reducing wrinkles.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Caviar Care",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Care fighting against premature aging.
                                Provides intensive renovation both for lifeless and mature skins.
                                It is possible to get rid of your stains by highly effective treatments.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Chemical Peeling",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Skin renewal slows down with the age.
                                Chemical peeling treatment helps the young and healthy skin to come to the surface and removes the problematic cells from the skin.
                                Chemical peeling treatment is used to solve the following problems;`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Acne(pimples)treatment",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Treatment of scars left after acne`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Elimination of thin lines and wrinkles`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Dry and lifeless skin treatment`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Closing of open pores in the skin`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Removing unwanted color changes (aging, sun, pregnancy and drug strains)`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`To delay the effects of aging and rejuvenate the skin, 6 sessions of peeling treatment is applied to each healthy skin.`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:``,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:``,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:``,
                                classname:"content-p",
                              },
                          ]
                      },
                    },
                    {
                      id:8948363,
                      paths:'/prp',
                      birimAdi:"Units ",
                      adi:"Prp",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Prp",
                        icerikContext:[
                          {
                              tag:"p",
                              icerik:"PRP (Platelet Rich Plasma) Treatment",
                              classname:"content-p-big",
                          },
                          {
                              tag:"p",
                              icerik:"What is PRP?",
                              classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`"PRP" is the platelet rich plasma treatment of the cells called platelets in the blood.A small amount of blood is taken from a person and seperated into its components with a special process . As a result of this, a small amount of platelet -rich plasma is obtained and given back to the same person through the process of injection for skin rejuvenation.PRP is also used in the heart surgery in order to reduce the risk of the infection, in tennis elbow in order to accelerate the healing process and for disorders such as tendon injury.At the same time, it is performed by dentists in implant treatment.
                            PRP is injected in the body to help to renew the body tissues by inducing and activating the stem cells.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"What is the purpose in PRP application?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`When any injury occurs in our tissues, repairing process begins through platelets.In PRP application, a large amount of circulating blood platelets are carried out to the target area with a content of growth factors.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"How is skin rejenuvation made by PRP?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`The aging of our skin is caused by the loss of some physical properties. Dermo cosmetic products starts the healing process through materials restructuring our skin and synthetically derived growth factors. When concentrated platelets within the plasma, injected in skin, growth factors stimulate collagen production and the formation of the new capillaries and provide skin renewal.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"How to apply skin resurfacing procedure with PRP? ",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`In order to obtain platelet platelet rich plasma a special fitter and centrifuge used.Approximately 97% of platelets break down and become 6-10 times bigger than the normal density in the plasma. PRP treatment starts with making a blood test to the patient.With the help of a special fitler and centrifuge, the platelet-rich plasma containing autologous white blood cells are prepared.Finally, autologous platelet-rich plasma (PRP) with white blood cells is injected into the skin. It provides realease of growth factors with platelets and white blood cells in the injection area.Growth factors provide removal of skin problems and accelerates skin repair.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"How long the PRP application last?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`It lasts approximetly 30 minutes.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Can PRP and skin resurfacing be applied other than injection?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Yes , can be prepared and applied as mask and cream.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`In what circumstances, skin regeneration with PRP is effective? `,
                            classname:"content-p-small",
                          },
                          {
                            tag:"li",
                            icerik:`Estetik amaçlı uygulamalarda yüz, boyun, dekolte, eller, bacak içleri, kollar gibi vücut bölgelerine gençleştirme için`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`For aesthetic applications, for the rejuvenation of the body parts such as face, neck, decollete, hands, iner legs and arms.`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`To ensure the rapid construction of the skin, immediately after the applications such as laser and peeling.`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`Correction of wrinkles in the skin resulting from exposure to ultraviolet rays for many years and to help eliminate depressions.`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`To gain a sustainable flexibility and brightness.`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`In the treatment of hair loss, can be used alone or in combination with other treatments.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`Is PRP a reliable skin rejuvenation method?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`The blood taken from the patient is used with a sterile and closed kit, therefore PRP is a reliable application.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`When does PRP's effect occur? `,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`A healty glow occurs on the skin immediately after the application. A permanent and significant effect can be seen after 3-4 applications.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`Has skin rejuvenation with PRP got a lasting impact?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`After 3-4 applications, if repeated every 10-12 months- the impact of cures will be equivalent to a permanent rejuvenating effect.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`What are the advantages of PRP method?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`The positive results of other methods will continue for some time, but the positive results of PRP, belongs to the person applied and not lost.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`Is skin regeneration procedure with PRP painful?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`No serious pain is felt except a slight discomfort. Anesthetic creams can be applied before the application.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`To whom PRP not be applied?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`To cancer patients and patients that have insufficient number of platalet.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`Does PRP mean skin rejuvenation with stem cells? `,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`PRP is injected to the stem cells to stimulate and enable the cells to be active.There is no stem cell in the serum content but there are many platelets and white blood cells. PRP has effect on the re-construction of regenerative stem sells.`,
                            classname:"content-p",
                          },
                        ]
                      },
                    },
                    {
                      id:25251513312,
                      paths:'/mezoterapi',
                      birimAdi:"Units ",
                      adi:"Mesotherapy",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                          icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                          icerikHeader:"Mesotherapy",
                          icerikContext:[
                            {
                                tag:"p",
                                icerik:"Mesotherapy",
                                classname:"content-p-big",
                            },
                            {
                                tag:"p",
                                icerik:"What is mesotherapy?",
                                classname:"content-p-small",
                            },
                            {
                              tag:"p",
                              icerik:`Mesotherapy is a treatment method which originated from Europe and applied in the form of injections of locally prepared specific drug combinations to the skin for the treatment of medical and cosmetic skin problems.Mesotherapy is first described by French physician Michel Pistor in 1952. An article about treatment technique was published by a local magazine in 1958 .Mesotherapy exhibition was organized in 1964. The French National Academy officially adopted mesotherapy as a branch of medicine in 1987. For nearly 50 years, more than 15 000 applications all around the world were made by mesotheraphy doctors. In mesotherapy, the aim is to make injections with very small drug doses to the problematic areas of the body.Injection is made to the middle layer of the skin which is called mesoderm. `,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`In which situations is Mesotherapy applied?`,
                              classname:"content-p-small",
                            },
                            {
                              tag:"li",
                              icerik:`Reoginal thinning`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Cellulite treatment`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Face, neck and decollete resurfacing`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Hair loss`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Acne scar treatment`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Pregnancy stretch marks`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:"Which medications are used in mesotherapy?",
                              classname:"content-p-small",
                            },
                            {
                              tag:"p",
                              icerik:`In classical mesotherapy, special mixtures are prepared by using herbal remedies, vitamins and minerals in accordance nwith the problem of the person.`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`In which situations mesotheraphy is not applied?`,
                              classname:"content-p-small",
                            },
                            {
                              tag:"li",
                              icerik:`Severe heart disease`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Kidney diseases`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Diabetes`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Blood thinning drug abusers`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Pregnant and breast-feeding women`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Those allergic to the drugs used`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Cancer patients`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`How is mesotherapy applied in regional slimming and cellulite treatment? In certain parts of the body, regional weight may occur because of genetic and structural reasons, some hormonal diseases, weight gained after pregnancy, life style away from physical activities . Weight is formed mostly in abdominal circumference and basin disrict.
                              In mesotherapy, body slimming is provided by injecting a special blend of medications prepared for problematic regions.In regional slimming oil degrading drugs and in cellulite treatment ,medications that increase blood flow are applied.Mesotherapy is applied , 1-2 sessions for regional slimming.8-12 sessions is in total.It is not applied to people with extreme overweight. `,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`In which situations mesotherapy is applied for renewal of the face(mezolift)? Mesotherapy applied to face is called " mezolift". Mezolift applied in the following cases:`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Resurfacing- rejuvenation`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Wrinkles treatment`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Acne scar treatment`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Loss of flexibality in face`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Neck, decolletage and hand care`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`Which medicals are used for mezolift?`,
                              classname:"content-p-small",
                            },
                            {
                              tag:"p",
                              icerik:`Skin condition is determined according to the age of the patient. Special medications that support the texture of the skin and vitamin mixtures are applied to neck, face and decollete and hands.`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`How mezolift sessions are arranged?`,
                              classname:"content-p-small",
                            },
                            {
                              tag:"p",
                              icerik:`The sessions are carried out every 7-10 days depending on the skin. Sessions are 15-20 minutes. After the application, the patients can return back to their daily activities. `,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`How is mesotherapy applied in hair loss?`,
                              classname:"content-p-small",
                            },
                            {
                              tag:"p",
                              icerik:`Mesotherapy applied to both men and women that have hair loss problems.
                              7-10 days, a total of 6-12 sessions mesotheraphy is applied for hair loss. Specially prepared mixture of drugs for hair injected to the scalp. At first, shedding is reduced and and hair out can be seen in the following sessions.`,
                              classname:"content-p",
                            },
                          ]
                      },
                    },
                  ],
                },
              ],
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Ünitelerimiz",
                icerikContext:[
                  {
                    tag:"li",
                    icerik:"Ünitelerimiz baby",
                    classname:"content-li",
                  }
                ]
              },
            },
            //140bitis
            {
              id:170,
              paths:'/doktorlarimiz',
              adi:"Doctors",
              sidemenuVisible:true,
              exact:false,
              componenti:2,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Doctors",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Deneme psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  }
                ],
                tab:{
                  tablist:[
                    {
                      id:0,
                      name:"Erdal ARAS",
                      title:"Uzm. Dr.",
                      bolum:"Radyoloji Hizmetleri",
                      tarih:"07.10.1945",
                      birthlocation:"Çorum",
                      lang:[
                        {id:0,adi:"İngilizce"},
                      ],
                      education:[
                        {id:0,adi:"HACETTEPE ÜNİVERSİTESİ TIP FAKÜLTESİ",yil:"1900"},
                      ],
                      icerik:"İCerik",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/erdal-aras.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:1,
                      name:"Levent İLHAN",
                      title:"Dr.",
                      bolum:"Pratisyen Hekim",
                      tarih:"11.09.1964",
                      birthlocation:"Erzincan",
                      lang:[
                        {id:0,adi:"İngilizce"},
                      ],
                      education:[
                        {id:0,adi:"ERCİYES ÜNİVERSİTESİ TIP FAKÜLTESİ",yil:"1900"},
                      ],
                      icerik:"İCerik2",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/yok.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:2,
                      name:"Ercan Akpınar",
                      title:"Dr.",
                      bolum:"Pratisyen Hekim",
                      tarih:"16.05.1970",
                      birthlocation:"Erzurum",
                      education:[
                        {id:0,adi:"Hacettepe Üniversitesi Tıp Fakültesi",yil:"1900"},
                      ],
                      lang:[
                        {id:0,adi:"ingilizce"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/ercan-akpınar.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:3,
                      name:"Ömer Orçun Koçak",
                      title:"Op. Dr.",
                      bolum:"Kadın Hastalıkları ve Doğum",
                      tarih:"31.08.1977",
                      birthlocation:"İstanbul",
                      education:[
                        {id:0,adi:"İstanbul Üniversitesi, Cerrahpaşa Tıp Fakültesi Kadın Hastalıkları ve Doğum Anabilim Dalı (2001–2007).",yil:"1900"},
                        {id:1,adi:"İstanbul Üniversitesi, İstanbul Tıp Fakültesi (1994–2001).",yil:"1900"},
                        {id:2,adi:"İstanbul Eğitim ve Kültür Vakfı (İSTEK), Florya Özel Bilge Kağan Lisesi (1988-1994)",yil:"1900"},
                        {id:3,adi:"Yeşilköy Hamdullah Suphi Tanrıöver İlköğretim Okulu, İstanbul (1983–1988)",yil:"1900"},
                      ],
                      lang:[
                        {id:0,adi:"ingilizce"},
                      ],
                      deneyim:[
                        {id:0,adi:"Özel Bodrum Hastanesi , Bodrum-Muğla (Temmuz 2011’den beri) ",yil:"1900"},
                        {id:1,adi:"Özel Anamed Hastanesi , Mersin ( Eylül 2010 – Temmuz 2011) ",yil:"1900"},
                        {id:2,adi:"Bingöl Özel Hastanesi, Bingöl (Ekim 2009 – Ağustos 2010 )",yil:"1900"},
                        {id:3,adi:"T.S.K 54’üncü Mekanize Piyade Tugayı, Harbiye Kışlası, Aile Reviri, Edirne (Kasım 2008 - Eylül 2009).",yil:"1900"},
                        {id:4,adi:"T.C. Sağlık Bakanlığı, Bingöl Doğum ve Çocuk Bakımevi Hastanesi, Bingöl (Zorunlu Hizmet kapsamında, Mart 2007-Eylül 2008 arası).",yil:"1900"},
                        {id:5,adi:"Türk Böbrek Vakfı, İstanbul Hizmet Hastanesi, Kadın Hastalıkları ve Doğum Departmanı (2006–2007 gece nöbetleri).",yil:"1900"},
                        {id:6,adi:"İstanbul Cerrahi Hastanesi (2007 gece nöbetleri).",yil:"1900"},
                        {id:7,adi:"Avcılar Medicana Hastanesi (2005–2007 gece nöbetleri)",yil:"1900"},
                        {id:8,adi:"Prof. Dr. Vildan Ocak (İ.Ü. CTF Kadın Hastalıkları ve Doğum ABD, Perinatoloji Bilim Dalı Başkanı) Özel Muayenehanesi, Mecidiyeköy, (2005–2007 yılları arasında ultrasonografist olarak)",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/omerorcun.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:4,
                      name:"Cengiz Kayahan",
                      title:"Doç. Dr.",
                      bolum:"Genel Cerrahi",
                      tarih:"24.10.1959",
                      birthlocation:"Ankara",
                      education:[
                        {id:1,adi:"İstanbul Üniversitesi, İstanbul Tıp Fakültesi",yil:"1900"},
                      ],
                      lang:[
                        {id:0,adi:"ingilizce"},
                      ],
                      deneyim:[
                        {id:0,adi:"2011-2017-Medicana International Hospital Samsun ",yil:"1900"},
                        {id:1,adi:"2009-2011-Medicalpark Özel Ordu Hastanesi",yil:"1900"},
                        {id:2,adi:"2008-2009-Özel Keçiören Hastanesi Ankara",yil:"1900"},
                        {id:3,adi:"2008-2008-Özel Caria Hastanesi Marmaris Muğla",yil:"1900"},
                        {id:4,adi:"2007-2008-Muayenehane Hekimliği",yil:"1900"},
                        {id:5,adi:"2004-2007-Emeklilik / Muayenehane Hekimliği",yil:"1900"},
                        {id:6,adi:"2000-2004-GATA Acil Tıp AD Öğr. Üyesi",yil:"1900"},
                        {id:7,adi:"2000 -Genel Cerrahi Doçenti",yil:"1900"},
                        {id:8,adi:"1999-2000-GATA Acil Tıp AD Öğretim Görevlisi",yil:"1900"},
                        {id:10,adi:"1993-1999-GATA Genel Cerrahi AD Yard. Doç.liği",yil:"1900"},
                        {id:11,adi:"1994:Siirt 30 Ytk.Seyy.Cerr.Hast Bştbp.liği (4 ay)",yil:"1900"},
                        {id:12,adi:"1996:Siirt 30 Ytk.Seyy.Cerr.Hast Bştbp.liği (4 ay)",yil:"1900"},
                        {id:13,adi:"1992-1993-Gölcük Dz. Hst. Genel Cerrahi Uzmanlığı",yil:"1900"},
                        {id:14,adi:"1988-1992-GATA Genel Cerrahi AD Uzmanlık Eğitimi",yil:"1900"},
                        {id:15,adi:"1987-1988-Dz.K.K.lığı TCG Turgut Reis Gemi Tabipliği",yil:"1900"},
                        {id:16,adi:"1986-1987-GATA Askeri Tıbbi Deontolojii Eğitimi",yil:"1900"},
                        {id:17,adi:"1985-1986-Dz.K.K.lığı Tuzla Dz H.O. Revir Tabipliği",yil:"1900"},
                        {id:18,adi:"1984-1985-Ank.Altındağ M.S.O.Tabipliği",yil:"1900"},
                        {id:19,adi:"1982-1984-Giresun Eynesil Sağ. Ocağı Tabipliği",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/cengiz-kayahan.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:5,
                      name:"Mehmet Demircioğlu",
                      title:"Dr.",
                      bolum:"İç Hastalıkları",
                      education:[
                        {id:1,adi:"2000 İstanbul Ü.Cerrahpaşa Tıp Fak İç Hastalıkları",yil:"1900"},
                        {id:2,adi:"1989 İstanbul Üniversitesi Cerrahpaşa Tıp Fakültesi",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"2011 - 2014 Memorial Antalya Hastanesi",yil:"1900"},
                        {id:1,adi:"2008 - Marmaris Caria Hastanesi",yil:"1900"},
                        {id:2,adi:"2004 - 2017 Acıbadem Sağlık Grubu ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/dr-mdemir.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:7,
                      name:"Muaffak Bağdatlı",
                      title:"Op. Dr.",
                      bolum:"Ortopedi ve Travmatoloji",
                      tarih:"16.11.1978",
                      birthlocation:"Bornova",
                      education:[
                        {id:1,adi:"1990 Çukurova Üniversitesi Tıp Fakültesi Kulak, Burun ve Boğaz Hastalıkları",yil:"1900"},
                        {id:2,adi:"1983 Çukurova Üniversitesi Tıp Fakültesi",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"2013 - Halen Acıbadem Sağlık Grubu ",yil:"1900"},
                        {id:1,adi:"2004 - 2013Universal Hospital Bodrum Hastanesi ",yil:"1900"},
                        {id:2,adi:"1998 - 2002Ankara Egitim ve Arastirma HastanesiOrtopedi ve Travmatoloji Asistanlik Programi ",yil:"1900"},
                        {id:3,adi:"1996 - 1997 Gediz Hastanesi ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/dr-murat.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:8,
                      name:"Alp Mustafa Günay",
                      title:"Dr.",
                      bolum:"İç Hastalıkları ve Gastroenteroloji",
                      tarih:"03.09.1966",
                      birthlocation:"Eskişehir",
                      mail:"alp.mustafa.gunay@acibadem.com.tr",
                      education:[
                        {id:1,adi:"2000'de Gastroenteroloji Yandal Uzmanı olarak mezun olmuş",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"Gülhane Askeri Tıp Akademisi'nden 1990'da Tıp Doktoru",yil:"1900"},
                        {id:1,adi:"1997'de İç Hastalıkları Uzmanı",yil:"1900"},
                        {id:2,adi:"9 yabancı, 16 yerli yayını ve 27 yabancı, 24 yerli bildirisi mevcuttur. ",yil:"1900"},
                        {id:3,adi:"2010'da da Avrupa Birliği Tıp Uzmanları Derneği'nden (EUMS) Avrupa Gastroenteroloji ve Hepatoloji Uzmanlık Sertifikası (EBGH) almıştır. ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/alp-mustafa-gunay.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:9,
                      name:"Tahir Aşkın Laçin",
                      title:"Dt.",
                      bolum:"Ağız ve Diş Sağlığı",
                      tarih:"1956",
                      lang:[
                        {id:0,adi:"İngilizce"},
                      ],
                      education:[
                        {id:1,adi:"ANKARA ÜNİVERSİTESİ DİŞ HEKİMLİĞİ FAKÜLTESİ - 1980",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"T.C. Ziraat Bankası Kadıköy Sağlık Polikliniği - Diş hekimi / İstanbul (1982-2007)",yil:"1900"},
                        {id:1,adi:"Diş Hekimi - Özel Muayenehane / İstanbul (2007-2009)",yil:"1900"},
                        {id:2,adi:"İstanbul Dentestetik Diş Polikliniği - Diş Hekimi, Mesul Müdür / İstanbul (2009-2011)",yil:"1900"},
                        {id:3,adi:"Dentalform Estetik Diş Klıniği - Diş Hekimi / İstanbul (2011-2015)",yil:"1900"},
                        {id:4,adi:"Diş Hekimi, Clinic International BMC Tip Merkezi, Bodrum (2015-... ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/tahir-askin-lacin.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:11,
                      name:"Özgür Bayındır",
                      title:"Dr.",
                      bolum:"Acil Hekimi",
                      tarih:"11.04.1973",
                      birthlocation:"Tire",
                      lang:[
                        {id:0,adi:"İngilizce"},
                        {id:1,adi:"İspanyolca"},
                      ],
                      education:[
                        {id:1,adi:"İSTANBUL ÜNİVERSİTESİ ÇAPA TIP FAKÜLTESİ 1991-1998",yil:"1900"},
                        {id:1,adi:"İZMİR BORNOVA ANADOLU LİSESİ 1985-1991",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"LÖSEMİLİ ÇOCUKLAR VAKFI 1999-2000",yil:"1900"},
                        {id:1,adi:"BRISTOL MYERS SQVIBA, CLINIC RESEARCH ASSOCIATE 2000-2001",yil:"1900"},
                        {id:2,adi:"CLINIC INTERNATIONAL TIP MERKEZİ, BODRUM 2001- ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/dr-ozgur.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:14,
                      name:"Fikret Mert Acar",
                      title:"Uzm. Dr.",
                      bolum:"Kardiyolji",
                      tarih:"25.03.1979",
                      birthlocation:"Manisa",
                      education:[
                        {id:1,adi:"2003-Hacettepe Üniversitesi Tıp Fakültesi",yil:"1900"},
                        {id:2,adi:"2010-İstanbul Üniversitesi Kardiyoloji Enstitüsü",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/fikret-mert-acar.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:15,
                      name:"Pelin Akıncı Demiray",
                      title:"Dt.",
                      bolum:"Ağız ve Diş Sağlığı",
                      tarih:"15.08.1992",
                      lang:[
                        {id:0,adi:"English"},
                      ],
                      birthlocation:"Edirne",
                      education:[
                        {id:1,adi:"ISTANBUL UNIVERSITY FACULTY OF DENTISTRY",yil:""},

                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/yok.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:16,
                      name:"Alp Kemal Okyay",
                      title:" Uzm.Dr.",
                      bolum:"Aile Hekimliği",
                      tarih:"25.03.1967",
                      lang:[
                        {id:0,adi:"English"},
                      ],
                      birthlocation:"Ankara",
                      education:[
                        {id:1,adi:"ANKARA UNIVERSITY FACULTY OF MEDICINE",yil:""},
                        {id:2,adi:"ANKARA EDUCATION AND RESEARCH HOSPITAL FAMILY MEDICINE",yil:""},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/yok.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                  ]
                }
              },
            },
            //acenterler
            {
              id:192,
              paths:'/anlasmali',
              adi:"Contracted Agencies",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              subMenu:[
                {
                  id:1678361,
                  paths:'/yerlisigortalar',
                  birimAdi:"Contracted Agencies ",
                  adi:"Local Agencies",
                  sidemenuVisible:true,
                  exact:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Contracted Agencies",
                    icerikContext:[
                        {
                            tag:"p",
                            icerik:"Contracted Agencies",
                            classname:"content-p-big",
                        },
                        {
                          tag:"li",
                          icerik:"AMERİCAN LİFE SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"EUREKO SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"YAPIKREDİ SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"ANADOLU SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"GROUPAMA SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"DEMİR HAYAT SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"AXA SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"ACIBADEM SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"ERGO SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"PROMED",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"RAY SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"GÜNEŞ SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"AK SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"MEDNET",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"ALLİANZ SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"AMERİCAN LİFE SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"MAPFRE GENEL YAŞAM SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"AKBANK T.A.Ş.TEKAÜT SANDIĞI VAKFI",
                          classname:"content-p",
                        },
                    ]
                  },
                },
                {
                  id:16267247278361,
                  paths:'/yabancisigortalar',
                  birimAdi:"Contracted Agencies ",
                  adi:"Foreign Agencies",
                  sidemenuVisible:true,
                  exact:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Foreign Agencies",
                    icerikContext:[
                      {
                          tag:"p",
                          icerik:"Contracted Agencies",
                          classname:"content-p-big",
                      },
                      {
                        tag:"p",
                        icerik:"England",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:"Assistance International",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"ChargeCare International",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Intergroup Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"AXA Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Europ Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Thompson Travel Insurances",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Cega",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"First Assist",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Speciality Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Holland",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:"Avero Achmea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Zorgenzekerheid",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"IZZ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"VGZ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ohra-Delta Lloyd",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial-Elvia",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"DSW",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Europeesche Verzekeringen",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Eurocenter",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"SOS International (Ass.Company)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"ANWB(Ass.Company)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Eurocross International (Ass.Company)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Neckermann Travel Insurances",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"FBTO",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Zilveren Kruis Achmea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"IZA",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"DeFriesland",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Unive",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Menzis",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"CZ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"OZ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Groene Land Achmea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Trias",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"France",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:"AXA Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Europ Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mutuaide Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial-Elvia",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"France Secours",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Belgium",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:"Eurocross Belgium ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Interassistance-Europese",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Thomas Cook Travel Insurances",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ethias",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Interpartner Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial-Elvia",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:85683273,
                  paths:'/oteller',
                  birimAdi:"Contracted Agencies ",
                  adi:"Hotels",
                  sidemenuVisible:true,
                  exact:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Hotels",
                    icerikContext:[
                        {
                            tag:"p",
                            icerik:"Contracted Agencies",
                            classname:"content-p-small",
                        },
                        {
                          tag:"li",
                          icerik:"Voyage Torba (Torba)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Voyage Bodrum Charm (Bardakçı)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Voyage Hebilköy (Türkbükü)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Blue Dreams Hotel (Torba)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Bodrum Holiday Resort (İçmeler)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Club Ersan (İçmeler)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Forever Hotel (İçmeler)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Manuela Hotel (Bitez)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Işıl Club Milta",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Rixos Hotel",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Jumeirah Hotel",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Vogue Hotel",
                          classname:"content-p",
                        },
                    ]
                  },
                }
              ],
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Deneme Başlığıdğr",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Deneme psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  }
                ]
              },
            },
            {
              id:200,
              paths:'/fotogaleri',
              adi:"Photo Gallery",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Photo Gallery",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Squares from our hospital",
                    classname:"content-p",
                  },
                ],
                fotogallery:true
              },
            },
            {
              id:210,
              paths:'/ik',
              adi:"Human Resources",
              sidemenuVisible:true,
              exact:false,
              insan_kaynaklari:true,
              componenti:0,
              emaillang:{
                kisisel:"Personal Information",
                egitim:"Educational Information",
                sonmezunOlunan:"Last Graduated School :",
                yabanciDil:"Foreign Language",
                pozisyon:"Position :",
                basvuruTarih:"Application date",
                adSoyad:"Fullname :",
                dogumYeri:"Your place of birth :",
                dogumTarih:"Birth Date :",
                cinsiyet:"Gender :",
                uyruk:"Nationality",
                mesaj:"Message :",
                gonder:"Send",
                cep:"Mobile Phone :",
                randevu_birim:"Randevu İstediğiniz Birim :",
                randevu_tarih:"Randevu İstediğiniz Tarih :",
                randevu_saat:"Randevu İstediğiniz Saat :",
                aciklama:"Açıklama"
              },
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"JOB APPLICATION FORM",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:`Vision We aim to employ people who are qualified in the direction of our mission and corporate values, who are capable of teamwork, who are open to innovations, open to innovations, potential to develop themselves and business, and to develop these people with continuous training programs, effective performance and reward systems.`,
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:`Job Application Form:`,
                    classname:"content-p-small",
                  },
                ]
              },
            },
            {
              id:220,
              paths:'/gorusoneri',
              adi:"Comments and Suggestions",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              email_gorus:true,
              emaillang:{
                adSoyad:"Full Name :",
                mesaj:"Message :",
                gonder:"Send"
              },
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Comments and Suggestions",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Your views and suggestions are important to us. Please share your opinions and suggestions with us.",
                    classname:"content-p",
                  },
                ]
              },
            },
            //randevu
            {
              id:230,
              paths:'/e_randevu',
              adi:"Appointment",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              emaillang:{
                adSoyad:"Fullname :",
                mesaj:"Message :",
                gonder:"Send",
                cep:"Mobile Phone :",
                randevu_birim:"Appointment Unit :",
                birimlang:[
                  {adi:"Family Medicine"},
                  {adi:"General Surgery"},
                  {adi:"Cardiology"},
                  {adi:"Urology"},
                  {adi:"Skin Diseases"},
                  {adi:"Gynecology Department"},
                  {adi:"Gastroenterology"},
                  {adi:"Internal Diseases"},
                  {adi:"Orthopaedics and Traumatology"},
                  {adi:"Eye Diseases"},
                  {adi:"Otorhinolaryngology"},
                  {adi:"Dentistry"},
                ],
                randevu_tarih:"Appointment Date you want :",
                randevu_saat:"Appointment Date you time :",
                aciklama:"Explanation"
              },
              email_gorus:false,
              email_randevu:true,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Appointment",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"You can create your online appointment request from here ..",
                    classname:"content-p",
                  },
                ]
              },
            },
            {
              id:19451812142,
              paths:'/hastahikayeleri',
              adi:"Patient Story",
              sidemenuVisible:false,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Patient Story",
                icerikContext:[
                  {
                    tag:"div",
                    icerik:"Patient Story",
                    classname:"content-p-big",
                  },
                  {
                    tag:"p",
                    icerik:"",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Tops Nadine, hotel Voyage Bodrum Thanks very much",
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:"Hallo,",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Via deze weg willen we jullie erg hard bedanken voor de goede zorgen die jullie hebben gegeven.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Dankzij jullie dokters,verplegers,al het andere personeel en vooral de nederlandstalige mevrouw voelden we ons meer dan comfortabel in",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Clinic International. We bedanken jullie hiervoor,en willen we ook nog meegeven dat we onze reis hebben voortgezet naar Kos.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Ondertussen zijn we terug thuis en daarom dit mailtje.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Nogmaals bedankt,",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Nadine Tops,",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Ik was bij jullie van 29/05 tot 30/05/2013.",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"div",
                    icerik:"Dear Dr. Ozgur and all your wonderful staff ...",
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:`At first: I'm sorry it has taken me so long to write this....
                    I want to thank you and your staff for the care I received during my recent stay at Clinic International. Thank you for all you do. It means a lot. You guys do a lot for so many. I dont know where I would be without your great help.`,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`I especially want to thank Dr Omur- one of the sweetest and friendliest doctor-, Nurse Duygu ( my angel) and her fiancé Mehmet, Ebru, Baris , Bulent and Steve. But I never met a doctor in my life like Dr. Ali, that has shown so much compassion for his patient as he did for me! I have to tell that every single one of them gave me excellent care/ attention/ emotional support! My stay in the hospital was a bit more tolerable because of your excellent and very profesisional care ! We need a Clinic International in every town.`,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Also, a big thank you to the drivers of the buses. They were very polite caring of their passengers.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Thank you all much more than I can ever express !",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`with Kind Regards
                    Leyla Karnaz ( Holland )`,
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:"Met vriendelijke groet,",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"div",
                    icerik:"Dear Clinic-international staff,",
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:`Today I visited an eye specialist in The Netherlands. Everything was ok. No problems with the cornea, eye-pressure and sight.`,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`My specialist complimented you with your great care and professional treatment. She said: “you were treated better and more efficient than you would have been in The Netherlands”.`,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Greetings to the medical team, dr. Ömür aydoğan, Laverne Costello Clarke and my personal “Florence Nightengale’ (the stunning blonde ;-)).",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"The last two have made my stay in your clinic very comfortable. Mr. Clarke supplied me with a WiFi-code, so I could talk to my family with LiveView and could find some information about the herpes-zoster-virus.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`Thank you very much! You were great!`,
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"div",
                    icerik:"Dear doctor, ",
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:`I'll like to thank you for the marvellous care!!!
                    Now, at home, I realise how gratefull I am to meet you at the moment I was afraid, miserable, sick and lonely!
                    Except y'are a friend of Octay, I even know your name and I never ask about....
                    Congratulations for the hole team, everybody was friendly, kind and helpfull in the clinic. `,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`With kind regards, `,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Mieke Demandt",
                    classname:"content-p",
                  },
                ]
              },
            },
            {
              id:1891486481,
              paths:'/iletisim',
              adi:"Communication",
              sidemenuVisible:false,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Communication",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"İnformation",
                    classname:"content-p-big",
                  },
                  {
                    tag:"p",
                    icerik:"Adress",
                    classname:"content-p-small",
                  },
                  {
                    tag:"p",
                    icerik:"Kıbrıs Şehitleri Cad. No 181 (Antik Tiyatro Çaprazı) 48400 Bodrum - Türkiye",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Phone",
                    classname:"content-p-small",
                  },
                  {
                    tag:"p",
                    icerik:"0252 313 30 30",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Fax",
                    classname:"content-p-small",
                  },
                  {
                    tag:"p",
                    icerik:"0252 316 00 08",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Email",
                    classname:"content-p-small",
                  },
                  {
                    tag:"p",
                    icerik:"info@clinicinternational.com.tr",
                    classname:"content-p",
                  },
                  {
                    tag:"map",
                    icerik:"info@clinicinternational.com.tr",
                    classname:"content-p",
                  },
                ]
              },
            },
           ]
        },
        fr: {
          baslik:"ENTRUST YOUR HEALTH",
          anasayfa:"HOME",
          veri:[
            //110
            {
              id:110,
              paths:'/',
              adi:"Home",
              sidemenuVisible:false,
              exact:true,
              componenti:1,
              slider:[
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webgeçiştirmeyin.jpg",
                  link:"http://bit.ly/2EgnDBD"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webmiyom.jpg",
                  link:"http://bit.ly/2H1njUR"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webtansiyon.jpg",
                  link:"http://bit.ly/2nLiwiw"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webterleme.jpg",
                  link:"http://bit.ly/2G0IK7e"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webtipIIdiyabet.jpg",
                  link:"http://bit.ly/2nQdX6t"
                },
                {
                  img:"http://clinicinternational.com.tr/2011/g/banner/webtüpmide.jpg",
                  link:"http://bit.ly/2C8TC0i"
                },
                {
                  img:"http://media.kenttv.net/20180204062659_kenttv_haber_606.jpg?v=1",
                  link:"https://www.kenttv.net/haber.php?id=40834"
                },
                {
                  img:"http://media.kenttv.net/20180221040704_kenttv_haber_606.jpg?v=1",
                  link:"http://www.kenttv.net/haber.php?id=41015"
                },
              ],
              widget:[
                {
                  id:"widget1",
                  link:"/hastahikayeleri",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"PATİENT STORİES",
                  widgetpic:"http://www.kadinhaberleri.com/images/haberler/kil_donmesi_kadinlarda_olur_mu_h600513.jpg"
                },
                {
                  id:"widget2",
                  link:"/iletisim",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"COMMUNICATION",
                  widgetpic:"https://www.framestr.com/wp-content/uploads/2017/08/emergency-contact-form.jpg"
                },
                {
                  id:"widget3",
                  link:"/check_up",
                  classname:"widgettriple",
                  classwrap:"widget-box",
                  title:"CHECK-UP",
                  widgetpic:"https://acibademmobil-wbeczwggocwcfpv3pwm.stackpathdns.com/wp-content/uploads/2017/01/bakicim-check-up-doktor-muayenesi.jpg"
                },
                {
                  id:"widget4",
                  link:"/hakkimizda",
                  classname:"widgetsecond",
                  classwrap:"widget-box2",
                  title:"VİDEO GALLERY",
                  widgetpic:"http://ahps.org.sg/wp-content/uploads/photo-gallery/41144817-HEALTH-word-cloud-concept-Stock-Photo-health.jpg"
                },
                {
                  id:"widget5",
                  link:"/hakkimizda",
                  classname:"widgetsecond",
                  classwrap:"widget-box2",
                  title:"HEALTHY INFORMATION",
                  widgetpic:"https://i.pinimg.com/736x/c4/db/c0/c4dbc0f1f6c23496f433ac82bc4cef83--healthy-tips-healthy-eating.jpg"
                },
              ],
              haberlerbaslik:"NEWS and ANNOUNCEMENTS",
              haberler:[
                {
                  id:"widget1",
                  link:"/hakkimizda",
                  mesaj:`6 yıldır Bodrum'da başka sağlık kuruluşlarında çalışan Kardiyoloji ( Kalp Hastalıkları ) Uzmanı Doktor Fikret Mert Acar kurumumuzda SGK anlaşmalı olarak çalışmaya başlamıştır, kendisine başarılar diliyoruz.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
                {
                  id:"widget2",
                  link:"/hakkimizda",
                  mesaj:`5 Yıldır Bodrum'da başka bir sağlık kuruluşunda çalışan Kadın Hastalıkları ve Doğum Uzmanı Op.Dr. Ömer Orçun Koçak merkezimizde SGK anlaşmalı olarak çalışmaya başlamıştır, kendisine başarılar diliyoruz.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
                {
                  id:"widget3",
                  link:"/hakkimizda",
                  mesaj:`Genel Cerrahi Uzmanı Doçent Doktor Cengiz Kayahan SGK anlaşmalı olarak hasta kabulüne başlamıştır kendisine başarılar diliyoruz.Hekimimiz özellikle obezite cerrahisi (mide küçültme ameliyatları ) , tiroid cerrahisi ( guatr ameliyatları ) bağırsak ameliyatları, ve meme cerrahisi konusunda üstün deneyime sahiptir.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
                {
                  id:"widget4",
                  link:"/hakkimizda",
                  mesaj:`Kulak Burun Boğaz Uzmanı Op. Dr. Lemi Özer merkezimizde SGK anlaşmalı olarak çalışmaya başlamıştır. 3 yıl boyunca Acıbadem Bodrum Hastanesi'nde görev yapan hekim, başarılı meslek hayatına tıp merkezimizde devam edecektir.  Kendisine başarılar diliyoruz.`,
                  widgetpic:"http://clinicinternational.com.tr/2011/g/box1.jpg"
                },
              ],
              icerikveri:{
                icerikImage:"https://paratic.com/dosya/2015/11/kadinlari-cezbeden-birbirinden-cekici-araba-modelleri-bugatti-veyron.jpg",
                icerikHeader:"Deneme Başlığıdğr",
                icerikContext:"Yukardakinin aynısı"
              },
            },
            //110bitis
            //120
            {
              id:120,
              paths:'/creator',
              adi:"About Us",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"About Us",
                icerikContext:[
                  {
                    tag:"div",
                    icerik:"Bodrum is a holiday paradise and a dream come true for thousands of people from around the world. However, sometimes unexpected diseases and accidents can turn your well planned holiday up side down. ",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"It becomes a source of distress and anxiety when unexpected situations such as accidents, diseases and injuries happen in unfamiliar country,city or culture . ",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"We understand your fears very well and strive to create a comfortable, safe and peaceful environment for you in Clinic International with our friendly and modern service concept. We are at your service for 24 hours to find out solutions to your problems as soos as possible(ASAP) and make you feel at home. ",
                    classname:"content-p",
                  },
                  {
                    tag:"br",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Private Clinic International Medical Center is a boutique clinic with its outstanding, friendly and experienced staff, fast and definitive service approach. We are increasing the service quality by following and updating the medical and technological developments in the health sector with respect to patient rights and ethics.",
                    classname:"content-warning",
                  },
                ]
              },
            },
            //120bitis
            //130
            {
              id:130,
              paths:'/tibbibirimler',
              adi:"Medical Units",
              sidemenuVisible:true,
              exact:false,
              subMenu:[
                {
                  id:131,
                  paths:'/kadin',
                  birimAdi:"Medical Units ",
                  adi:"Gynecology",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Gynecology",
                    icerikContext:[
                      {
                        tag:"li",
                        icerik:"Women's health inspection",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Pregnancy monitoring",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Control examination after birth",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ultrasonography",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Menapouse follow-up",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Family planning application",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Infertility follow-up",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mestruel disorders follow up",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Cervical control and biopsies",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Birth",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Gynecological operations",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Pap-smear test (Cervical smear)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Breast examination",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Endometrial biopsy",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Intrauterine device (RIA) insertion",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Gynecology is made by our experienced gynecologist in our clinic.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:132,
                  paths:'/ortopedi',
                  birimAdi:"Medical Units ",
                  adi:"Orthopaedics and Traumatology",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Orthopaedics and Traumatology",
                    icerikContext:[
                      {
                        tag:"li",
                        icerik:"Sports Injuries",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Muscular-skeletal system fractures and soft tissue injuries' surgical and non-surgical treatments",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Spine fractures, injuries and infections",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Surgical intervantion of knee, hip and shoulder joint calcifications",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Every arthroscopic approach",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Peripheral nevre impingements, carpal nerve syndrome,ulnar tunnel syndrome,",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Posterior interosseous syndrome",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Open and closed reduction of fractures",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Closed fructure surgeries ( minimal invasive surgery)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Corrective surgery of orthopedic malformations",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"All kinds of joint prosthesis placement (hip, elbow, finger, shoulder..)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Surgery of curvature of the spine and fractures",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:133,
                  paths:'/cocuk',
                  birimAdi:"Medical Units ",
                  adi:"Peadiatrics",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Peadiatrics",
                    icerikContext:[
                      {
                        tag:"li",
                        icerik:"Examinations related to all stages of childhood diseases between the ages of 0-15 including follow up and treatments",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Growth development monitoring",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Routine and special vaccines",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Child education consultatıons",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Nutrition and breastfeeding counseling",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Newborn screening tests made by our specialist",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Our clinic is made by our specialist physician.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:134,
                  paths:'/acil',
                  birimAdi:"Medical Units ",
                  adi:"Emergency Services",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Emergency Services",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Emergency service is the department where first intervention and treatment is made for many emergencies such as incisions, fractures, falls, high fever, abdominal pain, stomach bleeding, stroke, traffic accidents, heart attacks, heart rhytm disorders, cardiopulmonary arrest, assaults, poisonings, suicide attempts. ",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"In our emergency department , all patients receive first intervention treatment for all emergency diseases.",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"In our emergency department, 9 patient rooms, 3 observation rooms and an emergency room are available.",
                        classname:"content-p",
                      }
                    ]
                  },
                },
                {
                  id:135,
                  paths:'/agiz',
                  birimAdi:"Medical Units ",
                  adi:"Dentistry",
                  sidemenuVisible:true,
                  componenti:0,
                  subMenu:[
                    {
                      id:135256,
                      paths:'/imp',
                      birimAdi:"Medical Units ",
                      adi:"İmplantoloji ve Cerrahi Uygulamalar",
                      sidemenuVisible:true,
                      subMenu:[
                        {
                          id:262626131236,
                          paths:'/implant',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"İmplant",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"İmplant",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"İmplantoloji",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Diş kayıplarında tercih edilebilecek en iyi yöntem implanttır.
                                İmplantlar insan vücudu tarafından kolay kabuledilebilen ve çene kemiği içinde kök görevini gören titanyumdan yapılırlar.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`İmplantoloji denenmiş ve başarı sağlanmış bir tedavidir. Günümüz modern implantların %90’ı enaz 15 yıl kullanım sağlamaktadır. İmplantın başarısında kemiğin durumu çok önemlidir. Operasyon öncesinde hem klinik hem de radyolojik araştırma detaylı olarak yapılarak kemiğin durumu değerlendirilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Operasyon lokal anestezi altında yapılır ve bir implantın yerleştirilmesi yaklaşık 15-20 dakika alır.
                                Diş etinden flap kaldırılıp implant yerleştirildikten sonra diş etine tekrar dikiş atılır. İmpalantın yerleştirilmesinden sonra iyileşme 2-4 ay arası sürecektir. Kemik dokusu ve implantın kaynaştığı bu süre içinde beklemek gerekmektedir. Bu sürenin ardından implant kemik içine adapte olur. Daha sonra üst yapı yapılması için kapalı implantın üzeri açılır ve abutmentlar yerleştirilerek ölçü alınır. Daha sonra üst kuronlar hazırlanır, bu işlem 5-7 gün almaktadır.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:32424364,
                          paths:'/gom',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Gömük Diş Çekimi",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Gömük Diş Çekimi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Diş Çekimi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Basit Çekim",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Komplike Çekim ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Cerrahi Operasyon",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Diş çevresinde iyileşmeyen kronik iltihap",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dolgu ya da kuronla tamir edilemeyecek kadar diş dokusu kaybı olan dişler",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Çevre kemik dokusunda aşırı rezorbsiyon oluşmuş ve bu nedenle mobil hale gelmiş dişler",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Pozisyonu bozuk dişler",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Ortodontik tedavi için yer kazanılması gereken durumlarda diş çekimi yapılır.",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Diş çekimi öncesinde dijital röntgen çekilerek, olası komplikasyon riski minimize edilir.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kliniğimizde bütün komplike çekimler ve ağız içi cerrahi operasyonlar cerrahi uzmanı diş hekimleri tarafından yapılmaktadır.",
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                      ],
                      componenti:0,
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Deneme Başlığıdğr",
                        icerikContext:[
                          {
                            tag:"p",
                            icerik:"Teşhis ve tedavi plan",
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Bu nu bilmiyorum psidir.",
                            classname:"content-p",
                          }
                        ]
                      },
                    },
                    {
                      id:134546,
                      paths:'/kozmadis',
                      birimAdi:"TIBBİ BİRİMLER ",
                      adi:"Kozmetik Diş Hekimliği",
                      sidemenuVisible:true,
                      componenti:0,
                      subMenu:[
                        {
                          id:1515135,
                          paths:'/bleaching',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Bleaching",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Bleaching",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Bleaching",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Birçok araştırmaya göre dişler yüz estetiğini belirleyen ikinci en önemli kriterdir. Dişlerin renkleride kesinlikle biçim ve dizilişleri kadar önemlidir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Beyazlatma, parlak beyaz bir gülümsemeye sahip olmayı sağlayan en kolay ve ağrısız tedavidir. Basit ve güvenli olan bu tedavi son derece çarpıcı sonuçlar ortaya çıkarmaktadır. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kliniğimizde BDHF tarafından tavsiye edile, sonderece güvenli bir beyazlatma sistemi uygulanmaktadır. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Hastalarımız diş hekimi koltuğunda rahat bir şekilde uzanıp müzik dinlerken beyazlatma işlemi şu işlemlerde gerçekleştirilir.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"İlk adımda hastanın dudaklarını ve dişetlerini özel bariyerlerle izole edilerek beyazlatma ilacının dişetileri ve yumuşak dokularla teması engellenir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Ardından beyazlatma jeli dişlerin yüzeyine uygulanarak ışınla aktivasyonu sağlanır. Daha sonra jel diş yüzeyinden uzaklaştırılarak renk değişimi kontrol edilir. Bu işlem istenilen renge ulaşılıncaya kadar birkaç kez tekrar edilebilir.",
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:34621689,
                          paths:'/porselen',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Porselen Laminate",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Porselen Laminate",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Porselen Laminate",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Porselen laminate dişlerinizin şeklini, boyutunu, rengini ya da pozisyonunu değiştirerek gülüşünüzü güzelleştirmek amacıyla hazırlanana ince porselen bir tabakadır. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"En az miktarda diş materyali aşındırılarak, uzun süreli, mükemmel bir görünüşün sağlandığı koruyucu bir tekniktir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Özel rezin simanlarla dişe yapıştırılırlar ve düşmeleri çok zordur. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Porselen yüzeyi lekelenmelere, boyanmalara karşı oldukça dayanıklıdır, estetik ve doğal bir görünüm sağlarlar ve de diş etiyle çok uyumludurlar. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Laminatelerin hazırlanması 1 hafta kadar sürmektedir, bu sürede de 2-3 randevu gerekmektedir:
                                İlk randevuda diş yüzeylerinden bir miktar aşındırma yapılarak dişler laminate için hazırlanır. Ardından ölçü alınıp geçici restorasyon materyali ile açık dentin yüzeyi kapatılır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"İkinci randevuda geçiciler kaldırıldıktan sonra laboratuar tarafından hazırlanmış olan laminateler prova edilip, hasta ve hekim hemfikir ise özel resin simanlarla yapıştırılır. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Tedavi planları kişiden kişiye değişiklik göstermektedir. Sizin için en uygun tedavi seçeneği ve planlaması için dişhekimine muayene olmanız şarttır.",
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:5848237,
                          paths:'/bonding',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Bonding",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Bonding ve Estetik Dolgular",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Bonding",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Dental bonding estetik amaçla kullanılan bir tür dolgu materyalidirDişlerdeki küçük boşlukları kapatmak, dişleri yeniden şekillendirmek ve mine yüzeyinde oluşan doğal hasarı kapatmak için rutin olarak kullanılan estetik restorasyon materyalleridir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kompozit dolgu materyalinin çeşitli renk tonları vardır. Böylece dişinizin kendi rengine kolaylıkla uyum sağlar. Geleneksel amalgam dolgularınız kompozit dolgularla değiştirilebilir. Böylece dişleriniz sağlıklı bir ağız ortamıyla beraber estetik ve doğal bir görünüşe sahip olmuş olur. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kliniğimizde yüksek kalitede, dayanıklı ve kuvvetli kompozit materyali kullanmaktayız. Kullandığımız tüm malzemeler FDA, ADA ve Türk Sağlık Bakanlığı tarafından onaylıdır.",
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:9840059,
                          paths:'/smile',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Smile Design",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Smile Design",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Smile Design",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Gülüşünüzden memnun musunuz?",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dişlerinizin şekli ve rengi sizi rahatsız ediyor mu?",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Hiç başka birinin gülüşüne sahip olmak istediniz mi?",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dişetlerinizin görüntüsü sizi mutlu ediyor mu?",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Gülmek,yüzümüzdeki pek çok kas grubunun çalışmasıyla oluşan ve dişlerimizin göründüğü bir mimiktir. Gülüşümüzü etkileyen pek çok faktör vardır. Güzel bir gülüş için öncelikle yüz biçiminizle ,dudak yapınızla uyumlu dişlere sahip olmanız gerekir. İşte gülüş dizaynı, size kişisel özellikleriniz ve isteklerinizle doğallık ve fonksiyonu birleştirerek sizin için en ideal gülümsemeyi oluşturmak için vardır. Pırıl pırıl bir gülümseme hepimizin hoşuna gider. O kişi hakkında olumlu hisler hissetmemizi sağlar. Araştırmalar gösteriyor ki, dişler gözlerden sonra en önemli ikinci estetik unsurdur. Ahenkli bir gülümseme iletişimde her zaman avantaj sağlar. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"GÜLÜŞÜMÜZÜ ETKİLEYEN FAKTÖRLER",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"CİNSİYET",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"YÜZ ŞEKLİ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"YAŞ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"DUDAKLAR",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"DİŞ ETLERİ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"DİŞLER",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`1-CİNSİYET: Kadın ve erkek anatomisi birbirinden farklıdır. Erkeklerde yüz hatları daha keskin ve belirgindir.Alın burun, çene ucu orantısı kadın yüzüyle farklılıklar gösterir.Kadınlarda geçişler daha yumuşak burun ve kaş kemerleri daha siliktir. Dişlerde de aynı paralellik vardır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kadınlarda:",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Gülme hattı yukarı doğru kavislidir ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dişlerin köşeleri daha yumuşak döner. ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Komşu dişlerin köşeleri arasında minik aralıklar vardır.",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Ortadaki iki diş yandaki dişlerden biraz daha uzundur. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Erkeklerde:",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Gülme hattı daha düzdür. ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Dişlerin hatları daha belirgindir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Komşu dişler daha düz bir hatta birleşirler ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`2-YÜZ ŞEKLİ: Yüz şeklinizle diş formlarınız arasında benzerlikler bulunur. Genellikle uzun yüzlü kişilerde diş formları da uzun; kare veya yuvarlak yüzlü kişilerde diş formları da kare veya yuvarlak olur. Klasik diş hekimliğinde bu benzerlikler korunmaya çalışılarak restorasyon yapılırdı. Ancak estetik yönden bir şeyleri değiştirmek istediğinizde bu benzerlikleri tersine çevirerek farklı ifadeler veren illüzyonlar elde edilebilir. Örneğin uzun yüzlü bir kişiye dikdörtgen formda uzun dişler yapılırsa yüzü olduğundan da uzun görünecektir. Oysa oval veya daha geniş formlar denenerek yüzdeki hoş olmayan uzunluk kamufle edilebilir. Veya yuvarlak yüzlü bir kişiye daha ince uzun formda dişler yapılarak yüzünün daha ince görünmesi sağlanabilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`3-YAŞ: Yaşlanmaya karşı yürütülen savaş her yıl milyonlarca insanı da içine alarak büyüyor. Kadın ve erkek fark etmeden hepimiz genç ve güzel görünmeyi sürdürmek istiyoruz.Kozmetik dişhekimliği bu konuda oldukça önemli yardımlar yapabilir.Gülüşümüz yüzümüzdeki en önemli gençlik kriterlerinden biridir. Yaşla birlikte dişlerimizin renginde bir koyulaşma,boylarında kısalma meydana gelir.

                                Dokuların sarkmasıyla beraber üst dudağımız da yerçekimi etkisiyle aşağıya doğru sarkar. Dikey boyutumuz kısaldığı için çene ucu-burun ucu birbirine olması gerekenden çok yaklaşır. Konuşurken ve gülerken üst dişlerimizin daha az, alt dişlerimizin daha çok, gözükmesi sonucunda daha yaşlı bir ifadeye sahip oluruz. Birçok kişi çok yanlış bir inanışla yaşlanınca sadece takma dişler yaptırabileceklerini sanıyorlar. Oysa dikey boyut yükseltilmesi, diş beyazlatma, kozmetik düzenleme, bonding, laminate veneer, implant gibi yöntemlerle 20 yıl önceki gülüşünüze kavuşmanız hiç de hayal değildir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`4-DUDAKLAR: Tıpkı bir çerçevenin tabloyu şekillendirdiği gibi dudaklar da dişlerimizi ve gülüşümüzü şekillendirirler.Gülüş dizaynı ;dudakların kalın,ince, uzun,kısa vs. olmalarına göre düzenlenerek var olan kusurlar kapatılabilir `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`5-DİŞETLERİ: Dişetleri dişlerimizi en yakından çerçeveleyen aksesuarlardır. Dişlerimiz güzel olsa bile sağlıksız(şiş, kırmızı ve parlak)dişetleri gülüşümüzün çirkin görünmesine neden olurlar.
                                bazen de dişetlerimiz sağlıklıdır ama gülünce gereğinden fazla gözükürler.Ya da dişeti çekilmesi oluşmuştur ve gözükmelerini istediğimiz kadar dişetimiz yoktur

                                Bu gibi durumlarda gülüşümüzde oluşan rahatsız edici görüntüler Varolan dişetlerine sağlık kazandırılarak ve ya dişetlerine küçük estetik müdahaleler yapılarak düzenlenebilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`6-DİŞLER: Günümüzde modern dişhekimliği konseptinde estetik sınırlar içinde dişlerimizi güzel göstermek için yapılabilecek çok fazla alternatifimiz var.Önemli olan sorunları doğru tespit edebilmek ve soruna yönelik, hedefi bulan,minimal müdahale ile maksimum estetiği sağlayabilen tedavileri seçmektir.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:591924,
                          paths:'/dispirlantasi',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Diş Pırlantası",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Diş Pırlantası",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Diş Pırlantası",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Çeşitli şekillerde ve renklerdeki küçük taşlardır. Bazısı kıymetli metal ve taşlardan yapılmış olabilir (altın-gümüş-pırlanta) ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Gülüşünüzün size özel ve süslü olmasını sağlarlar.
                                Diş yüzeyine özel diş bağlayıcılarıyla yapıştırılırlar.
                                Yapıştırılmaları sırasında dişten herhangi bir aşındırma yapılmaz ve eğer istenirse daha sonra diş yüzeyinden tamamen uzaklaştırılabilirler.`,
                                classname:"content-p",
                              }
                            ]
                          },
                        },
                      ],
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Deneme Başlığıdğr",
                        icerikContext:[
                          {
                            tag:"p",
                            icerik:"Deneme psidir.",
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Bu nu bilmiyorum psidir.",
                            classname:"content-p",
                          }
                        ]
                      },
                    },
                    {
                      id:13428286,
                      paths:'/rutin',
                      birimAdi:"TIBBİ BİRİMLER ",
                      adi:"Rutin Dental Tedaviler",
                      sidemenuVisible:true,
                      componenti:0,
                      subMenu:[
                        {
                          id:7848373,
                          paths:'/pedodonti',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Pedodonti",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Pedodonti - Çocuklarda diş tedavisi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Pedodonti - Çocuklarda diş tedavisi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Ağız ve diş sağlığı bebeklikten itibaren çok önemlidir. Her öğün beslenmenin ardından bebeğin yanak- dil- dişeti kretleri ve çevresi temiz gazlı bezle silinip temizlenmelidir. İlk süt dişlerinin sürmesiyle birlikte, bu temizlik işlemine dişler de eklenir. Aksi takdirde kalan yemek artıkları ağız içerisinde kötü kokuya, dişler üzerinde de çürüğe neden olur. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Çocuklarla tedavi her zaman yoğun ilgi ve sabır ister. İlk diş hekimi tecrübesi hayat boyu etkili olacağı için çocuk hasta ile karşılıklı uzlaşma, onu tedavi için doğru motive etme çok önemlidir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Süt dişleri, alttan gelecek daimi dişlere sürme rehberliği yaparlar. Değişim zamanı gelinceye kadar kendi pozisyonlarında, sağlıklı bir şekilde korunmaları gerekir. Çürümeye başlayan süt dişlerine dolgu veya kanal tedavisi gibi koruyucu tedaviler yapılarak, gerek süt dişi ve çevresindeki komşu dişler, gerekse sürecek olan daimi dişlerin ve ağız içi ortamın sağlıklı kalmasına çalışılır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Kliniğimizde her türlü pedodontik müdahale yapılabilmektedir:`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Oral hijyen eğitimi`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Süt ve daimi diş dolguları`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Koruyucu tedaviler(fissür sealant uygulaması, flor uygulamaları)`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Pedodontik kuron`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Yer tutucular`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Kanal tedavisi`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Çekim`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Ortodonti`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:56626,
                          paths:'/kanal',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Kanal Tedavisi",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Kanal Tedavisi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Kanal Tedavisi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Kanal tedavisi pulpa dokusu (dişin kan ve sinir içeren kısmı) kazayla, çürükle ya da enfeksiyonla açığa çıktığı zaman yapılan tedavidir. Özenli çalışmayı gerektiren, zaman alan bu tedavi 2-3 randevuyu kapsar. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Kanal ve köklerin sayısı kişiden kişiye ve dişten dişe değişiklik gösterir. Tedavi kök kanallarının içindeki pulpa dokusunun temizlenmesi, kök kanal duvarlarının genişletilmesi ve en son temiz, sağlıklı hale gelen kök kanallarının doldurulması şeklindedir.",
                                classname:"content-p",
                              }
                            ]
                          },
                        },
                        {
                          id:5848237,
                          paths:'/kuron',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Kuron Köprüler",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Kuron Köprüler",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Kuron Köprüler",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Metal destekli porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Değerli metal destekli porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Zirkon porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Tüm porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Diş kuronları çürümüş, kırılmış ya da çatlamış bir dişi onarmak için hazırlanırlar. Ayrıca dişlerin görünüşünü güzelleştirmek, ark içerisindeki pozisyonlarını düzeltmek için de kullanılırlar. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Köprüler dişlere yapıştırılan sabit apareylerdir.
                                Bir ya da daha fazla diş eksikliğinde hazırlanırlar.
                                Doğal dişlerle aynı görünüme ve fonksiyona sahiptirler. Dişlerin hizasını korudukları gibi iyi bir fonksiyon ve estetik sağlarlar.
                                Kayıp diş ya da dişlerin yanında yer alan komşu dişler (ya da implantlar) köprü ayağı olarak kullanılırlar.
                                `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Köprü bu komşu dişlerin üzerine yapıştırılmaktadır.
                                Köprünün kayıp dişleri içeren parçası ortada 'pontik' adı verilen kısımdır ve diş eti üzerine oturur.

                                Köprü ayağı olarak kullanılacak olan komşu dişlere ait diş etlerinin ve çevrelerindeki kemik dokusunun sağlığı köprü başarısında çok önemli bir faktördür.
                                Bu nedenle köprü yapımı öncesinde dokular dikkatlice incelenir, Kuronlar farklı materyallerle hazırlanabilir,
                                `,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Metal destekli porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Değerli metal destekli porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Zirkon porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Tam porselen kuronlar",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"METAL DESTEKL PORSELEN KURONLAR: ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Klasik tip kuronlardır.
                                Kuronun içinde kullanılan metaller kuronun daha güçlü olmasını sağlar. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"DEĞERLİ METAL DESTEKLİ PORSELEN KURONLAR: ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Kuron içinde farklı içerikli metaller kullanılarak kuron daha az allerjik ve diş etiyle daha uyumlu hale getirilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"TAM PORSELEN KURONLAR: ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Daha çok ön dişlerde, estetiğin yüksek oranda sağlanmasını istediğimiz durumlarda kullanılan metalsiz modern tekniktir. Yapıştırılmaları için özel resin siman sistemleri kullanılmaktadır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"ZİRKONYUM: ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`İyi estetik sonuç elde ederken aynı zamanda güçlülük ve dayanıklılık sağlamak için zirkonyum kullanılır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Zirkonun dayanıklılığı ve translusensisi doğal dişlerle çok fazla benzerlik gösterdiği için estetik diş materyali olarak tercih edilirler.

                                Diş hekimlerimizin dikkatli muayene ve incelemelerinden sonra hastaya kendisine uygulanabilecek tedavi seçenekleri anlatılır, bu tedavi seçenekleri tek tek ele alınıp tartışılarak hastaya en uygun tedavi planlanır.
                                `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Muayene sonrasında tedavi şu aşamaları izler: `,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Dişlerden aşındırma yapılarak kuron için gerekli yer hazırlanır `,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Ölçü alınır`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Geçici köprü/kuron hazırlanır ve yapıştırılır`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Ölçü,kron/ köprünün hazırlanması için diş teknisyenlerine yollanır`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Bir sonraki randevuda, geçici restorasyon çıkarıldıktan sonra laboratuarda hazırlanmış olan kuron/köprü ağız içinde prova edilir`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Kuron/ Köprü cilalanıp son bitirme işlemi yapılır.`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Doğru kapanış ve pozisyonda köprü dişlere yapıştırılır.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:92161,
                          paths:'/protez',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Hareketli Protezler",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Hareketli Protezler",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Hareketli Protezler",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik: `Hareketli protezler ağızdaki kayıp dişlerin yerine akrilik rezinden yapılan takıp çıkarılabilen apareylerdir. Ağızda kalan dişler köprü yapımı için yeterince sağlıklı değilse ya da uygun pozisyonda değilse, kayıp dişlerden oluşan boşlukları doldurmak için parsiyel protez yapımı tercih edilmektedir.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik: `Protezin metal isketli ya da metal iskeletsiz olması ağzın durumuna, dişlerin sayısına bağlıdır. Tamamen dişsiz hastalarda total protezler yapılmaktadır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik: `Dişlerin şekil ve renginin belirlenmesinde de akrilik dişlerin çok çeşitli renk ve şekil seçeneği olması sayesinde hasta için en uygun olanı tercih edilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik: `Eksik dişler yüzünden kaybedilen estetik görünümü, çiğneme fonksiyonunu, konuşma problemlerini tekrardan hastaya kazandırmasına rağmen hastaların harekeli protezleri kullanmaya alışmaları kolay değildir, zaman alıcıdır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik: `Diş hekimlerimiz tedavi planınız yapılırken, size uygulanabilecek tüm protez seçenekleri hakkında sizi bilgilendirecektir.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:591924,
                          paths:'/periodontal',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Peridontal Tedavi",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Peridontal Tedavi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Peridontal Tedavi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Sağlıklı dişlere ve sağlıklı bir ağıza sahip olmada ilk adım sağlıklı diş etleridir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Diş yüzeyinde oluşan, bakteri içeriği zengin plak tabakası tükrük içeriğiyle birleşerek sertleşir ve diş taşı haline gelir. Diş taşları dişlerin yüzeyinde yer alabileceği gibi diş etinin üstünde ve altında da birikebilir.
                                Sigara, çay, kahve vb. gıdalar diş ve diş taşı yüzeylerinde renklenmeye neden olur. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Dişeti altında biriken diş taşları dişetini kolaylıkla iltihaplı hale getirir. Diş taşları ve dişetlerindeki iltihap zamanla çevre kemik dokusunda yıkıma sebep olur. Bu durum dikkate alınmazsa dişler çevresindeki kemik desteğini kaybederek zamanla mobil hale gelir. Önlem alınmadığı takdirde bu yıkım dişlerin ağızdan düşmesiyle sonlanır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Plak içinde bulunan bakteriler sadece diş ve dişetleri için tehlikeli değildir, bunlar akciğer, kalp gibi yaşamsal organlar için de öldürücü olabilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`İyi bir ağız hijyeni ,düzgün ağız bakımı ve sağlıklı dişetleri, yapılacak olan diğer restorasyonların başarısı için ilk adımdır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Dişeti iltihabının tedavisi diş taşlarının uzaklaştırılmasıyla mümkündür.

                                Kliniğimizde uygulanan dişeti tedavilerinde, ultrasonik temizleyici cihazlar ve özel el aletleriyle,gerek diş yüzeyindeki plağı ,gerekse dişeti altındaki, diş taşlarını temizliyor ve lekelenmeleri ortadan kaldırıyoruz. Ayrıca çok derin diştaşları ve tortuları da derin küretajla temizliyoruz. Son olarak kullandığımız özel macunlarla diş yüzeylerini fırçalayarak diş yüzeylerinin parlak ve pürüzsüz hale gelmesini sağlıyoruz. Bu şekilde pürüzsüz hale gelen diş yüzeylerine plağın tekrardan tutunması da zorlaşmış oluyor.

                                Bu prosedür kanamasız, sağlıklı dişetlerine sahip olmanızı amaçlamaktadır ve dişetlerinin durumuna göre bazen birkaç seans sürebilmektedir. Bu tedavi her 6 ayda bir tekrarlanmalıdır.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:65281321,
                          paths:'/konservatif',
                          birimAdi:"TIBBİ BİRİMLER ",
                          adi:"Konservatif Tedavi",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Konservatif Tedavi",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Konservatif Tedavi",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Sağlıklı dişlere ve sağlıklı bir ağıza sahip olmada ilk adım sağlıklı diş etleridir. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Diş yüzeyinde oluşan, bakteri içeriği zengin plak tabakası tükrük içeriğiyle birleşerek sertleşir ve diş taşı haline gelir. Diş taşları dişlerin yüzeyinde yer alabileceği gibi diş etinin üstünde ve altında da birikebilir.
                                Sigara, çay, kahve vb. gıdalar diş ve diş taşı yüzeylerinde renklenmeye neden olur. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Dişeti altında biriken diş taşları dişetini kolaylıkla iltihaplı hale getirir. Diş taşları ve dişetlerindeki iltihap zamanla çevre kemik dokusunda yıkıma sebep olur. Bu durum dikkate alınmazsa dişler çevresindeki kemik desteğini kaybederek zamanla mobil hale gelir. Önlem alınmadığı takdirde bu yıkım dişlerin ağızdan düşmesiyle sonlanır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Plak içinde bulunan bakteriler sadece diş ve dişetleri için tehlikeli değildir, bunlar akciğer, kalp gibi yaşamsal organlar için de öldürücü olabilir. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`İyi bir ağız hijyeni ,düzgün ağız bakımı ve sağlıklı dişetleri, yapılacak olan diğer restorasyonların başarısı için ilk adımdır. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Dişeti iltihabının tedavisi diş taşlarının uzaklaştırılmasıyla mümkündür.

                                Kliniğimizde uygulanan dişeti tedavilerinde, ultrasonik temizleyici cihazlar ve özel el aletleriyle,gerek diş yüzeyindeki plağı ,gerekse dişeti altındaki, diş taşlarını temizliyor ve lekelenmeleri ortadan kaldırıyoruz. Ayrıca çok derin diştaşları ve tortuları da derin küretajla temizliyoruz. Son olarak kullandığımız özel macunlarla diş yüzeylerini fırçalayarak diş yüzeylerinin parlak ve pürüzsüz hale gelmesini sağlıyoruz. Bu şekilde pürüzsüz hale gelen diş yüzeylerine plağın tekrardan tutunması da zorlaşmış oluyor.

                                Bu prosedür kanamasız, sağlıklı dişetlerine sahip olmanızı amaçlamaktadır ve dişetlerinin durumuna göre bazen birkaç seans sürebilmektedir. Bu tedavi her 6 ayda bir tekrarlanmalıdır.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                      ],
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Deneme Başlığıdğr",
                        icerikContext:[
                          {
                            tag:"p",
                            icerik:"Deneme psidir.",
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Bu nu bilmiyorum psidir.",
                            classname:"content-p",
                          }
                        ]
                      },
                    },
                  ],
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Ağız ve Diş Sağlığı",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Diagnosis and treatment plan",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"All examinations and diagnoses are made in completely sterile environment using the computer technology",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"To check all the details we use a special intra-oral camera system",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"With our digital x-ray device, x-ray of your theeth can be taken in a short time with very comfortable conditions.",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"No fee is charged for non- emergency examinations",
                        classname:"content-p",
                      }
                    ]
                  },
                },
                {
                  id:136,
                  paths:'/uroloji',
                  birimAdi:"Medical Units ",
                  adi:"Urology",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Urology",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"The site's internal and surgical diseases in men and women is a branch of medicine that deals with urinary.",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Injections kidney stones",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kidney tumors",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Renal trauma",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Kidney stone,grout and trauma",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Bladder disorders",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Cystitis,cystitis bloody",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Diseases of the prostate",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Urine and the cysts related complications that occur that occur in the ways of",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Circumcision",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Gonorrhea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Viral diseases such as HPV",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Of the process our specialist doctors by diagnosis, treatment and post-operative checks are performed in our clinic.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:137,
                  paths:'/kardiyoloji',
                  birimAdi:"Medical Units ",
                  adi:"Cardiology",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Cardiology",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:`Science that studies the diseases of the heart and circulatory system. The Science of Cardiology diagnosis and treatment (therapy) that have worked to provide today's most important health problems among the diseases there are some diseases that are located between. A few of these diseases can be listed as follows`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Hypertension",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Atherosclerotic heart disease (such as coronary artery disease)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Heart rhythm disturbances (arrhythmias)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Congenital heart disease",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:`Nephrology, Endocrinology disciplines also of interest, such as high blood pressure, various diseases, developing heart failure, congenital or valvular heart disease induced by various diseases, and similar diseases are among the diseases for the diagnosis and treatment of Cardiology, working.
                        The use of Cardiology diagnostic tools include:`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ecocardiography",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Electrocardiography (ECG) and related diagnostic methods:",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Cardiac stress test ('stress test' or ''stress test ECG also known as')",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"A portable ECG device ('Holter monitor')",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"The levels of cardiac enzymes in the blood",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Coronary angiography",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Of the process our specialist doctors by diagnosis, treatment and post-operative checks are performed in our clinic.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:138,
                  paths:'/gast',
                  birimAdi:"Medical Units ",
                  adi:"Gastroenterology",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Gastroenterology",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Esophagus,stomach,small intestines,large intestines,liver,gall bladder, pancreas of the subject area in question is the Science of organs; these organs ulcer,gastritis,jaundice,hepatitis,irritable bowel(irritable bowel syndrome: IBS),gall bladder stones and infections, gastro-intestinal cancers hemorrhoids(chilblains,piles) of known diseases, such as searches for a solution. For diagnostic purposes, different types of endoscopes are used. Esophagus, stomach and duodenum to display esophagogastroduodenoscopy (short endoscope in general use), colonoscopy for colon is used.",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"An example of gastroenterolojik diseases clinical symptoms and signs.",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Constipation",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Diarrhea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Vomiting",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Nausea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Abdominal pain",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Continuous murmur burning sensation (burning sensation behind the sternum)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Bitter water in the mouth come",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Abdominal pain",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"By our expert doctor for diagnosis, treatment and post-operative checks are performed in our clinic.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:139,
                  paths:'/genel',
                  birimAdi:"Medical Units ",
                  adi:"General Surgery",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"General Surgery",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Besides systemic and surgical treatment methods in the body of local issues, general principles (wound healing, injury, metabolic and endocrine contains topics like the answer, and that affected many branches of surgery and basic medical terms in a technical development discipline. Generally, the esophagus, stomach, small intestine, colon, liver, pancreas, gallbladder and bile ducts, including a branch of a surgical specialty that focuses on abdominal contents.",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Today 'General Surgery' at the mention of:",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Thyroid surgery",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Breast surgery",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Esophagus",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Stomach",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Small and large intestines,",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Diseases of the anus (hemorrhoids...),",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Liver",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Pancreas",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Gall bladder and bile ducts,",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Hernia Surgery ",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"it is understood.",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Expert on surgical procedures by our doctors diagnosis, treatment and post-operative checks are performed in our clinic.",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:1991,
                  paths:'/deri',
                  birimAdi:"Medical Units ",
                  adi:"Skin Diseases",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Skin Diseases",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Common diseases of the skin. Disease too much skin. Dermatosis diseases of the skin generally, is the name given to the respective branches of Dermatology. Skin diseases can be split into several sections in order to get a general idea.",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Skin infections (fungal diseases, warts)",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Gland diseases (acne, dandruff on the scalp)",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Allergic skin diseases (atopic eczema, contact eczema [contact eczema],cosmetic allergies, insect bites)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Drug reactions",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Urticaria (hives)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Psoriasis (psoriasis)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Vitiligo",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Behçet's disease",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Skin tumors",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Burns",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Hair Disorders",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Nail diseases",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Excessive sweating",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Sexually Transmitted Diseases (Syphilis).",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:1992,
                  paths:'/dahiliye',
                  birimAdi:"Medical Units ",
                  adi:"Internal Medicine",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Internal Medicine",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Diagnosis and treatment of all internal diseases are performed in the internal medicine department. As sub-units of the Internal Medicine department;",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:1993,
                  paths:'/kbb',
                  birimAdi:"Medical Units ",
                  adi:"Otorhinolaryngology",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Otorhinolaryngology",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"In our otorhinolaryngology (ear, nose and throat) department endoscopic diagnostic methods used , audiometric and tymparometric tests  are performed  and diagnosis and treatment of the diseases that are listed below are carried out by our experienced doctor.",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Rips Tympanic Membrane",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"The ear canal and auricula diseaseas",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Draining middle ear diseases ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Loss of hearing",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Dizziness and balance disorders",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Sinusitis",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Allergic Rhinitis",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Difficulty in nasal breathing and nasal figure disorders",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Tonsil problems",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Examination of hoarseness and vocal records",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Adenoid Disorders ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Snoring and sleep  Apnea ( Cessation of breathing )",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"",
                        classname:"content-p",
                      },
                    ]
                  },
                },
              ],
              componenti:0
            },
            //130bitis
            //140
            {
              id:140,
              paths:'/unitelerimiz',
              adi:"Units",
              sidemenuVisible:true,
              exact:false,
              subMenu:[
                {
                  id:141,
                  paths:'/bla',
                  birimAdi:"Units ",
                  adi:"Radiology Services",
                  sidemenuVisible:true,
                  exact:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Radiology Services",
                    icerikContext:[
                        {
                            tag:"li",
                            icerik:"General radiology",
                            classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Computerized tomography",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Mammography",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Panoramic x-ray",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Digital fluoroscopy",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Magnetic resonance imaging",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Ultrasonography",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Color Doppler ultrasonography",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Angiography",
                          classname:"content-p",
                        },
                    ]
                  },
                },
                {
                  id:14846846,
                  paths:'/diloymgfkdjhs',
                  birimAdi:"Units ",
                  adi:"Laboratory Services",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Laboratory Services",
                    icerikContext:[
                      {
                        tag:"li",
                        icerik:"Clinical Biology",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:" Tumor markers(PSA,TSH, chorionic gonadotropin (HCG),AFP,CEA,CA19-9,mean ca125,CA15-3,CA72-4, etc.)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Hormone tests(TSH,LH,FSH,AC, TS,T3,T4,ADH,FT3,FT4,HOMO-IR, etc.)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Tests related to lipid metabolism (LDL cholesterol,HDL cholesterol,total cholesterol, etc.)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Microbiology",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Hematology",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Molecular Microbiology",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Allergy Tests",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:52661,
                  paths:'/derma',
                  birimAdi:"Units ",
                  adi:"Derma Cosmetics",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Derma Cosmetics",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:`Skin, hair and body care, used for using pharmaceutical techniques are developed, manufactured and dermatological skin products that have undergone clinical testing. Dermocosmetics sold in pharmacies and only by experts in dermatology or plastic surgery for the solution of various skin problems, either directly or as a support for drug treatment are prescribed. Most of the time, or may resolve to reduce the side effects of medication aimed at supporting the use. Example : acne treatment , psoriasis treatment. Dermocosmetic products are only sold in pharmacies however , pharmacies doesn't mean that every skin care product that is sold in mechanical damage. Although there are some skincare products sold in pharmacies "drugstore cosmetics" as defined and should be classified.`,
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:8372,
                  paths:'/dermalaz',
                  birimAdi:"Units ",
                  adi:"Dermatological Laser Applications",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Dermatological Laser Applications",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:`the capillary and varicose veins of the leg: excess weight, some vascular diseases, resulting in the leg veins due to prolonged standing swelling for years as superficial or structural-what are. Treatment are performed to the accompaniment of specialist physicians in our clinic.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:`the hair removal : is based on the destruction by burning of unwanted hair laser. Treated with the laser every skin type and every season.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:`the acne :, that arise at any age especially during adolescence more of the face acne-what. Can be of different size and intensity. Dermatology some acne patients can be treated with laser, which is determined by the doctor.`,
                        classname:"content-p",
                      }
                    ]
                  },
                },
                {
                  id:969,
                  paths:'/dishiz',
                  birimAdi:"TIBBİ BİRİMLER ",
                  adi:"Diş Hizmetleri",
                  sidemenuVisible:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Diloy Diloy",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Diloy Diloy",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Bu nu bilmiyorum psidir.",
                        classname:"content-p",
                      }
                    ]
                  },
                },
                {
                  id:9529,
                  paths:'/lazer',
                  birimAdi:"Units ",
                  adi:"Laser Eplition and Regionel Slimming",
                  sidemenuVisible:true,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Laser Eplition and Regionel Slimming",
                    icerikContext:[
                      {
                        tag:"p",
                        icerik:"Laser Epilation",
                        classname:"content-p-big",
                      },
                      {
                        tag:"p",
                        icerik:`Laser epilation mus be done under the consultancy and control of a dermatologist in order not to have any side effects and negative impacts.It is very important to have hair removal process in health institutions authorized by the health ministry under the contol of a health care specialist or a doctor with laser epilation devices in compliance with international norms (U.S FDA approval, European CE certificate ). Keeping a head of the scientific data , your dermatologist will offer the most suitable laser epilation for you.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"How can laser epilation be effective?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`Laser epilation focuses on the thickness and color of the hair. Laser is absorbed by hair follicles because of its color. Due to high energy, the roots heats and lights. In this way, roots are poured after a successful laser epilation and in the growth phrase gets damaged and become unable to produce hair.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Is laser epilation successful?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`To achieve a succuessful result, the appropriate laser type and dose suitable for the skin color and hair structure must be selected in laser removable applications.Sessions repeated at regular intervals, makes the outcome quicker and more successful. Laser epilation is a treatment approved by IDA and considered as a complete success if the region the treatment applied has 80% hair removal.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Is laser epilation safe?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`Laser epilation is a technology safely used in medicine for about 30-40 years. It has been widely used last 10-15 years.Also, it has no effect on corcinogenicity. Dermatologist’s assessment is a necessity.Laser must not be applied on the risky moles and dark spots. In our medical center; alexandrite and ND-YAG laser epilation devices that are approved by FDA (U.S food and drug approval agency ) and CE are used. `,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"How many sessions are necessary for effective treatment?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`The hairs in our body has growth(anagen), pause (catagen) and shedding(telogen) phrases. Approximately 30-70% of them are in shedding(telojen) phrase. The most light-sensitive phrase of the hair is anagen stage. As there is no melanin production, the hairs are not effected from the light in other stages. For this reason, the laser hair removal application must be repeated at regular intervals in order to catch the hairs in anagen stage. The number of sessions in laser epilation application depends on the region of the body, skin color, hair color, thinness-thickness of the hair and hormonal factors. Average 4-10 sessions is essential for a permanent solution in laser epilation.
                        In general, in men, the hairs in all regions of the body and in women the hairs in face, the laser epilation sessions last long-term and the hairs become thinner and remain on the body.The success of laser epilation increases when skin color is lighter and the hair color is darker. Genitals, armpits and legs are areas best results received in laser epilation. Depending on all these factors, the success rate is 50-90%. Hormanal disorders may effect the number of sessions. Your doctor will inform you after making necessary examinations and evaluations. `,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"How session intervals determined in laser epilation?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`In laser epilation sessions, the spaces given vary from 4-8 weeks depending on the region. Laser epilation session intervals vary according to the the shedding and coming out of hairs.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Does laser epilation have any side-effects? ",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`When applying laser epilation, the pain perception varies from person to person. This pain is usually bearable and defined as rubber shock and as the feeling of touching the needle.

                        After the laser epilation session, redness and swelling which last a few minutes may occur but these side effects are temproray and do not affect patients’ daily activities. In laser epilation application, crusting on the skin surface is sometimes a temporary problem that can occur on the applied skin. The crust, usually resolves within days and the skin returns to normal. In rare cases, discoloration and darkening can be observed in the skin. In almost all cases, skin returns to normal within 6-12 months. Skin thickining called as scar tissue is a rare side effect that can be permanent. If the application of laser epilation is done under the supervision of expert dermatologists and if the right type and dose are selected; no side effects will be seen.
                        `,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Can laser epilation be applied in summer?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"p",
                        icerik:`Treatment can be applied in summer. However, the patients should carefully apply the sun protection methods recommended by the physician. In addition, no application must be made to bronze skin and no sun bath must be taken after the application. When application is implemented to bronze skin, both several side effects appear and the success rate decreases. Tanned individuals should wait at least 1 month to start laser epilation. `,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"What should be considered prior to laser epilation?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:`One month before the application of laser hair removal ; operations like epilator, waxing, tweezers should not be applied. The hair would be sufficient for prolongation of 2-3 mm when shaved three days before the application. Prior to treatment, yellowing and staining should not be made.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`Test shot can be applied to meet laser and determine dose before the application. After the test, most suitable wavelength is detected and the right therapy is applied.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`For the completion of the ripening stages of hair formation and for the body parts average 6-12 sessions of laser hair application may be needed.The implementation of sessions at more frequent and large intervals has a negative effect on the healing process as it breakes the cycle of hair..`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`Skin peeling and treatments such as acne, blemishes drugs should be left up to 15 days before laser epilation session.`,
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"What should be considered after laser epilation?",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:`No sunbathe and solarium for two days after the laser epilation application. Sunscreen with SPF 3D factor should be used for at least 2 weeks with doctor's prescription. Especially, sunscreen on face should be applied.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`Most of the hairs on the skin are removed by laser epilation. The roots start to elongate and begin to fall between 3-10 days.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`For an hour or a few days redness may occur on the laser applied skin due to the person's skin sensivity.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`A warm shower can be taken immediately after laser epilation. Applications irritating the skin such as peeling should not be applied for one week.`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`The hair density is reduced to 15-20% in each session, you can see the difference after the 3rd session. To continue your sessions regularly will let you get maximum efficiency from your laser epilation `,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`2 weeks after each session, a control session is free of charge for large regions (arm-leg-back-chest-genital). (Controls made after 20 days will negatively affect the course of therapy).`,
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:`No control session is needed for small regions as it does not contribute to the course of treatment.`,
                        classname:"content-p",
                      },
                    ]
                  },
                  subMenu:[
                    {
                        id:142,
                        paths:'/lazerkilcal',
                        birimAdi:"Units ",
                        adi:"Capillary Treatment with Laser",
                        exact:false,
                        sidemenuVisible:true,
                        componenti:0,
                        icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Capillary Treatment with Laser",
                            icerikContext:[
                                {
                                    tag:"p",
                                    icerik:"Capillary Treatment with Laser",
                                    classname:"content-p-big",
                                },
                                {
                                    tag:"p",
                                    icerik:`Genetic predisposition, prolonged standing, pregnancy periods, wearing high-heeled shoes and excessive weight gain cause extension in capillaries in the legs. The sun exposure, genetic predisposition, thin and sensitive skin type cause red-purple capillary in the face. Large number of capillaries may cause pain especially in the legs. Many people have a lot of cosmetic problem because of capilarries.
                                    Red-purple thin veins that appear in the face or in anywhere on the body can be treated and destroyed with 1-6 sessions.The best results are taken by Nd-YAG laser system in all around the world. `,
                                    classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"1. How many sessions should be done for he treatment of capillary vessels with laser?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"According to the width and depth, prevalence of cappillaries, can either be destroyed with one session or six sessions may be required.",
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"2. What should be the session intervals? ",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"Sessions are made 4-8 weeks intervals.",
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"3. Which veins are suitable for laser treatment?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"Blue-purple, maximum 4 mm wide veins in the face and legs give the best results in laser treatment.",
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"4. What to do before and after the treatment?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"Sun protection is essential. Should be done during the winter period. Temporary redness, mild swelling or color changes may occur after the treatmant.",
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"5. Can treated veins may occur again?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:"Generally, treated veins will disappear completely irreversible. However if there are situations such as an underlying venous insufficiency, the formation of varicose veins ; reformation of capillaries may be observed in other areas.",
                                  classname:"content-p",
                                },
                            ]
                        },
                    },
                    {
                        id:1461515,
                        paths:'/botoks',
                        birimAdi:"Units ",
                        adi:"Botox Treatment",
                        sidemenuVisible:true,
                        exact:false,
                        componenti:0,
                        icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Botox Treatment",
                            icerikContext:[
                              {
                                  tag:"p",
                                  icerik:"Botox Treatment",
                                  classname:"content-p-big",
                              },
                              {
                                  tag:"p",
                                  icerik:`Botox is a highly successful method used for the treatment of facial lines and wrinkles, the prevention of new wrinkles that may occur (antiaging). `,
                                  classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Which wrinkles treatment Botox is used?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"In Botox treatment, wrinkles in the forehead, around the eyes and between the eye brows give the best results. Botox is also used for to treat laugh lines and neck wrinkles. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"How do wrinkles form?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Some of the wrinkles occur due to the facial movements and contraction of muscles under the skin. Relaxation of these muscles reduce wrinkles and cause extinction. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"What is the affect of Botox?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Botox is a medicine containing botulium toxin A. Botilinum is a neuromuscular blocking agent, when injected into muscle that causes muscle paralysis, blocking the release of acetylcholine to the communication gap between nevre and muscle. When the muscle not warned by the nevre is paralyzed. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Is Botox used to treat other diseases in dermatology?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Yes. Botox is a a successful method used for the treatment of localized (underarms, palms and soles) excessive sweating. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"How Botox treatment is made?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"The process is simple and can be applied in inspection. It does not require anesthesia.Botox is a painless application and does not leave any scars upon injection. After reconstitution, a very small amount of Botox is injected with fine needles just under the skin to the muscle. After the application, no change will be at the applied body region and the person can immediately return to normal daily activities. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"When Botox starts and how long it takes effect?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Botox' s effect is seen in 2-7 days. This effect will continue 3-6 months then new injections are required. ",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"What should be considered after the application?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"The applied body region intact for 2-3 hours and adhered in an upright position.",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"The applied body region intact for 2-3 hours and adhered in an upright position.",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Bath can be made, make up should not be made.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"What are the side effects of Botox?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"As the toxin injected to a very small amount, treatment is safe with minimum side effects. Slight burning occurs in half of the patients during the injection. There may be 1% decrease in eyelid and eyebrow. Declines in 4 weeks.",
                                classname:"content-p",
                              },
                          ]
                        },
                    },
                    {
                      id:684936858,
                      paths:'/dolgu',
                      birimAdi:"Units ",
                      adi:"Filling Applications",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Filling Applications",
                        icerikContext:[
                          {
                              tag:"p",
                              icerik:"Filling Applications",
                              classname:"content-p-big",
                          },
                          {
                              tag:"p",
                              icerik:`What causes wrinkles and lines?`,
                              classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Hyaluronic acid content of the skin decreases with aging and decreases the skin’s water-holding capacity. Collagen and elastic fibers in the skin begins to break and tear. These breaks are a part of natural aging process, but more frowning, smiling and other facial expressions contribute to the formation of lines on the face. Smoking cigarette and environmental factors such as polluted air, also accelarates the aging process.The most frequently used process to fill facial wrinkles is facial filler treatment `,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"What is hyalunoric acid (Restylane, teosyal and juvederm etc.)?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Hyaluronic acid is a clear crystal looking gel substance that is produced under laboratory conditions and does not contain any animal material. (Filler materail between the cells in our skin). When injected into the skin combines with the body’s own hyaluronic acid and creates volume. Has a towing capacity up to 1000 times of its volume. Plumpled lips, filled lines and wrinkles and treated facial folds are provided with this volume. It is a quick and easy implementation and face filling process allows to obtain visible results immediately. `,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"What should the expectations be after the filler treatment?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Hyaluronic acid provides a natural look after injection. You can have your facial filling and lip filling procedures done at lunch break and continue to your daily life. You do not need to have any previous test. However, it is important to have it applied by experienced specialists. You can immediately return back to your daily activities after filler injections. Effects will continue about 6-12 months according to the material used.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Does filler treatment have any side effects?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Hyaluronic acid has been confidently applied to more than 2 million people and in 70 countries around the world since 1996. Rarely, a slight swelling in the treated area might be seen for a few hours. Allergic reactions can rarely be seen. Redness, itching or stiffness may occur at the injection site. A face filling treatment with Hyaluronic acid is usually applied as an alternative in aesthetic surgery. Hyaluronic acid is a safe application that does not require any testing before treatment. `,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Is the filler treatment painful?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Anesthesia , lasting 45 minutes with a local anesthetic cream is good enough. Facial filler is a treatment that can easily be tolerated. As lip is a region rich in nerve density,in filling process,anesthesia applied by dentists can be preffered. `,
                            classname:"content-p",
                          },
                        ]
                      },
                    },
                    {
                      id:5286727,
                      paths:'/bölgesel',
                      birimAdi:"ÜNİTELERİMİZ ",
                      adi:"Regional Slimming",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                          icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                          icerikHeader:"Regional Slimming",
                          icerikContext:[
                              {
                                  tag:"p",
                                  icerik:"Deneme psidir.",
                                  classname:"content-p",
                              },
                              {
                                  tag:"p",
                                  icerik:"Bu nu bilmiyorum psidir.",
                                  classname:"content-p",
                              }
                          ]
                      },
                      subMenu:[
                        {
                          id:143,
                          paths:'/kavitasyon',
                          birimAdi:"Units ",
                          adi:"Ultracavitation with Max Power",
                          sidemenuVisible:true,
                          exact:false,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Ultracavitation with Max Power",
                            icerikContext:[
                                {
                                    tag:"p",
                                    icerik:"Ultracavitation with Max Power",
                                    classname:"content-p-small",
                                },
                                {
                                    tag:"p",
                                    icerik:`Ultracavitation method gave visible results in reducing cellulite and regional thinning and greeted with interest.
                                    Especially, this application is very suitable for people who have long working hours and time problem. It works by dissolving the fatty tissue that is accumulated in the body by the help of strengthened ultrasound and forming the body.Recovary time and comfort of the transaction increases as it is not a surgical operation. `,
                                    classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"What is ultracavitation system? How does it work?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`Ultracavitation system is a non-surgical method fighting with regional adiposity and cellulite with the help of ulrtasound.Spread of new ultrasound applied to the outer surface of the skin causes a sudden, high pressure changes in the cell fluid of adipose tissue.Then, the expansion of the foaming creates the explosion.This effect called cavitation ; destroys the walls of the fat cells by fat liquefaction and destroys the structure of the body fat depot.In this tissue, fat cells and fatty acids that are released break down and tried to schism from these places by lymphatic pathways. The fats released excreted from the body either by burning in the muscles or through urinary system or liver. We are using this technology with lipotomi hipoosmalar method and getting cost effective results. The true recommended action mechanism that is scientifically proven is obtained by this method.`,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"What is the purpose of its use?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`It is a powerful system used in reducing cellulite, melting and getting rid of the primarily located regional fat. `,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"Does the application give any pain?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`Ultracavitation method is a safe and painless ultrasound application. During and after the application a light and sensible heat can be felt for several hours. `,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"Can be used with two methods.",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"li",
                                  icerik:`1. Wet method: Using liquid injection, the fat cells are inflated. Thus, liquefaction and blasting are carried out more effectively and this is called hipoosmolor lipotomi.`,
                                  classname:"content-p",
                                },
                                {
                                  tag:"li",
                                  icerik:`2. Dry method: It is applied directly to that area without giving liquid, results are obtained after 6-8 sessions. It is applied 1-2 days a week.This method is recommanded for those who fear the needle and can be supported by other systems.`,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"Can these also be applied to men?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`There is no discrimination in this system. It is applicable to both men and women.`,
                                  classname:"content-p",
                                },
                                {
                                  tag:"p",
                                  icerik:"What is the time period to see the results and what should be done meantime?",
                                  classname:"content-p-small",
                                },
                                {
                                  tag:"p",
                                  icerik:`Although a special diet and sports supplementation is not required, to eat more after the application will add new fats to the ones lost in ultracavitation. After the first session, a palpable softening and then downsizing in the faty region can be felt.Visible results can be obtained after 15 days.`,
                                  classname:"content-p",
                                },
                            ]
                          },
                        },
                        {
                          id:190,
                          paths:'/radyofrekans',
                          birimAdi:"Units ",
                          adi:"Radiofrequency-Slimming with Velashape",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Radiofrequency-Slimming with Velashape",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Radiofrequency-Slimming with Velashape",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Majority of women over the age of twenties has cellulite problems regardless of their body shape and weight.Nowadays, women prefer more effective and long-term solutions than limited traditional and expensive medical solutions.The common dream of all women in the world is to have a nice and smooth body… With the development of technology, various devices have been developed in order to eliminate accumulated fat in certain areas and to get rid of cellulite without any surgical intervantion. VelaShape is the only well -developed device that reduce and even manage to destroy cellulite that has proven its success in the aesthetic world in the clinical settings. Velshape received “ the first medical device to treat cellulitis effectively “ approval from U.S Food and Drug Administration FDA. VelaShape can also be easly applied to patients with capillary vascular disease.New Radio Frequency slimming treatment is like a warm massage that most patients enjoy. During or after the application, no special socks or clothes are needed.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Which regions can velaShape be applied ?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:"Legs and butt",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"The upper arm",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Waist and side region",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Abdominal region",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Face and neck",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"How often VelaShape is applied ?, After how many sessions can a result be obtained ? ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`For 2 times a week, 8-16 sessions are recommanded according to the degree of cellulite. 10 sessions is sufficient for patients that have cellulite with a degree one or two. In fact, even after a few sessions, there will be a noticeable difference. Sessions can continue once a month in order to ensure the same results after cellulite treatment. Each session lasts about 45-60 minutes. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"What is felt during the procedure?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`After VelaShape cellulite and regional thinning treatment, due to the heating of the skin, redness occurs on the skin surface. You can start your daily activities after the application and return back to your work. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Is cellulite and regional slimming treatment safe with VelaShape? ",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Regardless of color or skin type, it can safely be applied to all individuals over the age of 18.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"How can I start VelaShape treatment?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`The first step is to call our clinic to get an appointment for free consultation. After you decide to start treatment, inspection is made by our experienced doctor. Once you decide the VelaShape method is safe for you, according to the intensity of regional adiposity and the degree of cellulite or grievances, the number of sessions determined for you to get the most effective results. If necessary, it can be combined with other treatments such as mesotherapy.`,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                        {
                          id:959919,
                          paths:'/powerplate',
                          birimAdi:"Units ",
                          adi:"Powerplate",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Powerplate",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"What is power plate?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Due to gravity, we all have weight defined in kilograms.By acting against the gravity, the muscles in the humanbody develops and strengthens. In this world widely used technique, the vibration produced by the power plate is transmitted to the humanbody as an energy. During the practice, the muscles continuously stretch, relax and run. During the application period, all the muscles in the body run in a dicipline. With 40 Hz. release rate choicen from the power plate , our muscles relax and strech 40 times per second. After work is completed, the body starts to relax and the body metabolism adjusts itself .The body’s performance line was taken up with this adjustment. This initiative; increasing the body’s performance is called “supercompensatiom”. `,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"10 minute Power Plate study is equvailent to approximately 1 hour Fitness study…",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Adds flexibility,",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Allows to run all the muscles in the body,",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Minimize the risk of joint injuries,",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Increases the oxygenation and blood flow,",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Allows the hormones to run at a higher level,",
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Increases body water drainage,",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:`Reduces the effect of osteoporosis,`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Reduces back and low back pain and strengthens these areas,",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:`Prevents formation of cellulite, reduce cellulite`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Increases bone density,",
                                classname:"content-p-small",
                              },
                              {
                                tag:"li",
                                icerik:`Increases collogen production,`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:"Reduces mental and physical stress,",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Stay young with just 10 minutes of Power Plate study...`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Fields where Power Plate is applied: `,
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Physical Therapy: Vibration application strengthens the muscles, muscle tissue, bone structure and the bonds of the joints and reduces the risk of joint injury.`,
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Sports: In general, when applied with the muscle strengthening fitness programs, the muscles strengthens in a shorter time.Similar effects can be seen when used alone. By only 10 minutes application a day, all the muscles in a body strengthens.`,
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Fitness: As it can easly be adapted to traditional studies like fitness( stretching/relaxation), can also be used as healing and cooling programs in these studies.`,
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Rehabilitation: Eliminates muscle weakness(antrophy) and slack muscles (hypotonia), reduces pain, fixes perception problems(proprioceptive disturbances). All parts of the body relaxes. `,
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Medical: Improves blood circulation, contributes to correction of disorders of bone(osteoporosis) resorption.As the heart rate remains constant during the operation, it does not impose additional burden to the body.`,
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Cosmetics: Prevents the accumulation of fluid in the body, helps to drain accumulated fluids.Accelerates the degradation of fat free, develops muscle structure and resolves regional cellulite, helps the body to have a tight tissue and prevents the formation of cellulite. Increases the firmness of tissues by adjusting the balance of tonic in the body.`,
                                classname:"content-p-small",
                              },
                            ]
                          },
                        },
                        {
                          id:378235893,
                          paths:'/enjeksiyon',
                          birimAdi:"Units ",
                          adi:"Injection Lipolysis",
                          sidemenuVisible:true,
                          componenti:0,
                          icerikveri:{
                            icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                            icerikHeader:"Injection Lipolysis",
                            icerikContext:[
                              {
                                tag:"p",
                                icerik:"Injection Lipolysis",
                                classname:"content-p-big",
                              },
                              {
                                tag:"p",
                                icerik:"What is Lipolysis?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Fat problems of the certain regions in the body that can not be solved either with diet or fitness can now be solved with lypolysis injection method that is used in Europe and America. Is the direct injection of some substances to the subscutaneous fat tissue that accelerate fat burning and melting. Many people have accumulation of excess fat in certain areas of the body although they are not overweight. Leg, hip, abdomen, sides of the waist in women, abdomen and waist in men are the places of the body that are most prone to fat accumulation. Its effects increase approximetaly threefold when used in conjunction with cavitation and radiofrequency in regional thinning programs.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Which materials are used?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`One of the most popular slimming method which is called lypolsis; an active substance named phosphatolyl choline is used. The substance called lecithin is vital for people. It is found in all cells and plays a role in vital activities. It has been considered to be used in the treatment of excess fat settled in the unwanted regions of the body. With fat burning accelerating agent, L-carnitine can be used.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"How is treatment made?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`This method is used eliminating all kinds of glands.This theraphy which gives more successful results in women then in men; is applied by giving drug injection to the excess areas of the body with thin needles`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"How many sessions are required?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Depending on density of suboutaneous fat mass ,average 3-6 sessions are applied.The patient can be treated in 3 sessions as the fats can be solved easily. The frequency and the number of sessions can vary according to the amount of the drug given.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Is the application painful?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`During the application pain is not unbearable. There might be edema and tenderness for afew days. Approximately, in 1-2 weeks healing bruises may occur. People can return back to their daily activities after the application`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"What are the side effects?",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`When performed by experienced and trained specialists, any adverse side effects were seen. `,
                                classname:"content-p",
                              },
                            ]
                          },
                        },
                      ],
                    },
                    {
                      id:377392,
                      paths:'/medikal',
                      birimAdi:"Units ",
                      adi:"Medical skin care",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                          icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                          icerikHeader:"Medical skin care",
                          icerikContext:[
                              {
                                  tag:"p",
                                  icerik:"Medical skin care",
                                  classname:"content-p-big",
                              },
                              {
                                  tag:"p",
                                  icerik:"Special acne care for oily skin",
                                  classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"Excessive secretion of sebum on the skin leads to an imbalance and shine on the skin.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Lifting care",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"It is an innovative method for your skin to look younger, to reduce fat cells and to shape facial contours.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Needleless botox treatment",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:"A non-surgical effective care used in preventing and reducing wrinkles.",
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Caviar Care",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Care fighting against premature aging.
                                Provides intensive renovation both for lifeless and mature skins.
                                It is possible to get rid of your stains by highly effective treatments.`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Chemical Peeling",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Skin renewal slows down with the age.
                                Chemical peeling treatment helps the young and healthy skin to come to the surface and removes the problematic cells from the skin.
                                Chemical peeling treatment is used to solve the following problems;`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:"Acne(pimples)treatment",
                                classname:"content-p-small",
                              },
                              {
                                tag:"p",
                                icerik:`Treatment of scars left after acne`,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:`Elimination of thin lines and wrinkles`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Dry and lifeless skin treatment`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Closing of open pores in the skin`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`Removing unwanted color changes (aging, sun, pregnancy and drug strains)`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:`To delay the effects of aging and rejuvenate the skin, 6 sessions of peeling treatment is applied to each healthy skin.`,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:``,
                                classname:"content-p",
                              },
                              {
                                tag:"li",
                                icerik:``,
                                classname:"content-p",
                              },
                              {
                                tag:"p",
                                icerik:``,
                                classname:"content-p",
                              },
                          ]
                      },
                    },
                    {
                      id:8948363,
                      paths:'/prp',
                      birimAdi:"Units ",
                      adi:"Prp",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                        icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                        icerikHeader:"Prp",
                        icerikContext:[
                          {
                              tag:"p",
                              icerik:"PRP (Platelet Rich Plasma) Treatment",
                              classname:"content-p-big",
                          },
                          {
                              tag:"p",
                              icerik:"What is PRP?",
                              classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`"PRP" is the platelet rich plasma treatment of the cells called platelets in the blood.A small amount of blood is taken from a person and seperated into its components with a special process . As a result of this, a small amount of platelet -rich plasma is obtained and given back to the same person through the process of injection for skin rejuvenation.PRP is also used in the heart surgery in order to reduce the risk of the infection, in tennis elbow in order to accelerate the healing process and for disorders such as tendon injury.At the same time, it is performed by dentists in implant treatment.
                            PRP is injected in the body to help to renew the body tissues by inducing and activating the stem cells.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"What is the purpose in PRP application?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`When any injury occurs in our tissues, repairing process begins through platelets.In PRP application, a large amount of circulating blood platelets are carried out to the target area with a content of growth factors.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"How is skin rejenuvation made by PRP?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`The aging of our skin is caused by the loss of some physical properties. Dermo cosmetic products starts the healing process through materials restructuring our skin and synthetically derived growth factors. When concentrated platelets within the plasma, injected in skin, growth factors stimulate collagen production and the formation of the new capillaries and provide skin renewal.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"How to apply skin resurfacing procedure with PRP? ",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`In order to obtain platelet platelet rich plasma a special fitter and centrifuge used.Approximately 97% of platelets break down and become 6-10 times bigger than the normal density in the plasma. PRP treatment starts with making a blood test to the patient.With the help of a special fitler and centrifuge, the platelet-rich plasma containing autologous white blood cells are prepared.Finally, autologous platelet-rich plasma (PRP) with white blood cells is injected into the skin. It provides realease of growth factors with platelets and white blood cells in the injection area.Growth factors provide removal of skin problems and accelerates skin repair.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"How long the PRP application last?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`It lasts approximetly 30 minutes.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:"Can PRP and skin resurfacing be applied other than injection?",
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`Yes , can be prepared and applied as mask and cream.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`In what circumstances, skin regeneration with PRP is effective? `,
                            classname:"content-p-small",
                          },
                          {
                            tag:"li",
                            icerik:`Estetik amaçlı uygulamalarda yüz, boyun, dekolte, eller, bacak içleri, kollar gibi vücut bölgelerine gençleştirme için`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`For aesthetic applications, for the rejuvenation of the body parts such as face, neck, decollete, hands, iner legs and arms.`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`To ensure the rapid construction of the skin, immediately after the applications such as laser and peeling.`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`Correction of wrinkles in the skin resulting from exposure to ultraviolet rays for many years and to help eliminate depressions.`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`To gain a sustainable flexibility and brightness.`,
                            classname:"content-p",
                          },
                          {
                            tag:"li",
                            icerik:`In the treatment of hair loss, can be used alone or in combination with other treatments.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`Is PRP a reliable skin rejuvenation method?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`The blood taken from the patient is used with a sterile and closed kit, therefore PRP is a reliable application.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`When does PRP's effect occur? `,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`A healty glow occurs on the skin immediately after the application. A permanent and significant effect can be seen after 3-4 applications.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`Has skin rejuvenation with PRP got a lasting impact?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`After 3-4 applications, if repeated every 10-12 months- the impact of cures will be equivalent to a permanent rejuvenating effect.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`What are the advantages of PRP method?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`The positive results of other methods will continue for some time, but the positive results of PRP, belongs to the person applied and not lost.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`Is skin regeneration procedure with PRP painful?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`No serious pain is felt except a slight discomfort. Anesthetic creams can be applied before the application.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`To whom PRP not be applied?`,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`To cancer patients and patients that have insufficient number of platalet.`,
                            classname:"content-p",
                          },
                          {
                            tag:"p",
                            icerik:`Does PRP mean skin rejuvenation with stem cells? `,
                            classname:"content-p-small",
                          },
                          {
                            tag:"p",
                            icerik:`PRP is injected to the stem cells to stimulate and enable the cells to be active.There is no stem cell in the serum content but there are many platelets and white blood cells. PRP has effect on the re-construction of regenerative stem sells.`,
                            classname:"content-p",
                          },
                        ]
                      },
                    },
                    {
                      id:25251513312,
                      paths:'/mezoterapi',
                      birimAdi:"Units ",
                      adi:"Mesotherapy",
                      sidemenuVisible:true,
                      exact:false,
                      componenti:0,
                      icerikveri:{
                          icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                          icerikHeader:"Mesotherapy",
                          icerikContext:[
                            {
                                tag:"p",
                                icerik:"Mesotherapy",
                                classname:"content-p-big",
                            },
                            {
                                tag:"p",
                                icerik:"What is mesotherapy?",
                                classname:"content-p-small",
                            },
                            {
                              tag:"p",
                              icerik:`Mesotherapy is a treatment method which originated from Europe and applied in the form of injections of locally prepared specific drug combinations to the skin for the treatment of medical and cosmetic skin problems.Mesotherapy is first described by French physician Michel Pistor in 1952. An article about treatment technique was published by a local magazine in 1958 .Mesotherapy exhibition was organized in 1964. The French National Academy officially adopted mesotherapy as a branch of medicine in 1987. For nearly 50 years, more than 15 000 applications all around the world were made by mesotheraphy doctors. In mesotherapy, the aim is to make injections with very small drug doses to the problematic areas of the body.Injection is made to the middle layer of the skin which is called mesoderm. `,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`In which situations is Mesotherapy applied?`,
                              classname:"content-p-small",
                            },
                            {
                              tag:"li",
                              icerik:`Reoginal thinning`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Cellulite treatment`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Face, neck and decollete resurfacing`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Hair loss`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Acne scar treatment`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Pregnancy stretch marks`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:"Which medications are used in mesotherapy?",
                              classname:"content-p-small",
                            },
                            {
                              tag:"p",
                              icerik:`In classical mesotherapy, special mixtures are prepared by using herbal remedies, vitamins and minerals in accordance nwith the problem of the person.`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`In which situations mesotheraphy is not applied?`,
                              classname:"content-p-small",
                            },
                            {
                              tag:"li",
                              icerik:`Severe heart disease`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Kidney diseases`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Diabetes`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Blood thinning drug abusers`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Pregnant and breast-feeding women`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Those allergic to the drugs used`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Cancer patients`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`How is mesotherapy applied in regional slimming and cellulite treatment? In certain parts of the body, regional weight may occur because of genetic and structural reasons, some hormonal diseases, weight gained after pregnancy, life style away from physical activities . Weight is formed mostly in abdominal circumference and basin disrict.
                              In mesotherapy, body slimming is provided by injecting a special blend of medications prepared for problematic regions.In regional slimming oil degrading drugs and in cellulite treatment ,medications that increase blood flow are applied.Mesotherapy is applied , 1-2 sessions for regional slimming.8-12 sessions is in total.It is not applied to people with extreme overweight. `,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`In which situations mesotherapy is applied for renewal of the face(mezolift)? Mesotherapy applied to face is called " mezolift". Mezolift applied in the following cases:`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Resurfacing- rejuvenation`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Wrinkles treatment`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Acne scar treatment`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Loss of flexibality in face`,
                              classname:"content-p",
                            },
                            {
                              tag:"li",
                              icerik:`Neck, decolletage and hand care`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`Which medicals are used for mezolift?`,
                              classname:"content-p-small",
                            },
                            {
                              tag:"p",
                              icerik:`Skin condition is determined according to the age of the patient. Special medications that support the texture of the skin and vitamin mixtures are applied to neck, face and decollete and hands.`,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`How mezolift sessions are arranged?`,
                              classname:"content-p-small",
                            },
                            {
                              tag:"p",
                              icerik:`The sessions are carried out every 7-10 days depending on the skin. Sessions are 15-20 minutes. After the application, the patients can return back to their daily activities. `,
                              classname:"content-p",
                            },
                            {
                              tag:"p",
                              icerik:`How is mesotherapy applied in hair loss?`,
                              classname:"content-p-small",
                            },
                            {
                              tag:"p",
                              icerik:`Mesotherapy applied to both men and women that have hair loss problems.
                              7-10 days, a total of 6-12 sessions mesotheraphy is applied for hair loss. Specially prepared mixture of drugs for hair injected to the scalp. At first, shedding is reduced and and hair out can be seen in the following sessions.`,
                              classname:"content-p",
                            },
                          ]
                      },
                    },
                  ],
                },
              ],
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Ünitelerimiz",
                icerikContext:[
                  {
                    tag:"li",
                    icerik:"Ünitelerimiz baby",
                    classname:"content-li",
                  }
                ]
              },
            },
            //140bitis
            {
              id:170,
              paths:'/doktorlarimiz',
              adi:"Doctors",
              sidemenuVisible:true,
              exact:false,
              componenti:2,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Doctors",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Deneme psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  }
                ],
                tab:{
                  tablist:[
                    {
                      id:0,
                      name:"Erdal ARAS",
                      title:"Uzm. Dr.",
                      bolum:"Radyoloji Hizmetleri",
                      tarih:"07.10.1945",
                      birthlocation:"Çorum",
                      lang:[
                        {id:0,adi:"İngilizce"},
                      ],
                      education:[
                        {id:0,adi:"HACETTEPE ÜNİVERSİTESİ TIP FAKÜLTESİ",yil:"1900"},
                      ],
                      icerik:"İCerik",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/erdal-aras.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:1,
                      name:"Levent İLHAN",
                      title:"Dr.",
                      bolum:"Pratisyen Hekim",
                      tarih:"11.09.1964",
                      birthlocation:"Erzincan",
                      lang:[
                        {id:0,adi:"İngilizce"},
                      ],
                      education:[
                        {id:0,adi:"ERCİYES ÜNİVERSİTESİ TIP FAKÜLTESİ",yil:"1900"},
                      ],
                      icerik:"İCerik2",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/yok.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:2,
                      name:"Ercan Akpınar",
                      title:"Dr.",
                      bolum:"Pratisyen Hekim",
                      tarih:"16.05.1970",
                      birthlocation:"Erzurum",
                      education:[
                        {id:0,adi:"Hacettepe Üniversitesi Tıp Fakültesi",yil:"1900"},
                      ],
                      lang:[
                        {id:0,adi:"ingilizce"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/ercan-akpınar.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:3,
                      name:"Ömer Orçun Koçak",
                      title:"Op. Dr.",
                      bolum:"Kadın Hastalıkları ve Doğum",
                      tarih:"31.08.1977",
                      birthlocation:"İstanbul",
                      education:[
                        {id:0,adi:"İstanbul Üniversitesi, Cerrahpaşa Tıp Fakültesi Kadın Hastalıkları ve Doğum Anabilim Dalı (2001–2007).",yil:"1900"},
                        {id:1,adi:"İstanbul Üniversitesi, İstanbul Tıp Fakültesi (1994–2001).",yil:"1900"},
                        {id:2,adi:"İstanbul Eğitim ve Kültür Vakfı (İSTEK), Florya Özel Bilge Kağan Lisesi (1988-1994)",yil:"1900"},
                        {id:3,adi:"Yeşilköy Hamdullah Suphi Tanrıöver İlköğretim Okulu, İstanbul (1983–1988)",yil:"1900"},
                      ],
                      lang:[
                        {id:0,adi:"ingilizce"},
                      ],
                      deneyim:[
                        {id:0,adi:"Özel Bodrum Hastanesi , Bodrum-Muğla (Temmuz 2011’den beri) ",yil:"1900"},
                        {id:1,adi:"Özel Anamed Hastanesi , Mersin ( Eylül 2010 – Temmuz 2011) ",yil:"1900"},
                        {id:2,adi:"Bingöl Özel Hastanesi, Bingöl (Ekim 2009 – Ağustos 2010 )",yil:"1900"},
                        {id:3,adi:"T.S.K 54’üncü Mekanize Piyade Tugayı, Harbiye Kışlası, Aile Reviri, Edirne (Kasım 2008 - Eylül 2009).",yil:"1900"},
                        {id:4,adi:"T.C. Sağlık Bakanlığı, Bingöl Doğum ve Çocuk Bakımevi Hastanesi, Bingöl (Zorunlu Hizmet kapsamında, Mart 2007-Eylül 2008 arası).",yil:"1900"},
                        {id:5,adi:"Türk Böbrek Vakfı, İstanbul Hizmet Hastanesi, Kadın Hastalıkları ve Doğum Departmanı (2006–2007 gece nöbetleri).",yil:"1900"},
                        {id:6,adi:"İstanbul Cerrahi Hastanesi (2007 gece nöbetleri).",yil:"1900"},
                        {id:7,adi:"Avcılar Medicana Hastanesi (2005–2007 gece nöbetleri)",yil:"1900"},
                        {id:8,adi:"Prof. Dr. Vildan Ocak (İ.Ü. CTF Kadın Hastalıkları ve Doğum ABD, Perinatoloji Bilim Dalı Başkanı) Özel Muayenehanesi, Mecidiyeköy, (2005–2007 yılları arasında ultrasonografist olarak)",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/omerorcun.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:4,
                      name:"Cengiz Kayahan",
                      title:"Doç. Dr.",
                      bolum:"Genel Cerrahi",
                      tarih:"24.10.1959",
                      birthlocation:"Ankara",
                      education:[
                        {id:1,adi:"İstanbul Üniversitesi, İstanbul Tıp Fakültesi",yil:"1900"},
                      ],
                      lang:[
                        {id:0,adi:"ingilizce"},
                      ],
                      deneyim:[
                        {id:0,adi:"2011-2017-Medicana International Hospital Samsun ",yil:"1900"},
                        {id:1,adi:"2009-2011-Medicalpark Özel Ordu Hastanesi",yil:"1900"},
                        {id:2,adi:"2008-2009-Özel Keçiören Hastanesi Ankara",yil:"1900"},
                        {id:3,adi:"2008-2008-Özel Caria Hastanesi Marmaris Muğla",yil:"1900"},
                        {id:4,adi:"2007-2008-Muayenehane Hekimliği",yil:"1900"},
                        {id:5,adi:"2004-2007-Emeklilik / Muayenehane Hekimliği",yil:"1900"},
                        {id:6,adi:"2000-2004-GATA Acil Tıp AD Öğr. Üyesi",yil:"1900"},
                        {id:7,adi:"2000 -Genel Cerrahi Doçenti",yil:"1900"},
                        {id:8,adi:"1999-2000-GATA Acil Tıp AD Öğretim Görevlisi",yil:"1900"},
                        {id:10,adi:"1993-1999-GATA Genel Cerrahi AD Yard. Doç.liği",yil:"1900"},
                        {id:11,adi:"1994:Siirt 30 Ytk.Seyy.Cerr.Hast Bştbp.liği (4 ay)",yil:"1900"},
                        {id:12,adi:"1996:Siirt 30 Ytk.Seyy.Cerr.Hast Bştbp.liği (4 ay)",yil:"1900"},
                        {id:13,adi:"1992-1993-Gölcük Dz. Hst. Genel Cerrahi Uzmanlığı",yil:"1900"},
                        {id:14,adi:"1988-1992-GATA Genel Cerrahi AD Uzmanlık Eğitimi",yil:"1900"},
                        {id:15,adi:"1987-1988-Dz.K.K.lığı TCG Turgut Reis Gemi Tabipliği",yil:"1900"},
                        {id:16,adi:"1986-1987-GATA Askeri Tıbbi Deontolojii Eğitimi",yil:"1900"},
                        {id:17,adi:"1985-1986-Dz.K.K.lığı Tuzla Dz H.O. Revir Tabipliği",yil:"1900"},
                        {id:18,adi:"1984-1985-Ank.Altındağ M.S.O.Tabipliği",yil:"1900"},
                        {id:19,adi:"1982-1984-Giresun Eynesil Sağ. Ocağı Tabipliği",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/cengiz-kayahan.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:5,
                      name:"Mehmet Demircioğlu",
                      title:"Dr.",
                      bolum:"İç Hastalıkları",
                      education:[
                        {id:1,adi:"2000 İstanbul Ü.Cerrahpaşa Tıp Fak İç Hastalıkları",yil:"1900"},
                        {id:2,adi:"1989 İstanbul Üniversitesi Cerrahpaşa Tıp Fakültesi",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"2011 - 2014 Memorial Antalya Hastanesi",yil:"1900"},
                        {id:1,adi:"2008 - Marmaris Caria Hastanesi",yil:"1900"},
                        {id:2,adi:"2004 - 2017 Acıbadem Sağlık Grubu ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/dr-mdemir.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:7,
                      name:"Muaffak Bağdatlı",
                      title:"Op. Dr.",
                      bolum:"Ortopedi ve Travmatoloji",
                      tarih:"16.11.1978",
                      birthlocation:"Bornova",
                      education:[
                        {id:1,adi:"1990 Çukurova Üniversitesi Tıp Fakültesi Kulak, Burun ve Boğaz Hastalıkları",yil:"1900"},
                        {id:2,adi:"1983 Çukurova Üniversitesi Tıp Fakültesi",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"2013 - Halen Acıbadem Sağlık Grubu ",yil:"1900"},
                        {id:1,adi:"2004 - 2013Universal Hospital Bodrum Hastanesi ",yil:"1900"},
                        {id:2,adi:"1998 - 2002Ankara Egitim ve Arastirma HastanesiOrtopedi ve Travmatoloji Asistanlik Programi ",yil:"1900"},
                        {id:3,adi:"1996 - 1997 Gediz Hastanesi ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/dr-murat.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:8,
                      name:"Alp Mustafa Günay",
                      title:"Dr.",
                      bolum:"İç Hastalıkları ve Gastroenteroloji",
                      tarih:"03.09.1966",
                      birthlocation:"Eskişehir",
                      mail:"alp.mustafa.gunay@acibadem.com.tr",
                      education:[
                        {id:1,adi:"2000'de Gastroenteroloji Yandal Uzmanı olarak mezun olmuş",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"Gülhane Askeri Tıp Akademisi'nden 1990'da Tıp Doktoru",yil:"1900"},
                        {id:1,adi:"1997'de İç Hastalıkları Uzmanı",yil:"1900"},
                        {id:2,adi:"9 yabancı, 16 yerli yayını ve 27 yabancı, 24 yerli bildirisi mevcuttur. ",yil:"1900"},
                        {id:3,adi:"2010'da da Avrupa Birliği Tıp Uzmanları Derneği'nden (EUMS) Avrupa Gastroenteroloji ve Hepatoloji Uzmanlık Sertifikası (EBGH) almıştır. ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/alp-mustafa-gunay.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:9,
                      name:"Tahir Aşkın Laçin",
                      title:"Dt.",
                      bolum:"Ağız ve Diş Sağlığı",
                      tarih:"1956",
                      lang:[
                        {id:0,adi:"İngilizce"},
                      ],
                      education:[
                        {id:1,adi:"ANKARA ÜNİVERSİTESİ DİŞ HEKİMLİĞİ FAKÜLTESİ - 1980",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"T.C. Ziraat Bankası Kadıköy Sağlık Polikliniği - Diş hekimi / İstanbul (1982-2007)",yil:"1900"},
                        {id:1,adi:"Diş Hekimi - Özel Muayenehane / İstanbul (2007-2009)",yil:"1900"},
                        {id:2,adi:"İstanbul Dentestetik Diş Polikliniği - Diş Hekimi, Mesul Müdür / İstanbul (2009-2011)",yil:"1900"},
                        {id:3,adi:"Dentalform Estetik Diş Klıniği - Diş Hekimi / İstanbul (2011-2015)",yil:"1900"},
                        {id:4,adi:"Diş Hekimi, Clinic International BMC Tip Merkezi, Bodrum (2015-... ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/tahir-askin-lacin.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:11,
                      name:"Özgür Bayındır",
                      title:"Dr.",
                      bolum:"Acil Hekimi",
                      tarih:"11.04.1973",
                      birthlocation:"Tire",
                      lang:[
                        {id:0,adi:"İngilizce"},
                        {id:1,adi:"İspanyolca"},
                      ],
                      education:[
                        {id:1,adi:"İSTANBUL ÜNİVERSİTESİ ÇAPA TIP FAKÜLTESİ 1991-1998",yil:"1900"},
                        {id:1,adi:"İZMİR BORNOVA ANADOLU LİSESİ 1985-1991",yil:"1900"},
                      ],
                      deneyim:[
                        {id:0,adi:"LÖSEMİLİ ÇOCUKLAR VAKFI 1999-2000",yil:"1900"},
                        {id:1,adi:"BRISTOL MYERS SQVIBA, CLINIC RESEARCH ASSOCIATE 2000-2001",yil:"1900"},
                        {id:2,adi:"CLINIC INTERNATIONAL TIP MERKEZİ, BODRUM 2001- ",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/dr-ozgur.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:14,
                      name:"Fikret Mert Acar",
                      title:"Uzm. Dr.",
                      bolum:"Kardiyolji",
                      tarih:"25.03.1979",
                      birthlocation:"Manisa",
                      education:[
                        {id:1,adi:"2003-Hacettepe Üniversitesi Tıp Fakültesi",yil:"1900"},
                        {id:2,adi:"2010-İstanbul Üniversitesi Kardiyoloji Enstitüsü",yil:"1900"},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/fikret-mert-acar.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:15,
                      name:"Pelin Akıncı Demiray",
                      title:"Dt.",
                      bolum:"Ağız ve Diş Sağlığı",
                      tarih:"15.08.1992",
                      lang:[
                        {id:0,adi:"English"},
                      ],
                      birthlocation:"Edirne",
                      education:[
                        {id:1,adi:"ISTANBUL UNIVERSITY FACULTY OF DENTISTRY",yil:""},

                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/yok.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                    {
                      id:16,
                      name:"Alp Kemal Okyay",
                      title:" Uzm.Dr.",
                      bolum:"Aile Hekimliği",
                      tarih:"25.03.1967",
                      lang:[
                        {id:0,adi:"English"},
                      ],
                      birthlocation:"Ankara",
                      education:[
                        {id:1,adi:"ANKARA UNIVERSITY FACULTY OF MEDICINE",yil:""},
                        {id:2,adi:"ANKARA EDUCATION AND RESEARCH HOSPITAL FAMILY MEDICINE",yil:""},
                      ],
                      icerik:"İCerik3",
                      resim:"http://clinicinternational.com.tr/2011/g/doktorlar/yok.jpg",
                      compoLang:{
                        kisisel_bilgiler:"Kişisel Bilgiler:",
                        ad_soyad:"Adı Soyadı",
                        dogum_tarihi:"Doğum Tarihi",
                        dogum_yeri:"Doğum Yeri",
                        dil:"Yabancı Dil",
                        mesleki_deneyim:"Mesleki Deneyim",
                        egitim_bilgileri:"Eğitim Bilgileri",
                        mezun:"Mezun Olduğu Tıp Fakültesi ve yılı"
                      },
                    },
                  ]
                }
              },
            },
            //acenterler
            {
              id:192,
              paths:'/anlasmali',
              adi:"Contracted Agencies",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              subMenu:[
                {
                  id:1678361,
                  paths:'/yerlisigortalar',
                  birimAdi:"Contracted Agencies ",
                  adi:"Local Agencies",
                  sidemenuVisible:true,
                  exact:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Contracted Agencies",
                    icerikContext:[
                        {
                            tag:"p",
                            icerik:"Contracted Agencies",
                            classname:"content-p-big",
                        },
                        {
                          tag:"li",
                          icerik:"AMERİCAN LİFE SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"EUREKO SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"YAPIKREDİ SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"ANADOLU SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"GROUPAMA SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"DEMİR HAYAT SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"AXA SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"ACIBADEM SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"ERGO SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"PROMED",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"RAY SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"GÜNEŞ SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"AK SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"MEDNET",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"ALLİANZ SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"AMERİCAN LİFE SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"MAPFRE GENEL YAŞAM SİGORTA",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"AKBANK T.A.Ş.TEKAÜT SANDIĞI VAKFI",
                          classname:"content-p",
                        },
                    ]
                  },
                },
                {
                  id:16267247278361,
                  paths:'/yabancisigortalar',
                  birimAdi:"Contracted Agencies ",
                  adi:"Foreign Agencies",
                  sidemenuVisible:true,
                  exact:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Foreign Agencies",
                    icerikContext:[
                      {
                          tag:"p",
                          icerik:"Contracted Agencies",
                          classname:"content-p-big",
                      },
                      {
                        tag:"p",
                        icerik:"England",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:"Assistance International",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"ChargeCare International",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Intergroup Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"AXA Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Europ Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Thompson Travel Insurances",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Cega",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"First Assist",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Speciality Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Holland",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:"Avero Achmea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Zorgenzekerheid",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"IZZ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"VGZ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ohra-Delta Lloyd",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial-Elvia",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"DSW",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Europeesche Verzekeringen",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Eurocenter",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"SOS International (Ass.Company)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"ANWB(Ass.Company)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Eurocross International (Ass.Company)",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Neckermann Travel Insurances",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"FBTO",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Zilveren Kruis Achmea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"IZA",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"DeFriesland",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Unive",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Menzis",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"CZ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"OZ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Groene Land Achmea",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Trias",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"France",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:"AXA Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Europ Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mutuaide Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial-Elvia",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"France Secours",
                        classname:"content-p",
                      },
                      {
                        tag:"p",
                        icerik:"Belgium",
                        classname:"content-p-small",
                      },
                      {
                        tag:"li",
                        icerik:"Eurocross Belgium ",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Interassistance-Europese",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Thomas Cook Travel Insurances",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Ethias",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Interpartner Assistance",
                        classname:"content-p",
                      },
                      {
                        tag:"li",
                        icerik:"Mondial-Elvia",
                        classname:"content-p",
                      },
                    ]
                  },
                },
                {
                  id:85683273,
                  paths:'/oteller',
                  birimAdi:"Contracted Agencies ",
                  adi:"Hotels",
                  sidemenuVisible:true,
                  exact:false,
                  componenti:0,
                  icerikveri:{
                    icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                    icerikHeader:"Hotels",
                    icerikContext:[
                        {
                            tag:"p",
                            icerik:"Contracted Agencies",
                            classname:"content-p-small",
                        },
                        {
                          tag:"li",
                          icerik:"Voyage Torba (Torba)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Voyage Bodrum Charm (Bardakçı)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Voyage Hebilköy (Türkbükü)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Blue Dreams Hotel (Torba)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Bodrum Holiday Resort (İçmeler)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Club Ersan (İçmeler)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Forever Hotel (İçmeler)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Manuela Hotel (Bitez)",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Işıl Club Milta",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Rixos Hotel",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Jumeirah Hotel",
                          classname:"content-p",
                        },
                        {
                          tag:"li",
                          icerik:"Vogue Hotel",
                          classname:"content-p",
                        },
                    ]
                  },
                }
              ],
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Deneme Başlığıdğr",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Deneme psidir.",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Bu nu bilmiyorum psidir.",
                    classname:"content-p",
                  }
                ]
              },
            },
            {
              id:200,
              paths:'/fotogaleri',
              adi:"Photo Gallery",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Photo Gallery",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Squares from our hospital",
                    classname:"content-p",
                  },
                ],
                fotogallery:true
              },
            },
            {
              id:210,
              paths:'/ik',
              adi:"Human Resources",
              sidemenuVisible:true,
              exact:false,
              insan_kaynaklari:true,
              componenti:0,
              emaillang:{
                kisisel:"Personal Information",
                egitim:"Educational Information",
                sonmezunOlunan:"Last Graduated School :",
                yabanciDil:"Foreign Language",
                pozisyon:"Position :",
                basvuruTarih:"Application date",
                adSoyad:"Fullname :",
                dogumYeri:"Your place of birth :",
                dogumTarih:"Birth Date :",
                cinsiyet:"Gender :",
                uyruk:"Nationality",
                mesaj:"Message :",
                gonder:"Send",
                cep:"Mobile Phone :",
                randevu_birim:"Randevu İstediğiniz Birim :",
                randevu_tarih:"Randevu İstediğiniz Tarih :",
                randevu_saat:"Randevu İstediğiniz Saat :",
                aciklama:"Açıklama"
              },
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"JOB APPLICATION FORM",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:`Vision We aim to employ people who are qualified in the direction of our mission and corporate values, who are capable of teamwork, who are open to innovations, open to innovations, potential to develop themselves and business, and to develop these people with continuous training programs, effective performance and reward systems.`,
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:`Job Application Form:`,
                    classname:"content-p-small",
                  },
                ]
              },
            },
            {
              id:220,
              paths:'/gorusoneri',
              adi:"Comments and Suggestions",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              email_gorus:true,
              emaillang:{
                adSoyad:"Full Name :",
                mesaj:"Message :",
                gonder:"Send"
              },
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Comments and Suggestions",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"Your views and suggestions are important to us. Please share your opinions and suggestions with us.",
                    classname:"content-p",
                  },
                ]
              },
            },
            //randevu
            {
              id:230,
              paths:'/e_randevu',
              adi:"Appointment",
              sidemenuVisible:true,
              exact:false,
              componenti:0,
              emaillang:{
                adSoyad:"Fullname :",
                mesaj:"Message :",
                gonder:"Send",
                cep:"Mobile Phone :",
                randevu_birim:"Appointment Unit :",
                birimlang:[
                  {adi:"Family Medicine"},
                  {adi:"General Surgery"},
                  {adi:"Cardiology"},
                  {adi:"Urology"},
                  {adi:"Skin Diseases"},
                  {adi:"Gynecology Department"},
                  {adi:"Gastroenterology"},
                  {adi:"Internal Diseases"},
                  {adi:"Orthopaedics and Traumatology"},
                  {adi:"Eye Diseases"},
                  {adi:"Otorhinolaryngology"},
                  {adi:"Dentistry"},
                ],
                randevu_tarih:"Appointment Date you want :",
                randevu_saat:"Appointment Date you time :",
                aciklama:"Explanation"
              },
              email_gorus:false,
              email_randevu:true,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Appointment",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"You can create your online appointment request from here ..",
                    classname:"content-p",
                  },
                ]
              },
            },
            {
              id:19451812142,
              paths:'/hastahikayeleri',
              adi:"Patient Story",
              sidemenuVisible:false,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Patient Story",
                icerikContext:[
                  {
                    tag:"div",
                    icerik:"Patient Story",
                    classname:"content-p-big",
                  },
                  {
                    tag:"p",
                    icerik:"",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Tops Nadine, hotel Voyage Bodrum Thanks very much",
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:"Hallo,",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Via deze weg willen we jullie erg hard bedanken voor de goede zorgen die jullie hebben gegeven.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Dankzij jullie dokters,verplegers,al het andere personeel en vooral de nederlandstalige mevrouw voelden we ons meer dan comfortabel in",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Clinic International. We bedanken jullie hiervoor,en willen we ook nog meegeven dat we onze reis hebben voortgezet naar Kos.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Ondertussen zijn we terug thuis en daarom dit mailtje.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Nogmaals bedankt,",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Nadine Tops,",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Ik was bij jullie van 29/05 tot 30/05/2013.",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"div",
                    icerik:"Dear Dr. Ozgur and all your wonderful staff ...",
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:`At first: I'm sorry it has taken me so long to write this....
                    I want to thank you and your staff for the care I received during my recent stay at Clinic International. Thank you for all you do. It means a lot. You guys do a lot for so many. I dont know where I would be without your great help.`,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`I especially want to thank Dr Omur- one of the sweetest and friendliest doctor-, Nurse Duygu ( my angel) and her fiancé Mehmet, Ebru, Baris , Bulent and Steve. But I never met a doctor in my life like Dr. Ali, that has shown so much compassion for his patient as he did for me! I have to tell that every single one of them gave me excellent care/ attention/ emotional support! My stay in the hospital was a bit more tolerable because of your excellent and very profesisional care ! We need a Clinic International in every town.`,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Also, a big thank you to the drivers of the buses. They were very polite caring of their passengers.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Thank you all much more than I can ever express !",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`with Kind Regards
                    Leyla Karnaz ( Holland )`,
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:"Met vriendelijke groet,",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"div",
                    icerik:"Dear Clinic-international staff,",
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:`Today I visited an eye specialist in The Netherlands. Everything was ok. No problems with the cornea, eye-pressure and sight.`,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`My specialist complimented you with your great care and professional treatment. She said: “you were treated better and more efficient than you would have been in The Netherlands”.`,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Greetings to the medical team, dr. Ömür aydoğan, Laverne Costello Clarke and my personal “Florence Nightengale’ (the stunning blonde ;-)).",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"The last two have made my stay in your clinic very comfortable. Mr. Clarke supplied me with a WiFi-code, so I could talk to my family with LiveView and could find some information about the herpes-zoster-virus.",
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`Thank you very much! You were great!`,
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"",
                    classname:"content-p",
                  },
                  //
                  {
                    tag:"div",
                    icerik:"Dear doctor, ",
                    classname:"content-p-small",
                  },
                  {
                    tag:"div",
                    icerik:`I'll like to thank you for the marvellous care!!!
                    Now, at home, I realise how gratefull I am to meet you at the moment I was afraid, miserable, sick and lonely!
                    Except y'are a friend of Octay, I even know your name and I never ask about....
                    Congratulations for the hole team, everybody was friendly, kind and helpfull in the clinic. `,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:`With kind regards, `,
                    classname:"content-p",
                  },
                  {
                    tag:"div",
                    icerik:"Mieke Demandt",
                    classname:"content-p",
                  },
                ]
              },
            },
            {
              id:1891486481,
              paths:'/iletisim',
              adi:"Communication",
              sidemenuVisible:false,
              exact:false,
              componenti:0,
              icerikveri:{
                icerikImage:"http://clinicinternational.com.tr/2011/g/about-photo1.jpg",
                icerikHeader:"Communication",
                icerikContext:[
                  {
                    tag:"p",
                    icerik:"İnformation",
                    classname:"content-p-big",
                  },
                  {
                    tag:"p",
                    icerik:"Adress",
                    classname:"content-p-small",
                  },
                  {
                    tag:"p",
                    icerik:"Kıbrıs Şehitleri Cad. No 181 (Antik Tiyatro Çaprazı) 48400 Bodrum - Türkiye",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Phone",
                    classname:"content-p-small",
                  },
                  {
                    tag:"p",
                    icerik:"0252 313 30 30",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Fax",
                    classname:"content-p-small",
                  },
                  {
                    tag:"p",
                    icerik:"0252 316 00 08",
                    classname:"content-p",
                  },
                  {
                    tag:"p",
                    icerik:"Email",
                    classname:"content-p-small",
                  },
                  {
                    tag:"p",
                    icerik:"info@clinicinternational.com.tr",
                    classname:"content-p",
                  },
                  {
                    tag:"map",
                    icerik:"info@clinicinternational.com.tr",
                    classname:"content-p",
                  },
                ]
              },
            },
           ]
        },
       })
    }
    this._onSetLanguageToEnglish = this._onSetLanguageToEnglish.bind(this);
    this._onSetLanguageToTurkish = this._onSetLanguageToTurkish.bind(this);
    this._onSetLanguageToFrance = this._onSetLanguageToFrance.bind(this);
  }
  componentDidMount(){
    //this._onSetLanguageToItalian();
  }
  _onSetLanguageToEnglish() {
    this.state.strings.setLanguage('en');
    this.setState({});
    console.log("bastım");
    //this.setState({});
  }
  _onSetLanguageToTurkish() {
    this.state.strings.setLanguage('tr');
    this.setState({});
    console.log("bastım");
    //this.setState({});
  }
  _onSetLanguageToFrance() {
    this.state.strings.setLanguage('fr');
    this.setState({});
    console.log("bastım");
    //this.setState({});
  }
  render() {
    const Wrapper =()=>(
      <div>
        <Banner
          componentList={this.state.veri}
          baslik={this.state.strings.baslik}
          tr={this._onSetLanguageToTurkish}
          en={this._onSetLanguageToEnglish}
          fr={this._onSetLanguageToFrance}
          anaSayfa={this.state.strings.anasayfa}
        />
        <div className="middle-wrapper">
        <Sidebar
          componentList={this.state.strings.veri}
          baslik={this.state.strings.baslik}
        />
        <Content componentData={this.state.strings.veri}/>
        <Footer/>
        </div>
      </div>
    );
    return (
      <div className="App">
        <div className="Content">
          <BrowserRouter>
            <Wrapper/>
          </BrowserRouter>
        </div>
      </div>
    );
  }
}

export default App;
